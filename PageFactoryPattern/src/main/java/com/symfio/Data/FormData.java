package com.symfio.Data;

import com.symfio.Pages.abstractPage.Page;
import com.symfio.Utils.Generate;
import com.symfio.Utils.ReplacementKey;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.SimpleDateFormat;
import java.util.*;

public class FormData extends Page{

    public FormData(WebDriver driver) {
        super(driver);
    }
    private WebDriverWait wait = new WebDriverWait(driver, 15, 200);
    private Random random = new Random();

    @FindBy(xpath = "//meta[contains(@property,'price') and contains(@property,'amount')]")
    private WebElement currentPrice;

    @FindAll({@FindBy(xpath = "//ul[contains(@style,'display: block;')]/li/a")})
    private List<WebElement> autocompleteList;

    @Override
    public void open(){}
    @Override
    public void checkSendSuccessful(){}

    public HashMap<WebElement, String> generate(HashMap<WebElement, String> list){
        HashMap<WebElement, String> map = new HashMap<>();

        for (Map.Entry<WebElement, String> pair : list.entrySet()){
            if (!pair.getKey().getTagName().equalsIgnoreCase("select")
                    && !pair.getKey().getAttribute("type").equalsIgnoreCase("checkbox")
                    && !pair.getKey().getTagName().equalsIgnoreCase("ul")) {
                if (pair.getKey().getAttribute("data-plugin") != null){
                    if (pair.getKey().getAttribute("data-plugin").equalsIgnoreCase("autocomplete")){
                        // This is input : autocomplete, we don't generate data for him
                        map.put(pair.getKey(), "autocomplete");
                    }
                    else if (createData(pair.getValue()) != null){
                        map.put(pair.getKey(), createData(pair.getValue()));
                    }
                }
                else if (createData(pair.getValue()) != null) {
                    map.put(pair.getKey(), createData(pair.getValue()));
                }
            }else{
                map.put(pair.getKey(), pair.getValue());
            }
        }
        return map;
    }


    public Map<String, List<String>> fillForm(HashMap<WebElement, String> map){
        Map<String, List<String>> result = new HashMap<>();
        List<String> list;
        for (Map.Entry<WebElement, String> pair : map.entrySet()){
            if (isElementVisible(pair.getKey())) {
                try {
                    if (!pair.getKey().getTagName().equalsIgnoreCase("select")) {
                        if (pair.getKey().getAttribute("type").equalsIgnoreCase("checkbox")) {
                            choose(pair.getKey());
                            if (!pair.getKey().getAttribute("name").contains("optionNotifyme")
                                    && !pair.getKey().getAttribute("name").contains("optionMonitoring")) {

                                if (cutString(pair.getKey().getAttribute("name"), '[', ']').equalsIgnoreCase("vehicleOptions")){
                                    if (result.containsKey("vehicleInfo_options")){
                                        List<String> exist = new ArrayList<>();
                                        exist.addAll(result.get("vehicleInfo_options"));
                                        exist.add(pair.getKey().getAttribute("value"));
                                        result.put("vehicleInfo_options", exist);
                                    }else {
                                        list = Arrays.asList(pair.getKey().getAttribute("value"));
                                        result.put("vehicleInfo_options", list);
                                    }
                                }else {
                                    list = Arrays.asList("true");
                                    result.put(ReplacementKey.getProperties(cutString(pair.getKey().getAttribute("name"), '[', ']')), list);
                                }
                            }
                        } else if (pair.getKey().getTagName().equalsIgnoreCase("ul")) {
                            result.putAll(chooseLi(pair.getKey(), 5));
                        }/*else if (pair.getValue().equalsIgnoreCase("autocomplete")){
                            pair.getKey().click();
                            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[contains(@style,'display: block;')]")));
                            int current = random.nextInt(autocompleteList.size() - 1 + 1) + 1;
                            String value = autocompleteList.get(current).getText().trim();
                            autocompleteList.get(current).click();
                            if (pair.getKey().getAttribute("data-targets") != null){
                                detecteHtml();
                            }
                            list = Arrays.asList(value);
                            result.put(ReplacementKey.getProperties(cutString(pair.getKey().getAttribute("name"), '[', ']')), list);
                        }*/ else {
                            type(pair.getKey(), pair.getValue());
                            list = Arrays.asList(pair.getValue());
                            result.put(ReplacementKey.getProperties(cutString(pair.getKey().getAttribute("name"), '[', ']')), list);
                        }
                    } else {
                        int index = new Select(pair.getKey()).getOptions().size() > 1 ? 2 : 1;
                        if (pair.getKey().getAttribute("name").contains("registrationYear")){index++;}
                        new Select(pair.getKey()).selectByIndex(index);
                        if (pair.getKey().getAttribute("data-targets") != null) {
                            detecteHtml();
                        }
                        list = Arrays.asList(new Select(pair.getKey()).getFirstSelectedOption().getAttribute("value"));
                        result.put(ReplacementKey.getProperties(cutString(pair.getKey().getAttribute("name"), '[', ']')), list);
                    }
                } catch (Exception e) {
                    System.out.println("Skip... : " + pair.getKey().getAttribute("name"));
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    private String cutString(String str, char from, char to){
        String res;
        if (str.contains("[]")) {
            res = str.substring(0, str.lastIndexOf("["));
        }else {
            res = str;
        }
        int f = res.indexOf(from);
        int t = res.indexOf(to);
        return res.substring(f + 1, t);
    }

    private Map<String, List<String>> chooseLi(WebElement element, int count) throws Exception {
        List<WebElement> li = element.findElements(By.tagName("li"));
        String id = element.getAttribute("id");
        String key = driver.findElement(By.xpath("//ul[contains(@id,'" + id + "')]" +
                "/ancestor-or-self::div[@class='accordion-group']")).getAttribute("data-group");
        Map<String, List<String>> map = new HashMap<>();
        List<String> list = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            String value = li.get(i).getAttribute("data-text");
            list.add(value);
            li.get(i).click();
        }
        map.put(key, list);
        return map;
    }

    private String createData(String key){

        switch (key.toLowerCase()){
            case "vehiclelineholder":
                return Generate.uniqueString(10);
            case "vehicleexteriorcolor":
                //return Generate.uniqueString(5);
                return Generate.uniqueColor();
            case "vehiclesubmodel":
                return Generate.uniqueString(5);
            case "stock":
                return Generate.uniqueNumber(7);
            case "specification_emission_co2":
                return Generate.uniqueNumber(2);
            case "specification_fuelconsumption_extraurban":
                return Generate.uniqueNumber(2);
            case "specification_fuelconsumption_urban":
                return Generate.uniqueNumber(2);
            case "specification_fuelconsumption_combined":
                return Generate.uniqueNumber(2);
            case "specification_fuelconsumption_combinedpower":
                return Generate.uniqueNumber(2);
            case "comments":
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                return sdf.format(new Date(System.currentTimeMillis())) + " " + Long.toString(Generate.uniqueID());
//            case "vin":
//                return Generate.uniqueVIN();
            case "hsn":
                return Generate.uniqueHSN();
            case "tsn":
                return Generate.uniqueTSN();
            case "construction":
                SimpleDateFormat sdf2 = new SimpleDateFormat("dd.MM.yyyy");
                return sdf2.format(new Date(System.currentTimeMillis()));
            case "model":
                return Generate.uniqueString(3);
            case "submodelname":
                return Generate.uniqueString(3);
            case "doors":
                return Generate.uniqueNumber(1);
            case "seats":
                return Generate.uniqueNumber(1);
            case "exteriorcolormanufacturer":
                //return Generate.uniqueString(5);
                return  Generate.uniqueColor();
            case "engine":
                return Generate.uniqueNumber(5);
            case "powerkw":
                return Generate.uniqueNumber(3);
            case "powerhps":
                return Generate.uniqueNumber(3);
            case "pricecurrent":
                return Generate.uniqueNumber(5);
            case "odometer":
                return Generate.uniqueNumber(5);
            case "marketingtext":
                return Generate.uniqueString();
            case "extradescription":
                return Generate.uniqueString();
            case "contactname":
                return "Tester" + Generate.uniqueString(10);
            case "contactemail":
                return "symfio.test.1@gmail.com";
            case "contactphone":
                return "+38(098)566-84-65";
            case "contactcity":
                return Generate.uniqueString(6);
            case "contactstate":
                return Generate.uniqueString(5);
            case "contactaddress":
                return Generate.uniqueString(6);
            case "contactfirstname":
                return "Tester" + Generate.uniqueString(10);
            case "contactlastname":
                return "lastName" + Generate.uniqueString(6);
            case "contactpaygrade":
                return Generate.uniqueString(5);
            case "contactbranch":
                return Generate.uniqueString(5);
            case "vehiclepriceask":
                return Generate.uniqueNumber(5);
            case "vehicleodometermax":
                return Generate.uniqueNumber(5);
            case "vehicleodometerask":
                return Generate.uniqueNumber(5);
            case "pricetradein":
                return Generate.uniqueNumber(5);
            case "vehicleengine":
                return Generate.uniqueNumber(5);
            case "vehicleyear":
                return Generate.uniqueNumber(4);
//            case "vehiclevin":
//                return "WAUGGAFR4EA018263";
            case "vehicleinteriorcolor":
                //return Generate.uniqueString(5);
                return Generate.uniqueColor();
            case "financingmonthlyrate":
                return Generate.uniqueNumber(3);
            case "financinginitialpaymentfixed":
                Integer firstPayment = (Integer.parseInt(currentPrice.getAttribute("content")) / 100) * 20;
                return String.valueOf(firstPayment);
            case "financingfinalpaymentfixed":
                Integer finalPayment = (Integer.parseInt(currentPrice.getAttribute("content")) / 100) * 40;
                return String.valueOf(finalPayment);
            case "tradeindescription":
                Long time3 = System.currentTimeMillis();
                Date date3 = new Date(time3);
                SimpleDateFormat sdf3 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                return sdf3.format(date3) + " " + Long.toString(Generate.uniqueID());
            case "vehicletestdrivedate":
                Date date1 = new Date(System.currentTimeMillis());
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                return simpleDateFormat.format(date1) + " 13:00";
            case "vehicleregistration":
                Date date2 = new Date(System.currentTimeMillis());
                SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("MM/yyyy");
                return simpleDateFormat2.format(date2);
        }
        return null;
    }
}
