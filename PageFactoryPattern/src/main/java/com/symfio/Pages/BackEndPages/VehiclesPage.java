package com.symfio.Pages.BackEndPages;

import com.symfio.Pages.abstractPage.Page;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.*;
import java.util.stream.Collectors;

public class VehiclesPage extends Page{

    public VehiclesPage(WebDriver driver) {
        super(driver);
    }
    private WebDriverWait wait = new WebDriverWait(driver,15,200);

    @Override
    public void open() {}

    @Override
    public void checkSendSuccessful() {}

    @FindAll({@FindBy(xpath = "//div[contains(@class,'grid-cell-image')]/a/img")})
    private List<WebElement> photoCarsList;

    @FindBy(xpath = "//span[contains(@class,'header-info')]")
    private WebElement headInfo;

    @FindAll({@FindBy(xpath = "//tr[contains(@class,'row-inventory')]")})
    private List<WebElement> cars;

    @FindAll({@FindBy(xpath = "//div[contains(@class,'lead-data')]/a")})
    private List<WebElement> leadlinks;

    @FindBy(id = "tipue_search_input")
    private WebElement searchField;

    @FindAll({@FindBy(xpath = "//li[contains(@class,'ui-menu-item')]")})
    private List<WebElement> searchResult;

    @FindBy(xpath = "//select[contains(@name,'make_model')]")
    private WebElement makeModel;

    @FindBy(xpath = "//a[contains(@id,'list_grid_last')]")
    private WebElement buttonLastPage;

    @FindBy(xpath = "//a[contains(@class,'paginate_active')]")
    private WebElement currentPageNumber;

    @FindBy(id = "status")
    private WebElement filterStatus;
    
    @FindBy(id = "title")
    private WebElement filterTitle;

    public String parseCarLink(){
        String result = null;
        for (WebElement element : cars){
            if (isElementPresent(element.findElement(By.xpath("./descendant::span[contains(@class,'nobr')]")))){
                result = element.findElement(
                        By.xpath("./descendant::div[contains(@class,'grid-cell-image')]/a[1]")
                ).getAttribute("href");
                break;
            }
        }
        return result;
    }

    public List<String> openLastPage(){
        if (!isElementPresent(By.xpath("//a[contains(@id,'list_grid_last') and contains(@class,'disabled')]"))) {
            buttonLastPage.click();
            Long start = System.currentTimeMillis();
            waitGrid("vehicle");
            System.out.println(System.currentTimeMillis() - start);
            return Arrays.asList(currentPageNumber.getText().trim());
        }
        return null;
    }

    public Integer getFilterSumCountCars(String type){
        int result = 0;
        List<WebElement> list;

        if (type.equals("status")){
            list = new Select(filterStatus).getAllSelectedOptions();
        }
        else if (type.equals("title")){
            list = new Select(filterTitle).getAllSelectedOptions();
        }
        else {
            return null;
        }
        for(WebElement option : list){
            result += Integer.parseInt(cutFromString(option.getText().trim(),'(',')'));
        }
        return result;
    }

    public List<String> chooseFilterStatus(String command){
        new Select(filterStatus).deselectAll();
        waitGrid("all");
        if (command == "random"){
            new Select(filterStatus).selectByIndex(random.nextInt(getSelectOptions(filterStatus).size()));
        }
        else if (command.equals("max")){
            new Select(filterStatus).selectByValue(getMaxValueOption(filterStatus));
        }
        else if (command.equals("min")){
            new Select(filterStatus).selectByValue(getMinValueOption(filterStatus));
        }
        else if (command.equals("multiple")){
            new Select(filterStatus).selectByIndex(random.nextInt(getSelectOptions(filterStatus).size()));
            waitGrid("all");
            new Select(filterStatus).selectByIndex(random.nextInt(getSelectOptions(filterStatus).size()));
        }
        waitGrid("all");
        return new Select(filterStatus).getAllSelectedOptions().stream().map(
                elem -> elem.getText().trim().substring(0, elem.getText().trim().indexOf("(")).trim()).collect(Collectors.toList());
    }

    public String chooseFilterTitle(){
        new Select(filterTitle).deselectAll();
        new Select(filterTitle).selectByIndex(random.nextInt(getSelectOptions(filterTitle).size()));
        return new Select(filterTitle).getFirstSelectedOption().getText();
    }

    public Map<String, List<String>> checkSearchResult() throws Exception {
        Map<String, List<String>> keysMap = new HashMap<>();
        List<String> listStock = new ArrayList<>(),
                    listVin = new ArrayList<>(),
                    listMake = new ArrayList<>(),
                    listModel = new ArrayList<>();

        List<WebElement> stocks = driver.findElements(By.xpath("//tbody[contains(@role,'alert')]" +
                "/descendant::span[contains(@class,'spec-stock')]/span[@class='spec-value']"));
        List<WebElement> vins = driver.findElements(By.xpath("//tbody[contains(@role,'alert')]" +
                "/descendant::div[contains(@class,'extra-data')]/span[contains(@class,'vin-code')]"));
        for (int i = 0; i < 4; i++) {
            listStock.add(stocks.get(i).getText());
        }
     
        for (int i = 0; i < (vins.size() > 4 ? 4 : vins.size()); i++) {
            listVin.add(vins.get(i).getText());
        }
        
        keysMap.put("stock", listStock);
        keysMap.put("vin", listVin);
        int makeIndex = 0, modelIndex = 0, mn = 0;
        
        for (WebElement element : new Select(makeModel).getOptions()){
            if (mn++ > 1) {
                if (!element.getAttribute("value").contains("&")) {
                    if (makeIndex++ < 4){
                        listMake.add(parseMake(element.getAttribute("value")));
                    }
                }
                if (element.getAttribute("value").contains("&") &&
                        parseModel(element.getAttribute("value")).trim().length() > 1 ){
                    if (modelIndex++ < 4){
                        listModel.add(parseModel(element.getAttribute("value")));
                    }
                }
            }
        }
        keysMap.put("Make", listMake);
        keysMap.put("Model", listModel);
        return keysMap;
    }

    public void openCarPage(String link){
        driver.get(link + "#tab_leads");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class,'lead-data')]")));
    }

    public VehicleDetailsBackendPage openVehicleDetailsPage(String which){
        if (which.equals("first")){
            photoCarsList.get(0).click();
        }
        else if (which.equals("last")){
            photoCarsList.get(photoCarsList.size() - 1).click();
        }
        else {
            photoCarsList.get(random.nextInt(photoCarsList.size())).click();
        }
        return PageFactory.initElements(driver, VehicleDetailsBackendPage.class);
    }

    public String parseLeadId(){
        String result = null;
        for (int i = 0; i < leadlinks.size(); i++){
            result = parseId(leadlinks.get(i).getAttribute("href"));
            break;
        }
        return result;
    }

    public void openLead(String link){
        driver.get(link);
    }

    public void checkLeadIsOpened(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("attach_files")));
        wait.until(ExpectedConditions.visibilityOf(headInfo));
    }

    public void checkMultiSearch(Map<String, List<String>> map) throws Exception {
        for (Map.Entry<String, List<String>> pair : map.entrySet()){
            List<String> keys = pair.getValue();
            for (String key : keys){
                type(searchField, key);
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ul[contains(@style,'display: block')]")));
                if (searchResult.size() == 0){
                    throw new Exception("No search result for key : \"" + key + "\"");
                }
                for (WebElement element : searchResult){
                    String result = checkResult(key ,pair.getKey(), element);
                    if (!result.equals("success")){
                        throw new Exception(result);
                    }
                    driver.findElement(By.xpath(".//div[contains(@class,'dataTables_info')]")).click();
                }
            }
        }
    }

    public void checkSearchRemember(Map<String, String> result){
        if (result.containsKey("status")){
            Assert.assertEquals(result.get("status"),
                    new Select(filterStatus).getFirstSelectedOption().getAttribute("value"));
            //for (WebElement ){}
        }
        if (result.containsKey("title")){

        }
        if (result.containsKey("page")){

        }
    }

//======================================================================================================================//
//========================= Private Methods ============================================================================//
//======================================================================================================================//

    private String checkResult(String searchKey, String searchType, WebElement element){
        String monitor = "success", between = null;
        try {
            switch (searchType) {

                case "Make":
                    between = "Search result by keyword : " +  searchKey + " is not get result!!!";
                    element.findElement(By.xpath("./descendant::span[contains(@class,'vehicle-title')" +
                            " and contains(text(),'" + searchKey + "')]"));
                    break;
                case "Model":
                    between = "Search result by keyword : " +  searchKey + " is not get result!!!";
                    element.findElement(By.xpath("./descendant::span[contains(@class,'vehicle-title')" +
                            " and contains(text(),'" + searchKey + "')]"));
                    break;
                case "MakeModel":
                    between = "Search result by keyword : " + searchKey + " is not get result!!!";
                    element.findElement(By.xpath("./descendant::span[contains(@class,'vehicle-title')" +
                            " and contains(text(),'" + searchKey + "')]"));
                    break;
                case "Stock":
                    between = "Search result by keyword : " +  searchKey + " is not get result!!!";
                    element.findElement(By.xpath("./descendant::span[contains(@class,'spec-stock')]" +
                            "/span[contains(@class,'spec-value') and contains(text(),'" + searchKey + "')]"));
                    break;
                case "Vin":
                    between = "Search result by keyword : " +  searchKey + " is not get result!!!";
                    element.findElement(By.xpath("./descendant::div[contains(@class,'extra-data')]/" +
                            "span[contains(text(),'" + searchKey + "')]"));
                    break;
            }
        }catch (Exception e){monitor = between;}
        return monitor;
    }

    private String parseId(String string){
        String result = string.substring(string.indexOf("'") + 1, string.lastIndexOf("'"));
        return "?id=" + result;
    }

    private String parseMake(String string){
        String result = string.substring(string.indexOf("=") + 1);
        return result;
    }

    private String parseModel(String string){
        String result = string.substring(string.lastIndexOf("=") + 1);
        return result;
    }

    private List<WebElement> getFilterParams(WebElement element){
        return element.findElements(By.xpath("./preceding-sibling::div[contains(@id,'" + element.getAttribute("id") + "')]" +
                "//*/a[contains(@class,'search-choice-close')]"));
    }

    private String getMaxValueOption(WebElement select){
        int monitor = 0;
        String result = "";
        for (WebElement option : getSelectOptions(select)){
            int current = Integer.parseInt(cutFromString(option.getText().trim(),'(',')'));
            if (monitor < current){
                monitor = current;
                result = option.getAttribute("value");
            }
        }
        return result;
    }

    private String getMinValueOption(WebElement select){
        int monitor = Integer.parseInt(cutFromString(getSelectOptions(select).get(0).getText().trim(),'(',')'));
        String result = getSelectOptions(select).get(0).getAttribute("value");
        for (WebElement option : getSelectOptions(select)){
            if (monitor > Integer.parseInt(cutFromString(option.getText().trim(),'(',')'))){
                monitor = Integer.parseInt(cutFromString(option.getText().trim(),'(',')'));
                result = option.getAttribute("value");
            }
        }
        return result;
    }

    private List<String> getSelect2ChoiceText(WebElement element){
        List<String> list = element.findElements(
                By.xpath("./preceding-sibling::div[contains(@id,'" + element.getAttribute("id") + "')]" +
                "//*/li[contains(@class,'search-choice')]/div")
        ).stream().map(elem -> elem.getText().trim().substring(0, elem.getText().indexOf("(")).trim()).collect(Collectors.toList());
        return list;
    }

    private String cutFromString(String string, Character start, Character finish){
        return string.substring(string.indexOf(start) + 1, string.lastIndexOf(finish));
    }
}
