package com.symfio.Pages.BackEndPages;


import com.symfio.Pages.abstractPage.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class MobileDePage  extends Page{

    public MobileDePage(WebDriver driver) {
        super(driver);
    }
    private WebDriverWait wait = new WebDriverWait(driver, 10, 200);
    private int currentCountCars;
    private int IndexCurrentCount;

    @FindAll({@FindBy(xpath = "//tbody/descendant::tr")})
    private List<WebElement> carsList;

    @FindBy(xpath = "//div[contains(@class,'filter-addon')]/descendant::select")
    private WebElement buttonSwitchCoutShowCars;

    public void countCurrentCars(){
        currentCountCars = (carsList.size() - 1);
        IndexCurrentCount = (currentCountCars / 10);
    }

    public void switchNextCountCarOnPage() throws InterruptedException {
        int options = new Select(buttonSwitchCoutShowCars).getOptions().size();
        IndexCurrentCount++;
        if (IndexCurrentCount < options){
            new Select(buttonSwitchCoutShowCars).selectByValue(String.valueOf(currentCountCars + 10));
            boolean monitor = true;
            while (monitor){
                Thread.sleep(200);
                monitor = isElementPresent(By.xpath("//table[contains(@class,'loading')]"));
            }
            currentCountCars += 10;
            return;
        }
    }

    public void checkThatNewCount() throws Exception {
        int newCount = (carsList.size() - 1);
        if (newCount != (IndexCurrentCount * 10)){
            throw new Exception("Error : Button switch count cars on page Mobile.de not work!!!");
        }
    }

    @Override
    public void open() {}

    @Override
    public void checkSendSuccessful() {}
}
