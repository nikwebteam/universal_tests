package com.symfio.Pages;

import com.symfio.Pages.abstractPage.PageForm;
import com.symfio.Utils.ConfigProperties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashMap;
import java.util.List;

public class DetailsCarPage extends PageForm{

    @FindAll({@FindBy(xpath = "//div[contains(@class,'modal') and contains(@aria-hidden,'false')]//*/" +
            "div[contains(@class,'section-financing') or contains(@class,'section-contacts') or contains(@class,'section-permissions')]" +
            "//*[contains(@name,'[') and contains(@name,']') and not(contains(@class,'select'))]")})
    private List<WebElement> financingFormFields;

    @FindAll({@FindBy(xpath = "//div[contains(@class,'section-requestform')]//*/form[not(contains(@action,'ajax'))]//*/input[contains(@name,'[') and contains(@name,']')]"),
            @FindBy(xpath = "//div[contains(@class,'section-requestform')]//*/form[not(contains(@action,'ajax'))]//*/select[contains(@name,'[') and contains(@name,']')]"),
            @FindBy(xpath = "//div[contains(@class,'section-requestform')]//*/form[not(contains(@action,'ajax'))]//*/textarea[contains(@name,'[') and contains(@name,']')]")})
    private List<WebElement> detailsFormFields;

    @FindBy(xpath = "//div[contains(@class,'modal') and contains(@aria-hidden,'false')]//*/a[contains(@class,'button-send')]")
    private WebElement buttonFinance;

    @FindBy(id = "submit-contact")
    private WebElement buttonDetails;

    @FindBy(xpath = "//a[contains(@data-action,'financing') and contains(@data-toggle,'modal')]")
    private WebElement modalFinanceOpen;

    public DetailsCarPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public void open() {
        driver.get(ConfigProperties.getProperties("disa.test") +  "/app_test.php/auto/abarth/b7/neuwagen-R1Rx3d.html");
        driver.manage().window().maximize();
        for (String element : listclickHideCategory){
            if (isElementPresent(By.className(element))){
                driver.findElement(By.className(element)).click();
                try {
                    Thread.sleep(600);
                } catch (InterruptedException e) {

                }
            }
        }
    }

    @Override
    public void openModalForm() {
        modalFinanceOpen.click();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void checkSendSuccessful() {}

    public void sendFinanceModalForm(){buttonFinance.click();}

    public void sendDetailsForm(){buttonDetails.click();}

    public HashMap<WebElement, String> parseDetailsFormFields(){
        HashMap<WebElement, String> map = new HashMap<>();
        for (WebElement pair : detailsFormFields){
            String value = cutString(pair.getAttribute("name"), '[', ']');
            map.put(pair, value);
        }
        return map;
    }

    public HashMap<WebElement, String> parseFinancingFormFields(){
        HashMap<WebElement, String> map = new HashMap<>();
        for (WebElement pair : financingFormFields){
            String value = cutString(pair.getAttribute("name"), '[', ']');
            map.put(pair, value);
        }
        return map;
    }

    private String cutString(String str, char from, char to){
        int f = str.indexOf(from);
        int t = str.indexOf(to);
        return str.substring(f + 1, t);
    }
}
