package com.symfio.Pages;

import com.symfio.Pages.abstractPage.PageForm;
import com.symfio.Utils.ConfigProperties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashMap;
import java.util.List;

public class ContactPage extends PageForm{

    private WebDriverWait wait = new WebDriverWait(driver, 10, 200);

    @FindAll({@FindBy(xpath = "//form[not(contains(@action,'ajax'))]//*/input[contains(@name,'[') and contains(@name,']') and not(contains(@type,'hidden'))]"),
            @FindBy(xpath = "//form[not(contains(@action,'ajax'))]//*/select[contains(@name,'[') and contains(@name,']')]"),
            @FindBy(xpath = "//form[not(contains(@action,'ajax'))]//*/textarea[contains(@name,'[') and contains(@name,']')]")})
    private List<WebElement> fullFormFields;

    @FindBy(xpath = "//*[@class='button submit']")
    private WebElement buttonContactForm;

    public ContactPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public void open() {
        driver.get(ConfigProperties.getProperties("disa.test") + "/app_test.php/kontakt-test");
        driver.manage().window().maximize();
    }

    @Override
    public void openModalForm() {}

    @Override
    public void checkSendSuccessful() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class,'success')]")));
    }

    public void sendKontaktForm(){
        buttonContactForm.click();
    }

    public HashMap<WebElement, String> parseFullFormFields(){
        HashMap<WebElement, String> map = new HashMap<>();
        for (WebElement pair : fullFormFields){
            String value = cutString(pair.getAttribute("name"), '[', ']');
            map.put(pair, value);
        }
        return map;
    }

    private String cutString(String str, char from, char to){
        int f = str.indexOf(from);
        int t = str.indexOf(to);
        return str.substring(f + 1, t);
    }
}
