package com.symfio.Pages;

import com.symfio.Data.UserData;
import com.symfio.Pages.abstractPage.Page;
import com.symfio.Utils.ConfigProperties;
import com.symfio.Utils.Generate;
import com.symfio.Utils.ReplacementKey;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.SimpleDateFormat;
import java.util.*;

public class UploadWizardPage extends Page {

    private LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
    private WebDriverWait wait = new WebDriverWait(driver, 10, 200);
    private Random random = new Random();

    public UploadWizardPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//input[contains(@name,'priceNotax')]")
    private WebElement priceNotax;

    @FindBy(xpath = "//li[@class='auto']")
    private WebElement auto;

    @FindBy(xpath = "//li[@class='motorhome']")
    private WebElement motorHome;

    @FindBy(xpath = "//li[@class='motorbike']")
    private WebElement moto;

    @FindBy(xpath = "//li[@class='heavy_machinery']")
    private WebElement track;

    @FindBy(xpath = "//input[contains(@name,'stock')]")
    private WebElement stockField;

    @FindBy(xpath = "//a[contains(@href,'dashboard')]")
    private WebElement linkSignIn;

    @FindAll({@FindBy(xpath = "//label[contains(@class,'required')]")})
    private List<WebElement> listRequiredFields;

    @FindAll({@FindBy(xpath = "//div[@id='step-1']//*[contains(@name,'[') and contains(@name,']') and not(contains(@name,'mwst'))]")})
    private List<WebElement> firstPageFielads;

    @FindAll({@FindBy(xpath = "//div[@id='step-2']//*[contains(@name,'[') and contains(@name,']')]"),
              @FindBy(xpath = "//div[@id='step-2']//*/div[contains(@class,'accordion-group')]//*/ul")})
    private List<WebElement> secondPageFields;

    @FindAll({@FindBy(xpath = "//div[@id='step-4']//*[contains(@name,'[') and contains(@name,']')]")})
    private List<WebElement> fourPageFields;

    @FindBy(xpath = "//a[contains(@class,'uwNext')]")
    private WebElement buttonNext;

    @FindAll({@FindBy(xpath = "//button[contains(@data-name,'button_title')]")})
    private List<WebElement> titleButtons;

    @FindAll({@FindBy(xpath = "//button[contains(@data-name,'button_condition')]")})
    private List<WebElement> conditionButtons;

    @FindAll({@FindBy(xpath = "//button[contains(@data-name,'button_envkv')]")})
    private List<WebElement> envkvButtons;

    @FindBy(xpath = "//input[contains(@name,'mwst')]")
    private WebElement nds;

    @FindBy(xpath = "//a[contains(@class,'uwFinish')]")
    private WebElement buttonSave;

    @FindBy(xpath = "//a[contains(@class,'gotosite')]")
    private WebElement goToSiteButton;

    @FindBy(id = "detailsTabs")
    private WebElement detailsTabs;

    @FindAll({@FindBy(xpath = "(//ul[contains(@class,'ui-autocomplete')])[last()]/li/a")})
    private List<WebElement> autocompleteList;

    @Override
    public void open() {
        driver.get(ConfigProperties.getProperties("disa.test"));
        driver.manage().window().maximize();
        linkSignIn.click();
        loginPage.loginAs(new UserData("n.chupikov@symfio.de", "123456"));
        driver.get(ConfigProperties.getProperties("disa.test") + "/app_test.php/admin/en/upload-wizard");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@class='vehicle-categories']")));
    }

    public void ReOpen(){
        driver.get(ConfigProperties.getProperties("disa.test") + "/app_test.php/admin/en/upload-wizard");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@class='vehicle-categories']")));
    }

    public void openDetailsTab(int index) throws InterruptedException {
        List<WebElement> tabsList = detailsTabs.findElements(By.tagName("a"));
        tabsList.get(index - 1).click();
    }

    public String parseCarId(){
        String str = goToSiteButton.getAttribute("href");
        int f = str.lastIndexOf('-');
        int t = str.indexOf(".html");
        return str.substring(f + 1, t);
    }

    public void saveCar(){
        buttonSave.click();
        //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class,'plupload_header_title')]")));
    }

    public Map<String, List<String>> chooseNDStrue() throws InterruptedException {
        Map<String,List<String>> map = new HashMap<>();
        List<String> list = null;
        if (!nds.isSelected()){
            nds.click();
        }
        list = new ArrayList<>();
        list.add("true");
        map.put("mwst", list);
        list = new ArrayList<>();
        list.add(priceNotax.getAttribute("value"));
        map.put("priceNotax", list);
        return map;
    }

    public HashMap<WebElement, String> parseFirstPageFields(){
        HashMap<WebElement, String> map = new HashMap<>();
        for (WebElement pair : firstPageFielads){
            String value = cutString(pair.getAttribute("name"), '[', ']');
            map.put(pair, value);
        }
        return map;
    }

    public HashMap<WebElement, String> parseRequiredFields(){
        HashMap<WebElement, String> map = new HashMap<>();
        for (WebElement element : listRequiredFields){
            WebElement neighbor = element.findElement(By.xpath("./following-sibling::*"));
            List<WebElement> fields = neighbor.findElements(By.xpath("./descendant::*[contains(@name,'[') and contains(@name,']') and not(contains(@type,'button'))]"));
            for (WebElement field : fields) {
                String value = cutString(field.getAttribute("name"), '[', ']');
                map.put(field, value);
            }
        }
        return map;
    }

    public HashMap<WebElement, String> parseSecondPageFields(){
        HashMap<WebElement, String> map = new HashMap<>();
        for (WebElement pair : secondPageFields){
            if (!pair.getTagName().equalsIgnoreCase("ul")) {
                String value = cutString(pair.getAttribute("name"), '[', ']');
                map.put(pair, value);
            } else {
                map.put(pair, "ul");
            }
        }
        return map;
    }

    public HashMap<WebElement, String> parseFourPageFields(){
        HashMap<WebElement, String> map = new HashMap<>();
        for (WebElement pair : fourPageFields){
            String value = cutString(pair.getAttribute("name"), '[', ']');
            map.put(pair, value);
        }
        return map;
    }

    public void openCollapseOptions(){
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        String script = "arguments[0].setAttribute('style', 'height:auto')";
        String script2 = "arguments[0].setAttribute('style', 'display:none')";
        List<WebElement> blocks = driver.findElements(By.xpath("//div[contains(@class,'accordion-body')]"));
        for (WebElement element : blocks){
            executor.executeScript(script, element);
        }
        WebElement buttonFeedBack = driver.findElement(By.xpath("//button[contains(@class,'feedback-btn')]"));
        executor.executeScript(script2, buttonFeedBack);
    }

    public void openFormCategory(String category){
        switch (category){
            case "auto":
                auto.click();
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='stepDesc']")));
                break;
            case "motorhome":
                motorHome.click();
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='stepDesc']")));
                break;
            case "moto":
                moto.click();
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='stepDesc']")));
                break;
            case "track":
                track.click();
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='stepDesc']")));
                break;
        }
    }

    public Map<String, List<String>> fillForm(HashMap<WebElement, String> map) throws InterruptedException {
        Map<String, List<String>> result = new HashMap<>();
        List<String> list;
        for (Map.Entry<WebElement, String> pair : map.entrySet()){
            if (isElementVisible(pair.getKey())) {
                try {
                    if (!pair.getKey().getTagName().equalsIgnoreCase("select")) {
                        if (pair.getKey().getAttribute("type").equalsIgnoreCase("checkbox")) {
                            choose(pair.getKey());
                            if (!pair.getKey().getAttribute("name").contains("optionNotifyme")
                                    && !pair.getKey().getAttribute("name").contains("optionMonitoring")) {
                                list = Arrays.asList("true");
                                result.put(ReplacementKey.getProperties(cutString(pair.getKey().getAttribute("name"), '[', ']')), list);
                            }
                        } else if (pair.getKey().getTagName().equalsIgnoreCase("ul")) {
                            result.putAll(chooseLi(pair.getKey(), 5));
                        }else if (pair.getValue().equalsIgnoreCase("autocomplete")){
//                            String value;
//                            pair.getKey().click();
//                            int countVariant = autocompleteList.size();
//                            if (countVariant > 0) {
//                                pair.getKey().click();
//                                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[contains(@style,'display: block;')]")));
//                                int current = random.nextInt(countVariant - 1 + 1) + 1;
//                                autocompleteList.get(current).click();
//                                value = pair.getKey().getAttribute("value");
//                            }else{
//                                String pre = "";
//                                if (pair.getKey().getAttribute("name").contains("submodelName")){
//                                    pre = result.get("model").get(0) + " ";
//                                }
//                                value = pre + Generate.uniqueString(4);
//                                type(pair.getKey(), value);
//                            }
//                            if (pair.getKey().getAttribute("data-targets") != null) {
//                                detecteHtml();
//                            }
                            String value = fillAutocomplete(pair.getKey(), result);
                            list = Arrays.asList(value);
                            result.put(ReplacementKey.getProperties(cutString(pair.getKey().getAttribute("name"), '[', ']')), list);
                        }
                        else {
                            type(pair.getKey(), pair.getValue());
                            list = Arrays.asList(pair.getValue());
                            result.put(ReplacementKey.getProperties(cutString(pair.getKey().getAttribute("name"), '[', ']')), list);
                        }
                    } else {
//                        int index = new Select(pair.getKey()).getOptions().size() > 1 ? 2 : 1;
//                        if (pair.getKey().getAttribute("name").contains("registrationYear")){index++;}
//                        new Select(pair.getKey()).selectByIndex(index);
//                        if (pair.getKey().getAttribute("data-targets") != null) {
//                            detecteHtml();
//                        }
//                        list = Arrays.asList(new Select(pair.getKey()).getFirstSelectedOption().getAttribute("value"));
                        list = Arrays.asList(chooseSelect(pair.getKey()));
                        result.put(ReplacementKey.getProperties(cutString(pair.getKey().getAttribute("name"), '[', ']')), list);
                    }
                } catch (Exception e) {
                    System.out.println("Skip... : " + pair.getKey().getAttribute("name"));
                    e.printStackTrace();
                }
            }
        }
        return result;
    }
    private String chooseSelect(WebElement element) throws InterruptedException {
        List<String> listOptions = new ArrayList<>();
        for (WebElement elem : new Select(element).getOptions()){
            if (elem.getAttribute("value") != null && !elem.getAttribute("value").equals("") && !elem.getAttribute("value").equals(null)){
                listOptions.add(elem.getAttribute("value"));
            }
        }
        int current = random.nextInt((listOptions.size() - 1) + 1);
        current = current > 0 ? current : 1;
        if (element.getAttribute("name").contains("registrationYear")){current++;}
        new Select(element).selectByValue(listOptions.get(current));
        if (element.getAttribute("data-targets") != null) {
            detecteHtml();
        }
        return listOptions.get(current);
    }

    private Map<String, List<String>> chooseLi(WebElement element, int count) throws Exception {
        List<WebElement> li = element.findElements(By.tagName("li"));
        String id = element.getAttribute("id");
        String key = driver.findElement(By.xpath("//ul[contains(@id,'" + id + "')]" +
                "/ancestor-or-self::div[@class='accordion-group']")).getAttribute("data-group");
        Map<String, List<String>> map = new HashMap<>();
        List<String> list = new ArrayList<>();
        int index = count >= li.size() ? li.size() : count ;
        for (int i = 0; i < index; i++) {
            String value = li.get(i).getAttribute("data-text");
            list.add(value);
            li.get(i).click();
        }
        map.put(key, list);
        return map;
    }

    private String fillAutocomplete(WebElement element, Map<String, List<String>> result) throws InterruptedException {
        String value;
        element.click();
        int countVariant = autocompleteList.size() - 1;
        if (countVariant > 0) {
            element.click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[contains(@style,'display: block;')]")));
            int current = random.nextInt((countVariant - 1) + 1) + 1;
            autocompleteList.get(current).click();
            value = element.getAttribute("value");
        }else{
            String pre = "";
            if (element.getAttribute("name").contains("submodelName")){
                pre = result.get("model").get(0) + " ";
            }
            value = pre + Generate.uniqueString(4);
            type(element, value);
        }
        if (element.getAttribute("data-targets") != null) {
            detecteHtml();
        }
        return value;
    }

    public Map<String, List<String>> chooseTitle(String title){
        Map<String, List<String>> map = new HashMap<>();
        List<String> list = null;
        for (WebElement element : titleButtons){
            if (element.getAttribute("value").equalsIgnoreCase(title)){
                if (!element.getAttribute("class").equalsIgnoreCase("active")){
                    element.click();
                }
                list = new ArrayList<>();
                list.add(element.getAttribute("value"));
                map.put("title", list);
            }
        }
        list = new ArrayList<>();
        list.add(stockField.getAttribute("value"));
        map.put("stock", list);
        return map;
    }

    public Map<String, List<String>> chooseCondition(String condition){
        Map<String, List<String>> map = new HashMap<>();
        List<String> list = null;
        for (WebElement element : conditionButtons){
            if (element.getAttribute("value").equalsIgnoreCase(condition)){
                if (!element.getAttribute("class").equalsIgnoreCase("active")){
                    element.click();
                }
                list = Arrays.asList(element.getAttribute("value"));
                map.put("condition", list);
            }
        }
        return map;
    }

    @Override
    public void checkSendSuccessful() {

    }

    public void openNext(){
        buttonNext.click();
    }


    public Map changeDate(Map<WebElement, String> map){
        Map<WebElement, String> res = new HashMap<>();
        for (Map.Entry<WebElement, String> m : map.entrySet()){
            if (m.getKey().getAttribute("name").contains("construction")){
                SimpleDateFormat sdf2 = new SimpleDateFormat("dd.MM.yyyy");
                String date = sdf2.format(new Date(System.currentTimeMillis() - 31536000000L));
                res.put(m.getKey(), date);
            }
            else {
                res.put(m.getKey(), m.getValue());
            }
        }
        return res;
    }

    public HashMap<WebElement, String> changeOdometer(Map<WebElement, String> map){
        HashMap<WebElement, String> res = new HashMap<>();
        for (Map.Entry<WebElement, String> m : map.entrySet()){
            if (m.getKey().getAttribute("name").contains("odometer")){
                int km = Integer.parseInt(Generate.uniqueNumber(3));
                if (km > 499){
                    km /= 2;
                }
                res.put(m.getKey(), String.valueOf(km));
            }else {
                res.put(m.getKey(), m.getValue());
            }
        }
        return res;
    }

    private String cutString(String str, char from, char to){
        int f = str.indexOf(from);
        int t = str.indexOf(to);
        String tmp = str.substring(f + 1, t);
        while (tmp.contains("[") || tmp.contains("]")){
            tmp = removeChar(tmp);
        }
        return tmp;
    }

    private String removeChar(String str){
        String res = "";
        for (int i = 0; i < str.length(); i++) {
            if (!(str.charAt(i) == '[') && !(str.charAt(i) == ']')){
                res += str.charAt(i);
            }
        }
        return res;
    }
}
