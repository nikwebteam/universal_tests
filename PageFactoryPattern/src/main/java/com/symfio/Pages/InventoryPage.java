package com.symfio.Pages;

import com.symfio.Pages.abstractPage.Page;
import com.symfio.Utils.ConfigProperties;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.*;

public class InventoryPage extends Page{

    private Random random = new Random();
    public InventoryPage(WebDriver driver) {
        super(driver);
    }
    private WebDriverWait wait = new WebDriverWait(driver,10,200);

    @FindAll({@FindBy(xpath = "//div[contains(@class,'search-multiple')]//*/input[@data-name]"),
              @FindBy(xpath = "//div[contains(@class,'search-multiple')]//*/select[@data-name]")})
    private List<WebElement> inputSearch;

    @FindBy(xpath = "//a[contains(@id,'search-submit')]")
    private WebElement buttonSearch;

    @FindBy(xpath = "//input[contains(@data-name,'stock')]")
    private WebElement inputStockNumber;

    @FindBy(xpath = "//select[contains(@data-name,'location') and contains(@data-type,'vehicle')]")
    private WebElement locationSelect;

    @FindBy(xpath = "//select[contains(@data-name,'make') and contains(@data-type,'vehicle')]")
    private WebElement makeSelect;

    @FindBy(xpath = "//select[contains(@data-name,'model') and contains(@data-type,'vehicle')]")
    private WebElement modelSelect;

    @FindAll({@FindBy(xpath = "//div[contains(@class,'vehicle-img')]/a")})
    private List<WebElement> carsId;

    @FindBy(xpath = "//select[contains(@data-name,'fuel') and contains(@data-type,'vehicle')]")
    private WebElement fuelSelect;

    @FindBy(xpath = "//select[contains(@data-name,'transmission') and contains(@data-type,'vehicle')]")
    private WebElement transmisionSelect;

    @FindBy(xpath = "//select[contains(@data-name,'exteriorColor') and contains(@data-type,'vehicle')]")
    private WebElement exteriorColorSelect;

    @FindAll({@FindBy(xpath = "//div[contains(@class,'widget-inventory-item')]")})
    List<WebElement> carsList;

    @FindBy(xpath = "//span[contains(@class,'pages-number')]")
    private WebElement countPageCars;

    @FindBy(xpath = "//a[contains(@class,'button-next')]")
    private WebElement buttonNext;

    @FindBy(className = "total-found")
    private WebElement totalCountCar;

    @FindAll({@FindBy(xpath = "//a[contains(@class,'button-readmore')]")})
    private List<WebElement> buttonsReadMore;

    @FindBy(xpath = "//span[contains(@class,'pagination-page')]/select")
    private WebElement pageSwitcher;

    @FindBy(xpath = "//li[contains(@class,'go-back')]/a")
    private WebElement buttonGoBack;

    @Override
    public void open() {
        driver.get(ConfigProperties.getProperties("disa.test") + "/fahrzeugsuche.html");
        driver.manage().window().maximize();
    }

    public void openDetailsCar(Object object){
        if (object == null){
            buttonsReadMore.get(random.nextInt(buttonsReadMore.size() - 1)).click();
        }
    }

    public void checkSearchSave(Map<String, String> result) throws Exception {
        if (result.containsKey("page")){
            Assert.assertEquals(result.get("page"),
                    new Select(pageSwitcher).getFirstSelectedOption().getAttribute("value"));
        }
        if (result.containsKey("Make")){
            Assert.assertEquals(result.get("Make"),
                    new Select(makeSelect).getFirstSelectedOption().getAttribute("value"));
        }
    }

    public void backToSearchResult(Boolean method){
        if (method){
            buttonGoBack.click();
            waitInventoryUpdate();
         return;
        }
        driver.navigate().back();

    }

    public String openLastPage(){
        int i = new Select(pageSwitcher).getOptions().size();
        if (i > 1) {
            new Select(pageSwitcher).selectByIndex(i - 1);
            return String.valueOf(i);
        }
        return String.valueOf(1);
    }

    public void showAllNewCars(){
        int count = Integer.parseInt(totalCountCar.getText().trim());
        String total = count > 50 ? "50": String.valueOf(count);
        String getParams = "?title=new&limit=" + total;
        String currentUrl = driver.getCurrentUrl();
        driver.get(currentUrl + getParams);
    }

    public void checkCarContainsCO2() throws Exception {
        for (WebElement car : carsList){
            if (!isElementPresent(car.findElement(By.xpath("./descendant::div[contains(@class,'offset-null row-emissionCO2')]")))){
                throw new Exception("New car does not have emission sticker CO2 !!!  "
                        + car.findElement(By.xpath("./descendant::div[contains(@class,'vehicle-img')]/a")).getAttribute("href"));
            }
        }
    }

    public void checkCarContainsVerb_comb() throws Exception {
        for (WebElement car : carsList){
            if (!isElementPresent(car.findElement(By.xpath("./descendant::div[contains(@class,'offset-null row-fuelConsumptionCombined')]")))){
                throw new Exception("New car does not have emission sticker verb.(komb.): !!!  "
                        + car.findElement(By.xpath("./descendant::div[contains(@class,'vehicle-img')]/a")).getAttribute("href"));
            }
        }
    }

    public void checkAllPhotoVisible() throws Exception {
        int count = Integer.parseInt(countPageCars.getText()) < 20 ? Integer.parseInt(countPageCars.getText()): 20;
        for(int i = 0 ; i <  count; i++) {
            for (WebElement car : carsList) {
                if (!isElementVisible(car.findElement(By.xpath("./descendant::div[contains(@class,'vehicle-img')]/a/img")))) {
                    throw new Exception("Photo for car does not visible !!!  "
                            + car.findElement(By.xpath("./descendant::div[contains(@class,'vehicle-img')]/a")).getAttribute("href"));
                }
            }
            buttonNext.click();
            wait.until(ExpectedConditions.stalenessOf(driver.findElement(By.xpath("//div[contains(@class,'vehicle-img')]/a/img"))));
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[contains(@class,'vehicle-img')]/a/img"))));
        }
    }

    @Override
    public void checkSendSuccessful() {}

    public String chooseExteriorColor(Integer index) throws InterruptedException {
        new Select(exteriorColorSelect).selectByIndex(index);
        Thread.sleep(2000);
        return new Select(exteriorColorSelect).getFirstSelectedOption().getAttribute("value");
    }

    public String chooseTransmision(Integer index) throws InterruptedException {
        new Select(transmisionSelect).selectByIndex(index);
        Thread.sleep(2000);
        return new Select(transmisionSelect).getFirstSelectedOption().getAttribute("value");
    }

    public String chooseFuel(Integer index) throws InterruptedException {
        new Select(fuelSelect).selectByIndex(index);
        Thread.sleep(2000);
        return new Select(fuelSelect).getFirstSelectedOption().getAttribute("value");
    }

    public String chooseModel(Integer index) throws InterruptedException {
        index = checkIndex(modelSelect, index);
        new Select(modelSelect).selectByIndex(index);
        Thread.sleep(2000);
        return new Select(modelSelect).getFirstSelectedOption().getAttribute("value");
    }

    public String chooseMake(Object index) throws InterruptedException {
        if (index.getClass().getSimpleName().equalsIgnoreCase("string")){
            for (WebElement element : new Select(makeSelect).getOptions()){
                if (element.getAttribute("value").contains((CharSequence) index)){
                    new Select(makeSelect).selectByValue(element.getAttribute("value"));
                    break;
                }
            }
            wait.until(ExpectedConditions.stalenessOf(driver.findElement(By.xpath("//div[contains(@class,'vehicle-img')]/a/img"))));
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[contains(@class,'vehicle-img')]/a/img"))));
            return new Select(makeSelect).getFirstSelectedOption().getAttribute("value");
        }
        index = checkIndex(makeSelect, (Integer) index);
        new Select(makeSelect).selectByIndex((Integer) index);
        Thread.sleep(3000);
        return new Select(makeSelect).getFirstSelectedOption().getAttribute("value");
    }

    public String chooseLocation(Integer index) throws InterruptedException {
        new Select(locationSelect).selectByIndex(index);
        Thread.sleep(2000);
        return new Select(locationSelect).getFirstSelectedOption().getAttribute("value");
    }

    public void fillStockInput(String number){
        inputStockNumber.clear();
        inputStockNumber.sendKeys(number);
    }

    public void implementsSearch(){
        buttonSearch.click();
    }

    public HashMap<WebElement, String> parseSearchFormFields(){
        HashMap<WebElement, String> map = new HashMap<>();
        for (WebElement pair : inputSearch){
            if (pair.getAttribute("data-name") != null && !pair.getAttribute("type").equalsIgnoreCase("checkbox")) {
                map.put(pair, pair.getAttribute("data-name"));
            }
        }
        return map;
    }

    public List<String> parseCarId(){
        List<String> idList = new ArrayList<>();
        for (WebElement e : carsId) {
            String str = e.getAttribute("href");
            int f = str.lastIndexOf('-');
            int t = str.indexOf(".html");
            idList.add(str.substring(f + 1, t));
        }
        return idList;
    }

    public HashMap<WebElement, String> generate(HashMap<WebElement, String> list){
        HashMap<WebElement, String> map = new HashMap<>();
        for (Map.Entry<WebElement, String> pair : list.entrySet()){
            if (!pair.getKey().getTagName().equalsIgnoreCase("select")) {
                if (createData(pair.getValue()) != null) {
                    map.put(pair.getKey(), createData(pair.getValue()));
                }
            }else{
                map.put(pair.getKey(), pair.getValue());
            }
        }
        return map;
    }

    private String cutString(String str, char from, char to){
        int f = str.indexOf(from);
        int t = str.indexOf(to);
        return str.substring(f + 1, t);
    }

    public Map<String, String> fillForm(HashMap<WebElement, String> map){
        Map<String, String> result = new HashMap<>();

        for (Map.Entry<WebElement, String> pair : map.entrySet()){
            try {
                if (!pair.getKey().getTagName().equalsIgnoreCase("select")) {
                    type(pair.getKey(), pair.getValue());
                    result.put(changeKey(pair.getKey().getAttribute("data-name")), pair.getValue());
                } else {
                    new Select(pair.getKey()).selectByIndex(1);
                    Thread.sleep(2000);
                    String value = new Select(pair.getKey()).getFirstSelectedOption().getAttribute("value");
                    result.put(changeKey(pair.getKey().getAttribute("data-name")), value);
                }
            }catch (Exception e){
                System.out.println("Skip... : " + pair.getKey().getAttribute("name"));
            }
        }
        return result;
    }

    private Integer checkIndex(WebElement element ,Integer index){
        if (index == null){
            int op = new Select(element).getOptions().size();
            return random.nextInt(op + 1);
        }
        return index;
    }

    private String createData(String fieldName){
        switch (fieldName.toLowerCase().trim()){
            case "year_min":
                return "2010";
            case "year_max":
                return "2015";
            case "mileage_min":
                return "30000";
            case "mileage_max":
                return "90000";
            case "price_current_min":
                return "15000";
            case "price_current_max":
                return "50000";
            case "stock":
                return String.valueOf(random.nextInt(99));
        }
        return null;
    }

    private String changeKey(String key){
        switch (key.toLowerCase().trim()){
            case "comments":
                return "message";
            case "contactname":
                return "name";
            case "contactemail":
                return "email";
            case "contactphone":
                return "phone";
            case "contactcity":
                return "address";
            case "contactaddress":
                return "address";
            case "contactagreementpersonaldata":
                return "agreementPersonaldata";
            case "vehiclevin":
                return "vin";
            case "vehicleengine":
                return "engine";
            case "vehicletransmission":
                return "transmission";
            case "vehiclemodel":
                return "model";
            case "vehiclemake":
                return "make";
            case "vehiclebody":
                return "body";
            case "vehicleodometermax":
                return "odometerMax";
            case "vehicleregistration":
                return "registration";
            case "tradeindescription":
                return "description";
        }
        return key;
    }
    private void waitInventoryUpdate(){
        //wait.until(ExpectedConditions.stalenessOf(driver.findElement(By.xpath("//div[contains(@class,'vehicle-img')]/a/img"))));
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[contains(@class,'vehicle-img')]/a/img"))));
    }

}
