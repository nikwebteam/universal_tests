package com.symfio.Pages;

import com.symfio.Data.UserData;
import com.symfio.Pages.abstractPage.Page;
import com.symfio.Pages.abstractPage.PageForm;
import com.symfio.Utils.ConfigProperties;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class LeadsPage extends PageForm {

    private LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
    private WebDriverWait wait = new WebDriverWait(driver, 10, 200);

//    @FindAll({@FindBy(xpath = "//form[contains(@action,'ajax')]//*[contains(@name,'[') and contains(@name,']')]")})
//    private List<WebElement> fullFormFields;

    @FindAll({@FindBy(xpath = "//form//*[contains(@name,'[') and contains(@name,']')]")})
    private List<WebElement> fullFormFields;

    @FindBy(xpath = "//a[contains(@class,'button submit')]")
    private WebElement buttonSendForm;

//    @FindBy(xpath = "//span[contains(@class,'source badge')]")
//    private WebElement element;

    @FindAll({@FindBy(xpath = "//span[contains(@class,'leads-title')]")})
    private List<WebElement> leads;

    @FindAll({@FindBy(xpath = "//p[@class = 'lead-author']/span")})
    private List<WebElement> listLeads;

    @FindAll({@FindBy(xpath = "//tbody[@role='alert']/descendant::tr")})
    private List<WebElement> LeadsOnPage;

    @FindBy(id = "attach_files")
    private WebElement buttonAttachPhoto;

    private int monitor = 0;

    public LeadsPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public void open() {
        driver.get(ConfigProperties.getProperties("tradein.url"));
        driver.manage().window().maximize();
    }

    @Override
    public void openModalForm() {

    }

    public void login(){
        //driver.get(ConfigProperties.getProperties("tradein.url") + "/admin/login");
        driver.get(ConfigProperties.getProperties("disa.test") + "/app_test.php/admin/login");
        loginPage.loginAs(new UserData("n.chupikov@symfio.de", "123456"));
        driver.get(ConfigProperties.getProperties("disa.test") + "/app_test.php/admin/de/requests");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[contains(@class,'source badge')]")));
    }

    public HashMap<WebElement, String> parseFullFormFields(){
        HashMap<WebElement, String> map = new HashMap<>();
        for (WebElement pair : fullFormFields){
            String value = cutString(pair.getAttribute("name"), '[', ']');
            map.put(pair, value);
        }
        return map;
    }

    public void checkCarLinkInLead() throws InterruptedException {
        leads.get(monitor).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class = 'preview']")));
        driver.findElement(By.xpath("//div[@class = 'preview']")).isDisplayed();
    }

//    public void searchLead(Map<String, List<String>> dataMap)throws Exception{
//        String id = dataMap.get("name").get(0);
//        for (int i = 0; i < 20; i++) {
//            for (WebElement elem: listLeads){
//                String name = elem.getText().trim();
//                if (name.equalsIgnoreCase(id)) {
//                    return;
//                }
//                monitor++;
//            }
//            System.out.println("Wait for lead 15 second ....");
//            Thread.sleep(15000);
//            monitor = 0;
//            driver.navigate().refresh();
//        }
//        throw new Exception("No Such lead !!!");
//    }

    public void searchLead(Map<String, List<String>> dataMap)throws Exception{
        String id = dataMap.get("contact_name").get(0);
        for (int i = 0; i < 20; i++) {
            for (WebElement lead : LeadsOnPage) {
                String contactName = lead.findElement(By.xpath("./descendant::p[contains(@class,'lead-author')]/span[@class='name']")).getText();
                if (contactName.trim().equals(id)) {
                    return;
                }
            }
            Thread.sleep(15000);
            driver.navigate().refresh();
        }
        throw new NoSuchElementException("No lead present with such name :" + id);
    }

    public void openLead() throws InterruptedException {
        WebElement lead = LeadsOnPage.get(0);
        lead.findElement(By.xpath("./descendant::*[contains(@class,'lead-icon')]")).click();
    }

    public void openLead(String name) throws InterruptedException {
        for (int i = 0; i < 20; i++) {
            for (WebElement lead : LeadsOnPage) {
                String contactName = lead.findElement(By.xpath("./descendant::p[contains(@class,'lead-author')]/span[@class='name']")).getText();
                if (contactName.trim().equals(name)) {
                    lead.findElement(By.xpath("./descendant::*[contains(@class,'lead-icon')]")).click();
                    return;
                }
            }
            Thread.sleep(15000);
            driver.navigate().refresh();
        }
        throw new NoSuchElementException("No lead present with such name : " + name);
    }

    public void deleteLead(String name) throws InterruptedException {

        for (WebElement lead : LeadsOnPage) {
            String contactName = lead.findElement(By.xpath("./descendant::p[contains(@class,'lead-author')]/span[@class='name']")).getText();
            if (contactName.trim().equals(name)) {
                lead.findElement(By.xpath("./descendant::a[contains(@class,'button-delete')]")).click();
                return;
            }
        }
        throw new NoSuchElementException("Fail deleted lead after test!!! Lead name : " + name);
    }

    public void openWindowAttachPhoto(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@id,'cke_answer')]")));
        buttonAttachPhoto.click();
    }

    public void checkWindowAttachPhotoIsOpened() throws Exception {
        Set<String> set = driver.getWindowHandles();
        if (set.size() == 1){
            throw new Exception("Error: window for attach photo in lead answer not opened!!!");
        }
        for (String handle : set) {
            driver.switchTo().window(handle);
        }
        driver.close();
    }

    public void sendTradeInForm(){
        buttonSendForm.click();
    }
    @Override
    public void checkSendSuccessful() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class,'success')]")));
    }

    private String cutString(String str, char from, char to){
        int f = str.indexOf(from);
        int t = str.indexOf(to);
        return str.substring(f + 1, t);
    }

    public void openByLeadType(String leadName){
        driver.get(ConfigProperties.getProperties("disa.test") + getLeadPage(leadName));
        driver.manage().window().maximize();
        for (String element : listclickHideCategory){
            if (isElementPresent(By.className(element))){
                driver.findElement(By.className(element)).click();
                try {
                    Thread.sleep(600);
                } catch (InterruptedException e) {

                }
            }
        }
    }
    private String getLeadPage(String leadName){
        switch (leadName){
            case "tradeIn":
                return  "/app_test.php/newtradein";
            case "callBack":
                return  "/app_test.php/call-back";
            case "feedBack":
                return  "/app_test.php/reference";
            case "tradein-vin":
                return "/app_test.php/tradein-vin";
            case "financing":
                return "/app_test.php/financing";
        }
        return "/";
    }

}
