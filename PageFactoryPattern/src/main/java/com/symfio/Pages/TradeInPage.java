package com.symfio.Pages;

import com.symfio.Pages.abstractPage.PageForm;
import com.symfio.Utils.ConfigProperties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashMap;
import java.util.List;

public class TradeInPage extends PageForm{

    private WebDriverWait wait = new WebDriverWait(driver, 10, 200);

    @FindAll({@FindBy(xpath = "//form[contains(@action,'ajax')]//*[contains(@name,'[') and contains(@name,']')]")})
    private List<WebElement> modalFormFields;

    @FindAll({@FindBy(xpath = "//form[not(contains(@action,'ajax'))]//*/input[contains(@name,'[') and contains(@name,']') and not(contains(@type,'hidden'))]"),
            @FindBy(xpath = "//form[not(contains(@action,'ajax'))]//*/select[contains(@name,'[') and contains(@name,']')]"),
            @FindBy(xpath = "//form[not(contains(@action,'ajax'))]//*/textarea[contains(@name,'[') and contains(@name,']')]")})
    private List<WebElement> fullFormFields;

    @FindBy(xpath = "//a[contains(@data-target,'callback')]")
    private WebElement buttonRequestCallBack;

    @FindBy(xpath = "//form[contains(@action,'ajax')]//*/a[contains(@class,'button submit')]")
    private WebElement buttonSendModalForm;

    public TradeInPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public void open() {
        driver.get(ConfigProperties.getProperties("gallery") + "/tradein");
        driver.manage().window().maximize();
    }

    @Override
    public void openModalForm() {
        driver.get(ConfigProperties.getProperties("gallery") + "/tradein");
        driver.manage().window().maximize();
        buttonRequestCallBack.click();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void sendTradeInForm(){
        driver.findElement(By.xpath("//*[@class='button submit']")).click();
    }

    public void sendRequestCallBackForm(){
        buttonSendModalForm.click();
    }

    @Override
    public void checkSendSuccessful() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class,'success')]")));
    }

    public HashMap<WebElement, String> parseFullFormFields(){
        HashMap<WebElement, String> map = new HashMap<>();
        for (WebElement pair : fullFormFields){
            if (isElementVisible(pair)) {
                String value = cutString(pair.getAttribute("name"), '[', ']');
                map.put(pair, value);
            }
        }
        return map;
    }

    public HashMap<WebElement, String> parseModalFormFields(){
        HashMap<WebElement, String> map = new HashMap<>();
        for (WebElement pair : modalFormFields){
            String value = cutString(pair.getAttribute("name"), '[', ']');
            map.put(pair, value);
        }
        return map;
    }

    private String cutString(String str, char from, char to){
        int f = str.indexOf(from);
        int t = str.indexOf(to);
        return str.substring(f + 1, t);
    }
}
