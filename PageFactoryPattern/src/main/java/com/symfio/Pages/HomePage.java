package com.symfio.Pages;

import com.symfio.Pages.abstractPage.Page;
import com.symfio.Utils.ConfigProperties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashMap;
import java.util.List;

public class HomePage extends Page {

    private WebDriverWait wait = new WebDriverWait(driver, 10, 200);

    public HomePage(WebDriver driver){
        super(driver);
    }

    @FindBy(xpath = "//a[contains(@class,'avatar')]")
    private WebElement buttonOpenLink;

    @FindBy(xpath = "//a[contains(@href,'logout')]")
    private WebElement linkLogOut;

    @FindBy(partialLinkText = "Login")
    private WebElement linkLogin;

    @FindAll({@FindBy(xpath = "//form//*[contains(@name,'[') and contains(@name,']')]")})
    private List<WebElement> subscribeFormFields;

    @Override
    public void open(){driver.get(ConfigProperties.getProperties("login.url"));}

    public void openSubscribePage(){
        driver.get(ConfigProperties.getProperties("disa.test"));
        driver.manage().window().maximize();
    }

    @Override
    public void checkSendSuccessful() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class,'success')]")));
    }

    public boolean isLoggedOut(){
        if (isElementPresent(linkLogin)){
            return true;
        }else {
            return false;
        }
    }

    public void sendSubscribeForm(){
        driver.findElement(By.id("singlebutton")).click();
    }

    public boolean isLoggedIn(){
        WebDriverWait wait = new WebDriverWait(driver, 10, 200);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//a[contains(@class,'avatar')]")));
        buttonOpenLink.click();
        boolean tmp = isElementPresent(linkLogOut);
        buttonOpenLink.click();
        return tmp;
    }

    public void logout(){
        buttonOpenLink.click();
        linkLogOut.click();
    }

    public HashMap<WebElement, String> parseFullFormFields(){
        HashMap<WebElement, String> map = new HashMap<>();
        for (WebElement pair : subscribeFormFields){
            String value = cutString(pair.getAttribute("name"), '[', ']');
            map.put(pair, value);
        }
        return map;
    }

    public HashMap<WebElement, String> parseModalFormFields(){
        HashMap<WebElement, String> map = new HashMap<>();
        List<WebElement> formFields = driver.findElements(By.xpath(
                "//form[contains(@action,'ajax')]//*/input[contains(@name,'[') and contains(@name,']') and not(contains(@type,'hidden'))]" +
                        "|//form[contains(@action,'ajax')]//*/select[contains(@name,'[') and contains(@name,']')]" +
                        "|//form[contains(@action,'ajax')]//*/textarea[contains(@name,'[') and contains(@name,']')]"));
        for (WebElement pair : formFields){
            String value = cutString(pair.getAttribute("name"), '[', ']');
            map.put(pair, value);
        }
        return map;
    }

    private String cutString(String str, char from, char to){
        int f = str.indexOf(from);
        int t = str.indexOf(to);
        return str.substring(f + 1, t);
    }

    public void openPageForm(String pageName){
        switch (pageName){
            case "tradeIn":
                break;
            case "requestCallBack":
                break;
            case "":
                break;
        }
    }
}
