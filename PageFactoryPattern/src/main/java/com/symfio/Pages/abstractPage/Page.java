package com.symfio.Pages.abstractPage;


import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.stream.Collectors;

public abstract class Page {

    protected WebDriver driver;
    protected Random random = new Random();
    private WebDriverWait wait;

    public Page(WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver,15,200);
    }

    protected void type(WebElement element, String text){
        element.clear();
        element.sendKeys(text);
    }

    public abstract void open();
    public abstract void checkSendSuccessful();

    protected void choose(WebElement element){
        if (!element.isSelected()) {
            element.click();
        }
    }

    public boolean isElementPresent(WebElement webElement){
        try {
            webElement.isDisplayed();
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean isElementPresent(WebElement webElement, String path){
        try {
            webElement.findElement(By.xpath(path)).isDisplayed();
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean isElementPresent(By by){
        try {
            driver.findElement(by).isDisplayed();
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean isElementVisible(WebElement webElement){
        try {
            Dimension dimension =  webElement.getSize();
            int height = dimension.getHeight();
            int width = dimension.getWidth();
            if (height > 0 || width > 0) {
                return true;
            }
            else{
                return false;
            }
        }catch (Exception e){
            return false;
        }
    }

    public void detecteHtml() throws InterruptedException {
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        String script = " return arguments[0].innerHTML;";
        WebElement body = driver.findElement(By.tagName("body"));
        String old = (String) executor.executeScript(script, body);
        String New = old;
        for (int i = 0; i < 20; i++) {
            New = (String) executor.executeScript(script, body);
            if (!old.equalsIgnoreCase(New)){
                break;
            }
            Thread.sleep(100);
        }
    }

    protected List<WebElement> getSelectOptions(WebElement select){
        return new Select(select).getOptions().stream().filter(option ->
                !option.getAttribute("value").equalsIgnoreCase(null)
                && option.getAttribute("value") != null
                && option.getAttribute("value") != ""
                && !(option.getAttribute("value").length() == 0)
                && (option.getAttribute("selected") == null)).collect(Collectors.toList());
    }

    protected void waitGrid(String type){
        switch (type){
            case "vehicle":
                wait.until(ExpectedConditions.attributeContains(By.xpath("//div[contains(@id,'list_grid_processing')]"),"style","hidden"));
                break;
            case "all":
                wait.until(ExpectedConditions.attributeContains(By.xpath("//div[contains(@class,'dataTables_processing')]"),"style","hidden"));
                break;
        }
    }
}
