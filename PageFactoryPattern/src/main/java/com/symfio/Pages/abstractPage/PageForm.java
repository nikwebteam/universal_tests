package com.symfio.Pages.abstractPage;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

public abstract class PageForm {

    protected WebDriver driver;
    protected List<String> listclickHideCategory = Arrays.asList("option-offer", "option-question", "option-testdrive", "option-tradein", "option-question");

    public PageForm(WebDriver driver){
        this.driver = driver;
    }

    public abstract void open();

    public abstract void openModalForm();

    public abstract void checkSendSuccessful();

    protected void type(WebElement element, String text){
        element.clear();
        element.sendKeys(text);
    }

    protected void choose(WebElement element){
        if (!element.isSelected()) {
            element.click();
        }
    }

    public boolean isElementPresent(WebElement webElement){
        try {
            webElement.isDisplayed();
            return true;
        }catch (NoSuchElementException e){
            return false;
        }
    }

    public boolean isElementPresent(By by){
        try {
            driver.findElement(by).isDisplayed();
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean isElementVisible(WebElement webElement){
        try {
            Dimension dimension =  webElement.getSize();
            int height = dimension.getHeight();
            int width = dimension.getWidth();
            if (height > 0 || width > 0) {
                return true;
            }
            else{
                return false;
            }
        }catch (NoSuchElementException e){
            return false;
        }
    }
}
