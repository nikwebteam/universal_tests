package com.symfio.Pages;

import com.symfio.Data.UserData;
import com.symfio.Pages.BackEndPages.MobileDePage;
import com.symfio.Pages.BackEndPages.VehiclesPage;
import com.symfio.Pages.abstractPage.Page;
import com.symfio.Utils.ConfigProperties;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainBackEndPage extends Page{

    public MainBackEndPage(WebDriver driver) {super(driver);}
    private Actions actions = new Actions(driver);
    private WebDriverWait wait = new WebDriverWait(driver, 10, 200);
    private UploadWizardPage uploadWizardPage = PageFactory.initElements(driver, UploadWizardPage.class);
    private LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);


    @FindBy(id = "menu_item_2")
    private WebElement buttonLeadsMenu;

    @FindBy(id = "menu_item_5")
    private WebElement buttonCarsMenu;

    @FindBy(id = "menu_item_22")
    private WebElement buttonSchnittstelle;

    @FindBy(id = "menu_item_3")
    private WebElement btnLeadsPage;

    @FindBy(id = "menu_item_6")
    private WebElement btnCarsPage;

    @FindBy(id = "menu_item_23")
    private WebElement btnMobileDe;

    @FindBy(xpath = "//a[contains(@href,'dashboard')]")
    private WebElement linkSignIn;

    @FindBy(id = "loginBtn")
    private WebElement buttonLogin;

    @FindBy(xpath = "//a[contains(@class,'button-edit')]")
    private WebElement buttonEdit;

    @FindAll({@FindBy(xpath = "//div[contains(@class,'s_fix_images')]/a/img")})
    private List<WebElement> Carslist;

    @FindAll({@FindBy(xpath = "//tbody[@role='alert']/descendant::tr")})
    private List<WebElement> listLeads;

    @FindBy(xpath = "//span[@class,'title-name']")
    private WebElement monitorLeadOpen;

    @FindBy(xpath = "//button[contains(@class,'feedback-btn')]")
    private WebElement buttonFeedBack;

    @Override
    public void open() {
        driver.get(ConfigProperties.getProperties("disa.test"));
        driver.manage().window().maximize();
        linkSignIn.click();
        loginPage.loginAs(new UserData("n.chupikov@symfio.de", "123456"));
        waitGrid("all");
    }

    @Override
    public void checkSendSuccessful() {}

    public LeadsPage openLeadsPage(){
        actions.moveToElement(buttonLeadsMenu);
        actions.click().build().perform();
        btnLeadsPage.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[contains(@class,'lead-author')]/span[@class='name']")));
        hideFeedBackButton();
        return PageFactory.initElements(driver, LeadsPage.class);
    }

    public MobileDePage openMobileDePage(){
        actions.moveToElement(buttonSchnittstelle);
        actions.click().build().perform();
        wait.until(ExpectedConditions.visibilityOf(btnMobileDe));
        btnMobileDe.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(@class,'status-mobile_de')]")));
        hideFeedBackButton();
        return PageFactory.initElements(driver, MobileDePage.class);
    }

    public VehiclesPage openCarsListPage(){
        actions.moveToElement(buttonCarsMenu);
        actions.click().build().perform();
        btnCarsPage.click();
        waitGrid("vehicle");
        //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class,'s_fix_images')]")));
        hideFeedBackButton();
        return PageFactory.initElements(driver, VehiclesPage.class);
    }
    public void openCarPage(){
        Random r = new Random();
        Carslist.get(r.nextInt(10)).click();
    }

    public UploadWizardPage openEditingPage(){
        buttonEdit.click();
        return PageFactory.initElements(driver, UploadWizardPage.class);
    }

    public List<NameValuePair> createPreviewLeadRequest(){
        List<NameValuePair> res = new ArrayList<>();
        res.add(new BasicNameValuePair("type", "load_lead_simple"));
        res.add(new BasicNameValuePair("id", "full_preview"));
        res.add(new BasicNameValuePair("host", ConfigProperties.getProperties("upload.wizard")));
        return res;
    }

    public String openLead(int index){
        String name = null;
        if (index < listLeads.size()) {
            name = listLeads.get(index - 1).findElement(By.xpath(
                    "./descendant::p[contains(@class,'lead-author')]/span[@class='name']")).getText();
            listLeads.get(index - 1).findElements(By.xpath("./descendant::*[contains(@class,'lead-icon')]")).get(0).click();
            //wait.until(ExpectedConditions.visibilityOf(monitorLeadOpen));
        }
        return name;
    }

    private void hideFeedBackButton(){
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        String script2 = "arguments[0].setAttribute('style', 'display:none')";
        executor.executeScript(script2, buttonFeedBack);
    }


}
