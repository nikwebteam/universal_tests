package com.symfio.Pages;

import com.symfio.Data.UserData;
import com.symfio.Pages.abstractPage.Page;
import com.symfio.Utils.ConfigProperties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class UsersPage extends Page {

    private LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
    private WebDriverWait wait = new WebDriverWait(driver, 10, 200);

    @FindAll({@FindBy(xpath = "//a[contains(@id,'useritem') and contains(@class,'statusOn')]")})
    public List<WebElement> AllActiveUser;

    @FindBy(xpath = "//a[contains(@href,'/admin/de/users')]")
    public WebElement linkPageUsers;

    @FindBy(xpath = "//span[contains(@class,'symfio-icon-dealership')]")
    public WebElement linkAutoHouse;

    @FindBy(xpath = "//th[contains(@class,'sorting') and contains(@aria-label,'Active')]")
    public WebElement buttonSortActive;

    @FindAll({@FindBy(xpath = "//a[contains(@id,'useritem')]")})
    public List<WebElement> listUsers;

    public UsersPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public void open() {
        loginPage.open();
        loginPage.loginAs(new UserData("n.chupikov@symfio.de", "123456"));
        driver.get(ConfigProperties.getProperties("disa.test") + "/app_test.php/admin/de/users");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[contains(@id,'useritem')]")));
    }

    @Override
    public void checkSendSuccessful() {

    }

    public void doSortingDescending(){
        String sortStatus = wait.until(ExpectedConditions.visibilityOf(buttonSortActive)).getAttribute("class");
        if (!sortStatus.contains("desc")){
            buttonSortActive.click();
            wait.until(ExpectedConditions.stalenessOf(listUsers.get(0)));
            wait.until(ExpectedConditions.visibilityOf(listUsers.get(0)));
        }
    }

    public boolean hasActive(){
        wait.until(ExpectedConditions.visibilityOf(listUsers.get(0)));
        for (WebElement pair : listUsers){
            if (pair.getAttribute("class").contains("statusOn")){
                return true;
            }
        }
        return false;
    }

    public void doActiveUsers(int count){
        wait.until(ExpectedConditions.visibilityOf(listUsers.get(0)));
        int partSize = listUsers.size() / count;
        for (int i = 0; i < listUsers.size(); i += partSize) {
            listUsers.get(i).click();
        }
    }

    public void checkDescendingSort() throws Exception {
        wait.until(ExpectedConditions.visibilityOf(listUsers.get(0)));
        for (int i = 0; i < listUsers.size(); i++) {
            if (i < AllActiveUser.size()){
                if (!listUsers.get(i).getAttribute("class").contains("statusOn")){
                    throw new Exception("Sort test Active user is Filed");
                }
            }else{
                if (listUsers.get(i).getAttribute("class").contains("statusOn")){
                    throw new Exception("Sort test Active user is Filed");
                }
            }
        }
    }

    public void doSortingAscending(){
        String sortStatus = wait.until(ExpectedConditions.visibilityOf(buttonSortActive)).getAttribute("class");
        if (!sortStatus.contains("asc")){
            buttonSortActive.click();
            wait.until(ExpectedConditions.stalenessOf(listUsers.get(0)));
            wait.until(ExpectedConditions.visibilityOf(listUsers.get(0)));
        }
    }

    public void checkAscendingSort() throws Exception {
        wait.until(ExpectedConditions.visibilityOf(listUsers.get(0)));
        for (int i = 0; i < listUsers.size(); i++) {
            if (i > listUsers.size() - AllActiveUser.size() - 1){
                if (!listUsers.get(i).getAttribute("class").contains("statusOn")){
                    throw new Exception("Sort test Active user is Filed");
                }
            }else{
                if (listUsers.get(i).getAttribute("class").contains("statusOn")){
                    throw new Exception("Sort test Active user is Filed");
                }
            }
        }
    }
}
