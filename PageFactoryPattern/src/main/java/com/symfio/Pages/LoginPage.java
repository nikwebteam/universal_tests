package com.symfio.Pages;


import com.symfio.Data.UserData;
import com.symfio.Pages.abstractPage.Page;
import com.symfio.Utils.ConfigProperties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends Page {

    @FindBy(xpath = "//a[contains(@href,'dashboard')]")
    private WebElement linkSignIn;

    @FindBy(id = "username")
    private WebElement fieldUserName;

    @FindBy(id = "password")
    private WebElement fieldPassword;

    @FindBy(id = "loginBtn")
    private WebElement buttonLogin;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public HomePage loginAs(UserData admin){
        type(fieldUserName, admin.name);
        type(fieldPassword, admin.password);
        buttonLogin.click();
        return PageFactory.initElements(driver, HomePage.class);
    }

    @Override
    public void open() {
        driver.get(ConfigProperties.getProperties("disa.test"));
        driver.manage().window().maximize();
        linkSignIn.click();
    }

    @Override
    public void checkSendSuccessful() {

    }
}
