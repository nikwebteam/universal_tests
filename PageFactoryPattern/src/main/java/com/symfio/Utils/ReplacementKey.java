package com.symfio.Utils;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;

public class ReplacementKey {

    private static Properties PROPERTIES;

    static {
        PROPERTIES = new Properties();
        URL props = ClassLoader.getSystemResource("ReplaceKey.properties");
        try {
            PROPERTIES.load(props.openStream());
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public static String getProperties(String key){
        if (PROPERTIES.containsKey(key)) {
            String result = PROPERTIES.getProperty(key);
            return result;
        }else {
            return key;
        }
    }

}
