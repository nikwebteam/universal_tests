package com.symfio.Utils;

import com.symfio.Data.UserData;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import sun.misc.BASE64Encoder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DataBaseUtil extends SuperMain{

    private static UserData user = new UserData("nick@mail.com", "nick123");
    private static String urlRequest = "http://autogalerie.disa.webteam.com.ua/rest-admin/test_info";

    public DataBaseUtil(WebDriver driver) {
        super(driver);
    }

    public static List<WebElement> isSame(List<WebElement> list){
        List<WebElement> copyList = list;

        for (int i = 0; i < list.size(); i++) {
            for (int j = list.size() - 1; j >= 0; j--) {
                if (list.get(i).getAttribute("href").trim().equals(list.get(j).getAttribute("href").trim())){
                    copyList.remove(j);
                }
            }
        }
        return copyList;
    }

    public static void waitForLeadPresent(List<NameValuePair> urlParameters) throws Exception {
        for (int i = 0; i < 20; i++) {
            String monitor = getDataFromDB(urlParameters);
            if (monitor.trim().equalsIgnoreCase("null")){
                if (i == 19){
                    throw new Exception("No such Lead in Data BaseClasses " + urlParameters);
                }
                System.out.println("Wait 15 sec... content = " + monitor);
                Thread.sleep(15000);
            }else {
                System.out.println("Have some DB content ...");
                return;
            }
        }
    }

    public static String removeChar(String s, Character c) {
        String tmp = s;
        String finalr = "";
        for (int i = 0; i < tmp.length(); i++) {
            if (tmp.charAt(i) == c){

            }else {
                finalr += tmp.charAt(i);
            }
        }
        return finalr;
    }

    public static String getDataFromDB(List<NameValuePair> urlParameters) throws IOException {

        // Create request
        HttpPost httpPost = new HttpPost(urlRequest);
        byte[] encodedPassword = ( user.name + ":" + user.password ).getBytes();
        BASE64Encoder encoder = new BASE64Encoder();
        httpPost.addHeader("Authorization","Basic " + encoder.encode( encodedPassword ));
        httpPost.setEntity(new UrlEncodedFormEntity(urlParameters));

        // Execute request
        HttpClient client = HttpClientBuilder.create().build();
        HttpResponse response = client.execute(httpPost);
        HttpEntity httpEntity = response.getEntity();

        // Get response from server
        BufferedReader reader = new BufferedReader(new InputStreamReader(httpEntity.getContent()));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line + "\n");
        }
        reader.close();
        return sb.toString().trim();
    }

    public static List<NameValuePair> addUrlParameters(Map<String, String> map){
        List<NameValuePair> urlParameters = new ArrayList<>();
        for (Map.Entry<String, String> pair : map.entrySet()){
            urlParameters.add(new BasicNameValuePair(pair.getKey(), pair.getValue()));
        }
        return urlParameters;
    }

    public static List<NameValuePair> createDbLeadRequest(Map<String, String> map, String host){
        List<NameValuePair> res = new ArrayList<>();
        res.add(new BasicNameValuePair("type", "load_lead_simple"));
        res.add(new BasicNameValuePair("host", host));
        for (Map.Entry<String, String> pair : map.entrySet()){
            if (pair.getKey().equalsIgnoreCase("name")){
                res.add(new BasicNameValuePair("id", pair.getValue()));
            }
        }
        return res;
    }

    public static List<NameValuePair> createDbLeadRequest2(Map<String, List<String>> map, String host){
        List<NameValuePair> res = new ArrayList<>();
        res.add(new BasicNameValuePair("type", "load_lead_simple"));
        res.add(new BasicNameValuePair("host", host));
        for (Map.Entry<String, List<String>> pair : map.entrySet()){
            if (pair.getKey().equalsIgnoreCase("contact_name") || pair.getKey().equalsIgnoreCase("contact_firstName")){
                res.add(new BasicNameValuePair("id", pair.getValue().get(0)));
            }
        }
        return res;
    }


    public static List<NameValuePair> createDbCarsRequest(List<String> carsId, String host, String info, String type){
        List<NameValuePair> urlParameters = new ArrayList<>();
        String finalType = type == null ? "vehicle":type;
        urlParameters.add(new BasicNameValuePair("type", finalType));
        urlParameters.add(new BasicNameValuePair("host", host));
        if (info != null) {
            urlParameters.add(new BasicNameValuePair("info[]", info));
            urlParameters.add(new BasicNameValuePair("info[]", ""));
        }
        for (String e : carsId){
            urlParameters.add(new BasicNameValuePair("id_simple[]", e));
        }
        return urlParameters;
    }

    public static List<NameValuePair> createDbCarsRequest(String carsId, String host, String info, String type){
        List<NameValuePair> urlParameters = new ArrayList<>();
        String finalType = type == null ? "vehicle":type;
        urlParameters.add(new BasicNameValuePair("type", finalType));
        urlParameters.add(new BasicNameValuePair("host", host));
        if (info != null) {
            urlParameters.add(new BasicNameValuePair("info[]", info));
            urlParameters.add(new BasicNameValuePair("info[]", ""));
        }
        urlParameters.add(new BasicNameValuePair("id_simple[]", carsId));
        return urlParameters;
    }

    public static Map parseDataByInfo(List<String> list, String data, String info ) throws Exception {

        switch (info) {
            case "exteriorColor":
                Map<String, String> carsExteriorColor = new HashMap<>();
                for (String x : list){
                    JSONObject jo = new JSONObject(data);
                    jo = jo.getJSONObject(x);
                    jo = jo.getJSONObject("specification");
                    jo = jo.getJSONObject("exterior");
                    String color = jo.getString("name");
                    carsExteriorColor.put(x, color);
                }
                return carsExteriorColor;
            case "transmission":
                Map<String, String> carsTransmission = new HashMap<>();
                for (String x : list){
                    JSONObject jo = new JSONObject(data);
                    jo = jo.getJSONObject(x);
                    jo = jo.getJSONObject("specification");
                    String transmission = jo.getString("transmission");
                    carsTransmission.put(x, transmission);
                }
                return carsTransmission;
            case "fuel":
                Map<String, String> carsFuel = new HashMap<>();
                for (String x : list){
                    JSONObject jo = new JSONObject(data);
                    jo = jo.getJSONObject(x);
                    jo = jo.getJSONObject("specification");
                    String fuel = jo.getString("fuel");
                    carsFuel.put(x, fuel);
                }
                return carsFuel;
            case "make":
                Map<String, String> makeByCars = new HashMap<>();
                for (String x : list){
                    JSONObject jo = new JSONObject(data);
                    jo = jo.getJSONObject(x);
                    jo = jo.getJSONObject("specification");
                    String make = jo.getString("make");
                    makeByCars.put(x, make.toLowerCase());
                }
                return makeByCars;
            case "model":
                Map<String, String> modelByCar = new HashMap<>();
                for (String x : list){
                    JSONObject jo = new JSONObject(data);
                    jo = jo.getJSONObject(x);
                    jo = jo.getJSONObject("specification");
                    String model = jo.getString("model");
                    modelByCar.put(x, model);
                }
                return modelByCar;
            case "stock":
                Map<String, String> stockByCars = new HashMap<>();
                for (String x : list){
                    JSONObject jo = new JSONObject(data);
                    jo = jo.getJSONObject(x);
                    String stock = jo.getString("stock");
                    stockByCars.put(x, stock);
                }
                return stockByCars;
            case "location":
                Map<String, String> locationByCars = new HashMap<>();
                for (String x : list){
                    JSONObject jo = new JSONObject(data);
                    jo = jo.getJSONObject(x);
                }
                return locationByCars;
            case "price":
                Map<String, Integer> priceByCar = new HashMap<>();
                for (String x : list) {
                    JSONObject jo = new JSONObject(data);
                    jo = jo.getJSONObject(x);
                    jo = jo.getJSONObject("price");
                    int value = jo.getInt("value");
                    priceByCar.put(x, value);
                }
                return priceByCar;
            case "odometer":
                Map<String, Integer> odometerByCar = new HashMap<>();
                for (String x : list) {
                    JSONObject jo = new JSONObject(data);
                    jo = jo.getJSONObject(x);
                    jo = jo.getJSONObject("odometer");
                    int value = jo.getInt("value");
                    odometerByCar.put(x, value);
                }
                return odometerByCar;
            case "last_added_inventory":
                Map<String, Date> dateAddedByCar = new HashMap<>();
                for (String x : list) {
                    JSONObject jo = new JSONObject(data);
                    jo = jo.getJSONObject(x);
                    String date = jo.getString("last_added_inventory");
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                    dateAddedByCar.put(x, simpleDateFormat.parse(date));
                }
                return dateAddedByCar;
            case "power":
                Map<String, Integer> listPowerKW = new HashMap<>();
                for (String pair : list){
                    JSONObject jo = new JSONObject(data);
                    jo = jo.getJSONObject(pair);
                    jo = jo.getJSONObject("specification");
                    jo = jo.getJSONObject("power");
                    listPowerKW.put(pair, jo.getInt("kw"));
                    listPowerKW.put(pair, jo.getInt("ps"));
                }
                return listPowerKW;
            case "MakeModel":
                Map<String, Map<String, Integer>> mapMakeModel = new HashMap<>();
                for (String pair : list){
                    Map<String, Integer> tmp = new HashMap<>();
                    JSONObject jo = new JSONObject(data);
                    jo = jo.getJSONObject(pair);
                    jo = jo.getJSONObject("specification");
                    String result = jo.getString("make") + jo.get("model");
                    jo = jo.getJSONObject("monthYear");
                    int year = jo.getInt("year");
                    tmp.put(result, year);
                    mapMakeModel.put(pair , tmp);
                }
                return mapMakeModel;
            default:
                return null;
        }
    }

    public static Map parseDataByInfo(List<String> list, String data, Map<String, String> info ) throws Exception {
        Map<String, Map<String, String>> result = new HashMap<>();
        JSONObject jo = new JSONObject(data);
        Map<String, JSONObject> dataByCar = new HashMap<>();
        for (String c : list){
            JSONObject tmp =  jo.getJSONObject(c);
            Map<String, String> car = new HashMap<>();
            for (Map.Entry<String, String> p : info.entrySet()){
                car.put(p.getKey(), tmp.get(p.getKey()).toString());
            }
            result.put(c, car);
        }
        return result;
    }

    public static <K, V> V getValueByKey(Map<K , V> map, K key){
        for (Map.Entry<K, V> m : map.entrySet()){
            if (m.getKey().equals(key)){
                return m.getValue();
            }
        }
        return null;
    }
    public static <K, V> V getValueFromMap(Map<K , V> map){
        for (Map.Entry<K, V> m : map.entrySet()){
            return m.getValue();
        }
        return null;
    }
    public static <K, V> K getKeyFromMap(Map<K , V> map){
        for (Map.Entry<K, V> m : map.entrySet()){
            return m.getKey();
        }
        return null;
    }
    public static <K, V> K getKeyByValue(Map<K , V> map, K value){
        for (Map.Entry<K, V> m : map.entrySet()){
            if (m.getValue().equals(value)){
                return m.getKey();
            }
        }
        return null;
    }
    public static <K, V> V getValueByKeyFromSecondMap(Map<K , Map<K, V>> map, K key){
        for (Map.Entry<K, Map<K, V>> m : map.entrySet()){
            if (m.getKey().equals(key)){
                return getValueFromMap(m.getValue());
            }
        }
        return null;
    }
    public static <K, V> K getKeyByValueFromSecondMap(Map<K , Map<K, V>> map, K key){
        for (Map.Entry<K, Map<K, V>> m : map.entrySet()){
            if (m.getKey().equals(key)){
                return getKeyFromMap(m.getValue());
            }
        }
        return null;
    }
}
