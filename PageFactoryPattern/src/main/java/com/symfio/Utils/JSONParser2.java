package com.symfio.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

public class JSONParser2 {

    public static HashMap<String, List<String>> parse(JSONObject jo){

        HashMap<String, List<String>> result = new HashMap<>();
        List<String> list;
        Iterator<String> iterator = jo.keys();
        while (iterator.hasNext()){
            String key = iterator.next();
            String value = String.valueOf(jo.get(key));
            if (value.startsWith("{")){
                if (!value.contains("[") || value.contains("[]") || value.contains("[") && value.contains("options")){
                    result.putAll(gumUp(new JSONObject(value), key));
                }
                else {
                    result.putAll(parse(new JSONObject(value)));
                }
            }
            else if (value.startsWith("[")){
                if (value.contains(":")) {
                    result.putAll(getArr(new JSONArray(value)));
                }
                else{
                    if (value.trim().length() > 2) {
                        result.putAll(getMass(new JSONArray(value), key));
                    }
                }
            }
            else {
                if (value.trim().equals("") || value.trim().equals(null)){
                    list = new ArrayList<>();
                    list.add("null");
                    result.put(key, list);
                }
                else {
                    list = new ArrayList<>();
                    list.add(value);
                    result.put(key, list);
                }
            }
        }
        return result;
    }

    private static HashMap<String, List<String>> getArr(JSONArray arr){

        HashMap<String, List<String>> result = new HashMap<>();
        List<String> list;
        for (int i = 0; i < arr.length(); i++) {
            JSONObject ob = (JSONObject) arr.get(i);
            Iterator<String> iterator = ob.keys();
            while (iterator.hasNext()) {
                String key = iterator.next();
                String value = String.valueOf(ob.get(key));
                if (value.startsWith("{")){
                    if (!value.contains("[")){
                        result.putAll(gumUp(new JSONObject(value), key));
                    }else {
                        result.putAll(parse(new JSONObject(value)));
                    }
                }
                else if (value.startsWith("[")){
                    if (value.contains(":")) {
                        result.putAll(getArr(new JSONArray(value)));
                    }
                    else{
                        result.putAll(getMass(new JSONArray(value), key));
                    }
                }
                else {
                    if (value.trim().equals("") || value.trim().equals(null)){
                        list = new ArrayList<>();
                        list.add("null");
                        result.put(key, list);
                    }else {
                        list = new ArrayList<>();
                        list.add(value);
                        result.put(key, list);
                    }
                }
            }
        }
        return result;
    }

    private static HashMap<String, List<String>> gumUp(JSONObject jo, String key){

        HashMap<String, List<String>> result = new HashMap<>();
        List<String> list;
        Iterator<String> iterator = jo.keys();
        while (iterator.hasNext()){
            String keyForValue = iterator.next();
            String keyFinal = key + "_" + keyForValue;
            String value = String.valueOf(jo.get(keyForValue));
            if (keyForValue.trim().equalsIgnoreCase("date")){
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                Timestamp ts = Timestamp.valueOf(value);
                value = sdf.format(ts);
                list = Arrays.asList(value);
                result.put(keyFinal, list);
            }
            else if (keyForValue.trim().equalsIgnoreCase("registration")){
                SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
                Long date = Long.parseLong(value);
                list = Arrays.asList(sdf.format(new java.sql.Date(date * 1000)));
                result.put(keyFinal, list);
            }
            else if (keyForValue.trim().equalsIgnoreCase("options")){
                result.putAll(getMass(new JSONArray(value), keyFinal));
            }
            else if (value.trim().equals("") || value.equals(null)){
                list = Arrays.asList("null");
                result.put(keyFinal, list);
            }
            else {
                list = Arrays.asList(value);
                result.put(keyFinal, list);
            }
        }
        return result;
    }

    private static HashMap<String, List<String>> getMass(JSONArray arr, String key){

        HashMap<String, List<String>> result = new HashMap<>();
        List<String> list = new ArrayList<>();
        for (int i = 0; i < arr.length(); i++) {
            list.add(String.valueOf(arr.get(i)));
        }
        result.put(key, list);
        return result;
    }
}
