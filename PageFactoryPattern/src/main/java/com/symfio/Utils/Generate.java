package com.symfio.Utils;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Generate {

    private static Random random = new Random();

    public static Long uniqueID(){
        Random random = new Random();
        Long rand = random.nextLong();
        if (rand < 0){
            rand = - rand;
        }
        return rand;
    }

    public static String uniqueNumber(int count){
        Random random = new Random();
        String result = "";
        int monitor = 0;
        while (monitor < count) {
            Long rand = random.nextLong();
            if (rand < 0){
                rand = - rand;
            }
            if (rand.byteValue() > 48 && rand.byteValue() < 58){
                result += (char) rand.byteValue();
                monitor++;
            }
        }
        return result.toUpperCase();
    }

    public static String uniqueHSN(){
        Random random = new Random();
        String result = "";
        int monitor = 0;
        while (monitor < 4) {
            Long rand = random.nextLong();
            if (rand < 0){
                rand = - rand;
            }
            if (rand.byteValue() > 96 && rand.byteValue() < 123
                    || rand.byteValue() > 64 && rand.byteValue() < 91
                    || rand.byteValue() >48 && rand.byteValue() < 58){
                result += (char) rand.byteValue();
                monitor++;
            }
        }
        return result.toUpperCase();
    }

    public static String uniqueTSN(){
        Random random = new Random();
        String result = "";
        int monitor = 0;
        while (monitor < 3) {
            Long rand = random.nextLong();
            if (rand < 0){
                rand = - rand;
            }
            if (rand.byteValue() > 96 && rand.byteValue() < 123
                    || rand.byteValue() > 64 && rand.byteValue() < 91
                    || rand.byteValue() >48 && rand.byteValue() < 58){
                result += (char) rand.byteValue();
                monitor++;
            }
        }
        return result.toUpperCase();
    }

    public static String uniqueVIN(){
        Random random = new Random();
        String result = "";
        int monitor = 0;
        while (monitor < 17) {
            Long rand = random.nextLong();
            if (rand < 0){
                rand = - rand;
            }
            if (rand.byteValue() > 64 && rand.byteValue() < 73
                    || rand.byteValue() > 73 && rand.byteValue() < 79
                    || rand.byteValue() >81 && rand.byteValue() < 91
                    || rand.byteValue() >48 && rand.byteValue() < 58 || rand.byteValue() == 80){
                result += (char) rand.byteValue();
                monitor++;
            }
        }
        return result.toUpperCase();
    }

    public static String uniqueString(){
        Random random = new Random();
        String result = "";
        int monitor = 0;
        while (monitor < 17) {
            Long rand = random.nextLong();
            if (rand < 0){
                rand = - rand;
            }
            if (rand.byteValue() > 96 && rand.byteValue() < 123
                    || rand.byteValue() > 64 && rand.byteValue() < 91){
                result += (char) rand.byteValue();
                monitor++;
            }
        }
        return result;
    }

    public static String uniqueColor(){
        List<String> colorList = Arrays.asList("Yellow", "Black", "Red",
                "Orange", "Green", "Brown", "White", "Pink", "Gold",
                "Silver", "Blue", "Gray");
        int current = random.nextInt((colorList.size() - 1) + 1);
        return colorList.get(current);
    }

    public static String uniqueString(int count){
        Random random = new Random();
        String result = "";
        int monitor = 0;
        while (monitor < count) {
            Long rand = random.nextLong();
            if (rand < 0){
                rand = - rand;
            }
            if (rand.byteValue() > 96 && rand.byteValue() < 123
                    || rand.byteValue() > 64 && rand.byteValue() < 91){
                result += (char) rand.byteValue();
                monitor++;
            }
        }
        return result;
    }
}
