package com.symfio.Utils;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class UrlResponse {

    public static HashMap<String, Integer> getParallelResponse(List<String> list, int threads, int countTasks) throws ExecutionException, InterruptedException {

        HashMap<String, Integer> response = new HashMap<>();

        ExecutorService executor = Executors.newFixedThreadPool(threads);

        List<Future<?>> futures = new ArrayList<>();

        int partSize = list.size() / countTasks;

        for (int i = 0; i < countTasks; i++) {
            final int finalI = i;
            futures.add(executor.submit(()->{
                int from = finalI * partSize;
                for (int j = from; j < from + partSize; j++) {
                    int res = getResponseCode(list.get(j));
                    response.put(list.get(j), res);
                }
            }));
        }
        for (Future<?> f : futures){
            f.get();
        }
        executor.shutdown();
        return response;
    }

    private static int getResponseCode(String urlString){
        int responseCode = 0;
        try {
            URL url = new URL(urlString);
            HttpURLConnection huc = (HttpURLConnection) url.openConnection();
            huc.setRequestMethod("GET");
            huc.connect();
            responseCode = huc.getResponseCode();
            huc.disconnect();
        }catch (Exception e){
            e.printStackTrace();
        }
        return responseCode;
    }

    public static void checkResponseCode(HashMap<String, Integer> map) throws Exception {
        StringBuffer buffer = new StringBuffer();

        for (HashMap.Entry<String, Integer> pair : map.entrySet()){
            if (pair.getValue() != 200){
                buffer.append("Server response error!!! - " + pair.getKey() + " : " + pair.getValue() + "\n");
            }
        }
        if (buffer.length() != 0){
            System.out.println(buffer.toString());
            throw new Exception("Test: ResponseServerTest-checkResponseForAllSiteLinks is Filed!!!");
        }
    }
}
