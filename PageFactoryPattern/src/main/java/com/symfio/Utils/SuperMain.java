package com.symfio.Utils;

import org.openqa.selenium.WebDriver;

public abstract class SuperMain {

    protected WebDriver driver;

    public SuperMain(WebDriver driver){
        this.driver = driver;
    }
}
