package com.symfio.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

public class JSONParser {

    public static HashMap<String, String> parse(JSONObject jo){

        HashMap<String, String> result = new HashMap<>();

        Iterator<String> iterator = jo.keys();
        while (iterator.hasNext()){
            String key = iterator.next();
            String value = String.valueOf(jo.get(key));
            if (value.startsWith("{")){
                if (!value.contains("[")){
                    result.putAll(gumUp(new JSONObject(value), key));
                }else {
                    result.putAll(parse(new JSONObject(value)));
                }
            }
            else if (value.startsWith("[")){
                if (value.contains(":")) {
                    result.putAll(getArr(new JSONArray(value)));
                }
                else{
                    if (value.trim().length() > 2) {
                        result.putAll(getMass(new JSONArray(value)));
                    }
                }
            }
            else {
                if (value.trim().equals("") || value.trim().equals(null)){
                    result.put(key, "null");
                }
                else {
                    result.put(key, value);
                }
            }
        }
        return result;
    }

    private static HashMap<String, String> getArr(JSONArray arr){

        HashMap<String, String> result = new HashMap<>();

        for (int i = 0; i < arr.length(); i++) {
            JSONObject ob = (JSONObject) arr.get(i);
            Iterator<String> iterator = ob.keys();
            while (iterator.hasNext()) {
                String key = iterator.next();
                String value = String.valueOf(ob.get(key));
                if (value.startsWith("{")){
                    if (!value.contains("[")){
                        result.putAll(gumUp(new JSONObject(value), key));
                    }else {
                        result.putAll(parse(new JSONObject(value)));
                    }
                }
                else if (value.startsWith("[")){
                    if (value.contains(":")) {
                        result.putAll(getArr(new JSONArray(value)));
                    }
                    else{
                        result.putAll(getMass(new JSONArray(value)));
                    }
                }
                else {
                    if (value.trim().equals("") || value.trim().equals(null)){
                        result.put(key, "null");
                    }else {
                        result.put(key, value);
                    }
                }
            }
        }
        return result;
    }

    private static HashMap<String, String> gumUp(JSONObject jo, String key){

        HashMap<String, String> result = new HashMap<>();

        Iterator<String> iterator = jo.keys();
        while (iterator.hasNext()){
            String keyForValue = iterator.next();
            String keyFinal = key + "_" + keyForValue;
            String value = String.valueOf(jo.get(keyForValue));
            if (value.trim().equals("") || value.equals(null)){
                result.put(keyFinal, "null");
            }
            else {
                result.put(keyFinal, value);
            }
        }
        return result;
    }

    private static HashMap<String, String> getMass(JSONArray arr){

        HashMap<String, String> result = new HashMap<>();
        for (int i = 0; i < arr.length(); i++) {
            result.put(String.valueOf(arr.get(i)), "true");
        }
        return result;
    }
}
