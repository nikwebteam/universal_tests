package com.symfio.Utils;

import java.util.List;
import java.util.Map;

public class Checker {

    public static <K, V> void check(Map<K, V> first, Map<K, V> second) throws Exception {
        for (Map.Entry<K, V> map : first.entrySet()){
            if (second.containsKey(map.getKey())){
                if ( !(map.getValue().equals(getValueByKey(second, map.getKey()))) ){
                    throw new Exception("Error... expected: " + map.getValue() + " - found: " + getValueByKey(second, map.getKey()));
                }
            }else{
                throw new Exception("Error... Not found such field: " + map.getKey());
            }
        }
    }

    public static <K, V> void checkEquals(Map<K, V> first, V second) throws Exception {
        for (Map.Entry<K, V> map : first.entrySet()){
            if (!map.getValue().equals(second)){
                throw new Exception("Error... for key : " + map.getKey() + "  expected: " + second + " - found: " + map.getValue());
            }
        }
    }

    public static void checkContains(Map<String, String> first, String second) throws Exception {
        for (Map.Entry<String, String> map : first.entrySet()){
            if (!map.getValue().contains(second)){
                throw new Exception("Error... expected: " + map.getValue() + " - found: " + second);
            }
        }
    }

    public static void checkContains(Map<String, List<String>> first, Map<String, List<String>> second) throws Exception {
        for (Map.Entry<String, List<String>> map : first.entrySet()){
            if (!second.containsKey(map.getKey())){
                throw new Exception("Error... expected: " + map.getValue() + " - found: " + second.get(map.getKey()));
            }
            List<String> firstList = map.getValue();
            List<String> secondList = second.get(map.getKey());
            if (!secondList.containsAll(firstList)){
                throw new Exception("Error... expected: " + firstList.toString() + " - found: " + secondList.toString());
            }
        }
    }

    private static <K, V> V getValueByKey(Map<K , V> map, K key){
        for (Map.Entry<K, V> m : map.entrySet()){
            if (m.getKey().equals(key)){
                return m.getValue();
            }
        }
        return null;
    }

    public static void checkAll(Map<String, Map<String, String>> allCars, Map<String, String> datasearch) throws Exception {
        for (Map.Entry<String, Map<String, String>> f : allCars.entrySet()){
            Map<String, String> car = f.getValue();
            for (Map.Entry<String, String> pair : car.entrySet()){
                String second = datasearch.get(pair.getKey());
                if (!pair.getValue().contains(second)){
                    throw new Exception("Error... expected: " + second + " - found: " + pair.getValue());
                }
            }
        }
    }

    public static Boolean onlyNumber(String string){
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) < 16 || string.charAt(i) > 25){
                return false;
            }
        }
        return true;
    }
}
