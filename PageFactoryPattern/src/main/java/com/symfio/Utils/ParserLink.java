package com.symfio.Utils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;


public class ParserLink{

    public static HashSet<String> parseLinksFromAllSite(String mainURL) throws InterruptedException, IOException {

        List<String> urls = new ArrayList<>();

        // parse all links from base page
        try {
            org.jsoup.nodes.Document document = Jsoup.connect(mainURL).timeout(0).get();
            Element mBody = document.body();
            Elements NewLink = mBody.getElementsByTag("a");
            for (Element link : NewLink) {
                urls.add(link.attr("href").trim());
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        HashSet<String> FirstPage = sortLinks(urls);
        System.out.println(mainURL + " : " + FirstPage);
        // parse links from all pages site
        HashSet<String> AllSiteLinks = new HashSet<>(FirstPage);

        for (String p : FirstPage){
            if (p.contains(mainURL)) {
                AllSiteLinks.addAll(parseLinksByPage(p));
            }
        }
        return AllSiteLinks;
    }

    // Method who parse all links by URl page
    public static HashSet<String> parseLinksByPage(String url) throws IOException, InterruptedException {

        HashSet<String> urls = new HashSet<>();
        try {
            org.jsoup.nodes.Document document = Jsoup.connect(url).timeout(0).get();
            Element Body = document.body();
            Elements AllLink = Body.getElementsByTag("a");
            for (Element link : AllLink)
            {
                urls.add(link.attr("href").trim());
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        System.out.println(url + " : " + sortLinks(urls));
        return sortLinks(urls);
    }

    public static List<String> createFakeUrl(int count){
        List<String> list = new ArrayList<>();
        String url = ConfigProperties.getProperties("login.url");
        for (int i = 0; i < count; i++) {
            list.add(url + "/some" + i);
        }
        return list;
    }

    private static HashSet<String> sortLinks(List<String> list){
        String baseUrl = ConfigProperties.getProperties("login.url");
        HashSet<String> res = new HashSet<>();
        for (String s : list){
            if( (s.contains("xml")) || (s.contains("#0")) || (s.equals("/")) ||
                    (s.contains("pdf")) || (s.startsWith("#"))
                    || (s.equals("http://symfio.de")) || (s.equals(baseUrl)) || (s.contains("url=http"))) {

            }else {
                if (s.contains("http")) {
                    res.add(s);
                }else{
                    res.add(baseUrl + s);
                }
            }
        }
        return res;
    }

    private static HashSet<String> sortLinks(HashSet<String> list){
        String baseUrl = ConfigProperties.getProperties("login.url");
        HashSet<String> res = new HashSet<>();
        for (String s : list){
            if( (s.contains("xml")) || (s.contains("#0")) || (s.equals("/")) ||
                    (s.contains("pdf")) || (s.startsWith("#"))
                    || (s.equals("http://symfio.de")) || (s.equals(baseUrl)) || (s.contains("url=http"))) {

            }else {
                if (s.contains("http")) {
                    res.add(s);
                }else{
                    res.add(baseUrl + s);
                }
            }
        }
        return res;
    }

    public static List<String> parse(String baseUrl)
    {
        List<String> FinalList = new ArrayList<>();
        try {
            URL url = new URL(baseUrl + "/sitemap.xml");
            URLConnection connection = url.openConnection();

            Document doc = parseXML(connection.getInputStream());
            NodeList descNodes = doc.getElementsByTagName("loc");

            List<String> firstList = new ArrayList<>();

            for (int i = 0; i < descNodes.getLength(); i++) {
                if(descNodes.item(i).getTextContent().contains("http")) {
                    firstList.add(descNodes.item(i).getTextContent());
                }else {
                    firstList.add(baseUrl + descNodes.item(i).getTextContent());
                }
            }

            FinalList = new ArrayList<>();
            for (String pair : firstList) {
                FinalList.addAll(parseByLink(pair, baseUrl));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return FinalList;
    }

    private static List<String> parseByLink(String link, String baseUrl) throws Exception {
        List<String> result = new ArrayList<>();

        URL url = new URL(link);
        URLConnection connection = url.openConnection();

        Document doc = parseXML(connection.getInputStream());
        NodeList descNodes = doc.getElementsByTagName("loc");

        for(int i=0; i<descNodes.getLength();i++)
        {
            if(descNodes.item(i).getTextContent().contains("http")) {
                result.add(descNodes.item(i).getTextContent());
            }else {
                result.add(baseUrl + descNodes.item(i).getTextContent());
            }
        }
        return result;
    }

    private static Document parseXML(InputStream stream)
            throws Exception
    {
        DocumentBuilderFactory objDocumentBuilderFactory = null;
        DocumentBuilder objDocumentBuilder = null;
        Document doc = null;
        try
        {
            objDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
            objDocumentBuilder = objDocumentBuilderFactory.newDocumentBuilder();

            doc = objDocumentBuilder.parse(stream);
        }
        catch(Exception ex)
        {
            throw ex;
        }

        return doc;
    }
}
