package com.symfio.ProblemsTest;

import com.symfio.Data.FormData;
import com.symfio.Pages.LeadsPage;
import com.symfio.BaseClasses.BasicTestCase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TradeInLeadCarsLinkTest extends BasicTestCase {

    private LeadsPage leadsPage;
    private FormData formData;

    @BeforeClass
    private void setUp(){
        leadsPage = PageFactory.initElements(getDriver(), LeadsPage.class);
        formData = PageFactory.initElements(getDriver(), FormData.class);
    }

    @Test  // DONE
    public void testUntitled() throws Exception {
        try {
            // open main page for tradeIn test
            leadsPage.openByLeadType("tradein-vin");
            // parse all form fields
            HashMap<WebElement, String> map = leadsPage.parseFullFormFields();
            // generate for fields data
            map = formData.generate(map);
            // fill All form fields
            Map<String, List<String>> data = formData.fillForm(map);
            // send form
            leadsPage.sendTradeInForm();
            // Check that form was sanded successful
            leadsPage.checkSendSuccessful();
            // login and open leads page
            leadsPage.login();
            // search such lead
            leadsPage.searchLead(data);
            // Check that cars link is present
            leadsPage.checkCarLinkInLead();
        }catch (Exception e){
            throw new Exception(e);
        }
    }

//    @Test  // DONE
//    public void testUntitled() throws Exception {
//        try {
//            // open main page for tradeIn test
//            leadsPage.open();
//            // parse all form fields
//            HashMap<WebElement, String> map = leadsPage.parseFullFormFields();
//            // generate for fields data
//            map = formData.generate(map);
//            // fill All form fields
//            Map<String, List<String>> data = formData.fillForm(map);
//            // send form
//            leadsPage.sendTradeInForm();
//            // Check that form was sanded successful
//            leadsPage.checkSendSuccessful();
//            // login and open leads page
//            leadsPage.login();
//            // search such lead
//            leadsPage.searchLead(data);
//            // Check that cars link is present
//            leadsPage.checkCarLinkInLead(data);
//        }catch (Exception e){
//            throw new Exception(e);
//        }
//    }

}
