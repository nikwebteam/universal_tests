package com.symfio.BaseClasses.testMethods;

import com.symfio.BaseClasses.BasicTestCase;
import com.symfio.Pages.LoginPage;
import com.symfio.Pages.MainBackEndPage;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.NoSuchElementException;

public class TestClass extends BasicTestCase {

    private LoginPage loginPage;
    private MainBackEndPage mainBackEndPage;

    @BeforeClass
    private void setUp(){
        loginPage = PageFactory.initElements(getDriver(), LoginPage.class);
        mainBackEndPage = PageFactory.initElements(getDriver(), MainBackEndPage.class);
    }

    @Test
    public void test() throws IOException, InterruptedException {

//        driver.get("http://amw.demo.symfio.de/fahrzeugsuche.html?location=5583e7f662f487700c00002c");
//        driver.manage().window().maximize();
//
//        List<String> carsId = inventoryPage.parseCarId();
//        List<NameValuePair> urlParam = DataBaseUtil.createDbCarsRequest(carsId, driver.getCurrentUrl(), null);
//        System.out.println(DataBaseUtil.getDataFromDB(urlParam));

    }
    public static boolean isElementVisible(WebElement webElement){
        try {
            Dimension dimension =  webElement.getSize();
            int height = dimension.getHeight();
            int width = dimension.getWidth();
            if (height > 0 || width > 0) {
                return true;
            }
            else{
                return false;
            }
        }catch (NoSuchElementException e){
            return false;
        }
    }
}

