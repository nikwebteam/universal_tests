package com.symfio.BaseClasses;


import com.symfio.Data.UserData;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.*;

import java.net.MalformedURLException;
import java.net.URL;

public class BasicTestCase {

    protected RemoteWebDriver driver;

    public UserData admin = new UserData("n.chupikov@symfio.de", "123456");

    @BeforeClass
    @Parameters("browser")
    protected void setDriver(String browser) {
        DesiredCapabilities capabilities = null;

        if (browser.equalsIgnoreCase("firefox")){
            capabilities = DesiredCapabilities.firefox();
            capabilities.setBrowserName(browser);
        }
        else if (browser.equalsIgnoreCase("chrome")) {
            capabilities = DesiredCapabilities.chrome();
            capabilities.setBrowserName("chrome");
        }
        if (capabilities == null){
            capabilities = DesiredCapabilities.chrome();
            capabilities.setBrowserName("chrome");
        }
        try {
            driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
       // driver.manage().timeouts().implicitlyWait(Long.parseLong(ConfigProperties.getProperties("imp.wait")), TimeUnit.SECONDS);
    }

    public WebDriver getDriver(){
        return driver;
    }

    @AfterClass
    public void quit(){
        driver.quit();
    }
}
