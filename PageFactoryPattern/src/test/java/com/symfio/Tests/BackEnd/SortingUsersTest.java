package com.symfio.Tests.BackEnd;

import com.symfio.BaseClasses.BasicTestCase;
import com.symfio.Pages.UsersPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class SortingUsersTest extends BasicTestCase {

    private UsersPage usersPage;

    @BeforeClass
    private void setUp(){
        usersPage = PageFactory.initElements(getDriver(), UsersPage.class);
    }

    @Test
    public void testUntitled() throws Exception {

        usersPage.open();
        usersPage.doSortingDescending();
        if (!usersPage.hasActive()){
            usersPage.doActiveUsers(5);
        }
        usersPage.checkDescendingSort();
        usersPage.doSortingAscending();
        usersPage.checkAscendingSort();
    }
}
