package com.symfio.Tests.BackEnd;

import com.symfio.BaseClasses.BasicTestCase;
import com.symfio.Pages.BackEndPages.VehiclesPage;
import com.symfio.Pages.MainBackEndPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class LinkForLeadInMessageTest extends BasicTestCase{

    private MainBackEndPage mainBackEndPage;
    private VehiclesPage vehiclesPage;

    @BeforeClass
    private void setUp(){
        mainBackEndPage = PageFactory.initElements(getDriver(), MainBackEndPage.class);
    }

    @Test
    public void testUntitled(){
        mainBackEndPage.open();
        vehiclesPage = mainBackEndPage.openCarsListPage();
        String carLink = vehiclesPage.parseCarLink();
        vehiclesPage.openCarPage(carLink);
        String leadId = vehiclesPage.parseLeadId();
        vehiclesPage.openLead(carLink + leadId);
        vehiclesPage.checkLeadIsOpened();
    }
}
