package com.symfio.Tests.BackEnd;


import com.symfio.BaseClasses.BasicTestCase;
import com.symfio.Data.FormData;
import com.symfio.Pages.UploadWizardPage;
import com.symfio.Utils.Checker;
import com.symfio.Utils.DataBaseUtil;
import com.symfio.Utils.JSONParser2;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class uploadWizardTest extends BasicTestCase{

    private UploadWizardPage uploadWizardPage;
    private FormData formData;

    @BeforeClass
    private void setUp(){
        uploadWizardPage = PageFactory.initElements(getDriver(), UploadWizardPage.class);
        formData = PageFactory.initElements(getDriver(), FormData.class);
    }

    @Test // DONE
    public void createCarAllFieldsTitleNew() throws Exception {
        try{
            uploadWizardPage.open();
            uploadWizardPage.openFormCategory("auto");
            // fill first page
            HashMap<WebElement, String> map = uploadWizardPage.parseFirstPageFields();
            map = formData.generate(map);
            map = uploadWizardPage.changeOdometer(map);
            Map<String, List<String>> data = uploadWizardPage.chooseTitle("new");
            data.putAll(uploadWizardPage.chooseCondition("excellent"));
            data.putAll(uploadWizardPage.fillForm(map));
            data.putAll(uploadWizardPage.chooseNDStrue());
            // fill second page
            uploadWizardPage.openNext();
            uploadWizardPage.openCollapseOptions();
            map = uploadWizardPage.parseSecondPageFields();
            map = formData.generate(map);
            data.putAll(uploadWizardPage.fillForm(map));
            // open and skip third page
            uploadWizardPage.openNext();
            // fill four page
            uploadWizardPage.openNext();
            map = uploadWizardPage.parseFourPageFields();
            map = formData.generate(map);
            data.putAll(uploadWizardPage.fillForm(map));
            uploadWizardPage.saveCar();
            uploadWizardPage.openDetailsTab(1);
            String carId = uploadWizardPage.parseCarId();
            List<NameValuePair> urlParameters = DataBaseUtil.createDbCarsRequest(carId, getDriver().getCurrentUrl(),null,"vehicle_simple");
            DataBaseUtil.waitForLeadPresent(urlParameters);
            String response = DataBaseUtil.getDataFromDB(urlParameters);
            JSONObject res = new JSONObject(response);
            HashMap<String, List<String>> pars = JSONParser2.parse(res.getJSONObject(carId));
            Checker.checkContains(data, pars);
        }catch (Exception e){
            throw new Exception(e);
        }
    }

    @Test // DONE
    public void createCarRequiredFieldsTitleNew() throws Exception {
        try {
            uploadWizardPage.ReOpen();
            uploadWizardPage.openFormCategory("auto");
            // fill first page
            Map<String, List<String>> data = uploadWizardPage.chooseTitle("new");
            data.putAll(uploadWizardPage.chooseCondition("good"));
            HashMap<WebElement, String> map = uploadWizardPage.parseRequiredFields();
            map = formData.generate(map);
            data.putAll(uploadWizardPage.fillForm(map));
            // Fill second page
            uploadWizardPage.openNext();
            uploadWizardPage.openCollapseOptions();
            map = uploadWizardPage.parseRequiredFields();
            map = formData.generate(map);
            data.putAll(uploadWizardPage.fillForm(map));
            // open and skip third page
            uploadWizardPage.openNext();
            // fill four page
            uploadWizardPage.openNext();
            map = uploadWizardPage.parseRequiredFields();
            map = formData.generate(map);
            data.putAll(uploadWizardPage.fillForm(map));
            uploadWizardPage.saveCar();
            uploadWizardPage.openDetailsTab(1);
            String carId = uploadWizardPage.parseCarId();
            List<NameValuePair> urlParameters = DataBaseUtil.createDbCarsRequest(carId, getDriver().getCurrentUrl(),null,"vehicle_simple");
            DataBaseUtil.waitForLeadPresent(urlParameters);
            String response = DataBaseUtil.getDataFromDB(urlParameters);
            JSONObject res = new JSONObject(response);
            HashMap<String, List<String>> pars = JSONParser2.parse(res.getJSONObject(carId));
            Checker.checkContains(data, pars);
        }catch (Exception e){
            throw new Exception(e);
        }
    }
}
