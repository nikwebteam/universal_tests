package com.symfio.Tests.BackEnd;

import com.symfio.BaseClasses.BasicTestCase;
import com.symfio.Data.FormData;
import com.symfio.Pages.LeadsPage;
import com.symfio.Pages.MainBackEndPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ButtonAddPhotoToLeadTest extends BasicTestCase{

    private FormData formData;
    private MainBackEndPage mainBackEndPage;
    private LeadsPage leadsPage;

    @BeforeClass
    private void setUp(){
        formData = PageFactory.initElements(getDriver(), FormData.class);
        mainBackEndPage = PageFactory.initElements(getDriver(), MainBackEndPage.class);
        leadsPage = PageFactory.initElements(getDriver(), LeadsPage.class);
    }

    @Test // DONE
    public void testUntitled() throws Exception {

        leadsPage.openByLeadType("callBack");
        HashMap<WebElement, String> map = leadsPage.parseFullFormFields();
        map = formData.generate(map);
        Map<String, List<String>> data = formData.fillForm(map);
        leadsPage.sendTradeInForm();
        String name = data.get("contact_name").get(0);
        // Open lead page
        mainBackEndPage.open();
        leadsPage = mainBackEndPage.openLeadsPage();
        leadsPage.searchLead(data);
        leadsPage.openLead(name);
        leadsPage.openWindowAttachPhoto();
        leadsPage.checkWindowAttachPhotoIsOpened();
    }
}
