package com.symfio.Tests.BackEnd;


import com.symfio.BaseClasses.BasicTestCase;
import com.symfio.Pages.BackEndPages.MobileDePage;
import com.symfio.Pages.MainBackEndPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CountCarsOnPageMobileDeTest extends BasicTestCase{

    private MainBackEndPage mainBackEndPage;
    private MobileDePage mobileDePage;

    @BeforeClass
    private void setUp(){
        mainBackEndPage = PageFactory.initElements(getDriver(), MainBackEndPage.class);
    }

    @Test // DONE
    public void testUntitled() throws Exception {

        mainBackEndPage.open();
        mobileDePage = mainBackEndPage.openMobileDePage();
        mobileDePage.countCurrentCars();
        mobileDePage.switchNextCountCarOnPage();
        mobileDePage.checkThatNewCount();
        mobileDePage.switchNextCountCarOnPage();
        mobileDePage.checkThatNewCount();
    }
}
