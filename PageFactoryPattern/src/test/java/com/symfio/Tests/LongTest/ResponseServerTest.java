package com.symfio.Tests.LongTest;

import com.symfio.Utils.ConfigProperties;
import com.symfio.Utils.ParserLink;
import com.symfio.Utils.UrlResponse;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;

public class ResponseServerTest{

    @Test
    public void checkResponseForAllSiteLinks() throws Exception {

        // Get all Links for Site
        List<String> list = ParserLink.parse(ConfigProperties.getProperties("link.response.url"));
        // Get response for all links
        HashMap<String, Integer> response = UrlResponse.getParallelResponse(list,8,8);
        // Check response
        UrlResponse.checkResponseCode(response);
    }
}
