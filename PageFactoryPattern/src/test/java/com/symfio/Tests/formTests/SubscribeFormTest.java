package com.symfio.Tests.formTests;

import com.symfio.Data.FormData;
import com.symfio.Pages.HomePage;
import com.symfio.BaseClasses.BasicTestCase;
import com.symfio.Utils.Checker;
import com.symfio.Utils.DataBaseUtil;
import com.symfio.Utils.JSONParser2;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubscribeFormTest extends BasicTestCase{

    private HomePage homePage;
    private  FormData formData;

    @BeforeClass
    private void setUp(){
        homePage = PageFactory.initElements(getDriver(), HomePage.class);
        formData = PageFactory.initElements(getDriver(), FormData.class);
    }

    @Test // DONE
    public void testUntitled() throws Exception {
        try {
            homePage.openSubscribePage();
            // get All form fields
            HashMap<WebElement, String> map = homePage.parseFullFormFields();
            // generate for fields data
            map = formData.generate(map);
            // fill All form fields
            Map<String, List<String>> data = formData.fillForm(map);
            // send form
            homePage.sendSubscribeForm();
            // Check that form was sanded successful
            homePage.checkSendSuccessful();
            // Create url parameters for check that such lead is present in DB
            List<NameValuePair> urlParam = DataBaseUtil.createDbLeadRequest2(data, driver.getCurrentUrl());
            // Wait for lead will receive
            DataBaseUtil.waitForLeadPresent(urlParam);
            // get information about this lead
            String response = DataBaseUtil.getDataFromDB(urlParam);
            // parse lead info
            Map<String, List<String>> listMap = JSONParser2.parse(new JSONObject(response));
            // check result
            Checker.checkContains(data, listMap);
        }catch (Exception e){
            throw new Exception(e);

        }
    }
}
