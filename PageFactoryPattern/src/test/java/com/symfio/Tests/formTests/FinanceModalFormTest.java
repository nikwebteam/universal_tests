package com.symfio.Tests.formTests;

import com.symfio.Data.FormData;
import com.symfio.Pages.DetailsCarPage;
import com.symfio.BaseClasses.BasicTestCase;
import com.symfio.Utils.Checker;
import com.symfio.Utils.DataBaseUtil;
import com.symfio.Utils.JSONParser2;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FinanceModalFormTest extends BasicTestCase {

    private DetailsCarPage detailsCarPage;
    private FormData formData;

    @BeforeClass
    public void setUp(){
        detailsCarPage = PageFactory.initElements(getDriver(), DetailsCarPage.class);
        formData = PageFactory.initElements(getDriver(), FormData.class);
    }


    @Test // DONE
    public void testUntitled() throws Exception {
        try {
            // open test page
            detailsCarPage.open();
            // open modal finance form
            detailsCarPage.openModalForm();
            // get All form fields
            HashMap<WebElement, String> map = detailsCarPage.parseFinancingFormFields();
            // generate for fields data
            map = formData.generate(map);
            // fill All form fieldsMonthsCount
            Map<String, List<String>> data = formData.fillForm(map);
            // send modal finance form
            detailsCarPage.sendFinanceModalForm();
            // Create url parameters for check that such lead is present in DB
            List<NameValuePair> urlParam = DataBaseUtil.createDbLeadRequest2(data, getDriver().getCurrentUrl());
            // Wait for lead will receive
            DataBaseUtil.waitForLeadPresent(urlParam);
            //get information about this lead
            String response = DataBaseUtil.getDataFromDB(urlParam);
            //parse lead info
            Map<String, List<String>> listMap = JSONParser2.parse(new JSONObject(response));
            // check result
            Checker.checkContains(data, listMap);
        }catch (Exception e){
            throw new Exception(e);
        }
    }
}
