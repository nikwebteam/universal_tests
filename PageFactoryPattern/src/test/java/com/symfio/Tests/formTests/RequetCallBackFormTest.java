package com.symfio.Tests.formTests;

import com.symfio.Data.FormData;
import com.symfio.Pages.HomePage;
import com.symfio.Pages.LeadsPage;
import com.symfio.BaseClasses.BasicTestCase;
import com.symfio.Utils.Checker;
import com.symfio.Utils.DataBaseUtil;
import com.symfio.Utils.JSONParser2;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RequetCallBackFormTest extends BasicTestCase{

   // private TradeInPage tradeInPage;
    private FormData formData;
    private HomePage homePage;
    private LeadsPage leadsPage;

    @BeforeClass
    private void setUp(){
     //   tradeInPage = PageFactory.initElements(getDriver(), TradeInPage.class);
        formData = PageFactory.initElements(getDriver(), FormData.class);
        homePage = PageFactory.initElements(getDriver(), HomePage.class);
        leadsPage = PageFactory.initElements(getDriver(),LeadsPage.class);
    }

    @Test // DONE
    public void testUntitled() throws Exception {
        try {
            // Open modal form
            leadsPage.openByLeadType("callBack");
            // get All form fields
            HashMap<WebElement, String> map = leadsPage.parseFullFormFields();
            // generate data for fields
            map = formData.generate(map);
            // fill All form fields
            Map<String, List<String>> data = formData.fillForm(map);
            // send callBack form
            leadsPage.sendTradeInForm();
            // Create url parameters for check that such lead is present in DB
            List<NameValuePair> urlParam = DataBaseUtil.createDbLeadRequest2(data, getDriver().getCurrentUrl());
            // Wait for lead will receive
            DataBaseUtil.waitForLeadPresent(urlParam);
            // get information about this lead
            String response = DataBaseUtil.getDataFromDB(urlParam);
            // parse lead info
            Map<String, List<String>> listMap = JSONParser2.parse(new JSONObject(response));
            // check result
            Checker.checkContains(data, listMap);
        }catch (Exception e){
            throw new Exception(e);
        }
    }

//    @Test // DONE
//    public void testUntitled() throws Exception {
//        try {
//            // Open modal form
//            tradeInPage.openModalForm();
//            // get All form fields
//            HashMap<WebElement, String> map = tradeInPage.parseModalFormFields();
//            // generate data for fields
//            map = formData.generate(map);
//            // fill All form fields
//            Map<String, List<String>> data = formData.fillForm(map);
//            // send callBack form
//            tradeInPage.sendRequestCallBackForm();
//            // Create url parameters for check that such lead is present in DB
//            List<NameValuePair> urlParam = DataBaseUtil.createDbLeadRequest2(data, getDriver().getCurrentUrl());
//            // Wait for lead will receive
//            DataBaseUtil.waitForLeadPresent(urlParam);
//            // get information about this lead
//            String response = DataBaseUtil.getDataFromDB(urlParam);
//            // parse lead info
//            Map<String, List<String>> listMap = JSONParser2.parse(new JSONObject(response));
//            // check result
//            Checker.checkContains(data, listMap);
//        }catch (Exception e){
//            throw new Exception(e);
//        }
//    }
}
