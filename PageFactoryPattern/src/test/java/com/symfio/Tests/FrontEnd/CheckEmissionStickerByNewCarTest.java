package com.symfio.Tests.FrontEnd;


import com.symfio.BaseClasses.BasicTestCase;
import com.symfio.Pages.InventoryPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CheckEmissionStickerByNewCarTest extends BasicTestCase{

    private InventoryPage inventoryPage;

    @BeforeClass
    public void setUp(){
        inventoryPage = PageFactory.initElements(getDriver(), InventoryPage.class);
    }

    @Test
    public void test_CO2_comb() throws Exception {

        inventoryPage.open();
        inventoryPage.showAllNewCars();
        inventoryPage.checkCarContainsCO2();
    }

    @Test
    public void test_Verb_comb() throws Exception {
        inventoryPage.checkCarContainsVerb_comb();
    }
}
