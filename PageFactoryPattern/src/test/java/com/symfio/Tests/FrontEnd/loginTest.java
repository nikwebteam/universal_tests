package com.symfio.Tests.FrontEnd;

import static org.testng.Assert.assertTrue;
import com.symfio.Pages.HomePage;
import com.symfio.Pages.LoginPage;
import com.symfio.BaseClasses.BasicTestCase;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class loginTest extends BasicTestCase {

    private LoginPage loginPage;
    private HomePage homePage;

    @BeforeClass
    private void setUp(){
        loginPage = PageFactory.initElements(getDriver(), LoginPage.class);
    }

    @Test
    public void testUntitled(){

        loginPage.open();
        homePage = loginPage.loginAs(admin);
        assertTrue(homePage.isLoggedIn());
        homePage.logout();
        assertTrue(homePage.isLoggedOut());
    }
}
