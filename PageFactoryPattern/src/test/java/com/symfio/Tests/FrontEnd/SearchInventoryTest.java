package com.symfio.Tests.FrontEnd;

import com.symfio.Pages.InventoryPage;
import com.symfio.BaseClasses.BasicTestCase;
import com.symfio.Utils.Checker;
import com.symfio.Utils.DataBaseUtil;
import org.apache.http.NameValuePair;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;
import java.util.Random;

public class SearchInventoryTest extends BasicTestCase{

    private Random random = new Random();
    private InventoryPage inventoryPage;

    @BeforeClass
    private void setUp(){
        inventoryPage = PageFactory.initElements(getDriver(), InventoryPage.class);
    }

    @Test // DONE
    public void searchByLocation() throws Exception {
        try {
            inventoryPage.open();
            String location = inventoryPage.chooseLocation(1);
            inventoryPage.implementsSearch();
            List<String> carsId = inventoryPage.parseCarId();
            List<NameValuePair> urlParam = DataBaseUtil.createDbCarsRequest(carsId, driver.getCurrentUrl(), null, null);
            String response = DataBaseUtil.getDataFromDB(urlParam);
            Map result = DataBaseUtil.parseDataByInfo(carsId, response, "location");
            Checker.checkContains(result, location);
        }catch (Exception e){
            throw new Exception(e);
        }
    }

    @Test //DONE
    public void searchByStockNumber() throws Exception {
        try {
            inventoryPage.open();
            String stockNumber = String.valueOf(random.nextInt(99));
            inventoryPage.fillStockInput(stockNumber);
            inventoryPage.implementsSearch();
            List<String> carsId = inventoryPage.parseCarId();
            List<NameValuePair> urlParam = DataBaseUtil.createDbCarsRequest(carsId, driver.getCurrentUrl(), null, null);
            String response = DataBaseUtil.getDataFromDB(urlParam);
            Map result = DataBaseUtil.parseDataByInfo(carsId, response, "stock");
            Checker.checkContains(result, stockNumber);
        }catch (Exception e){
            throw  new Exception(e);
        }
    }

    @Test //DONE
    public void searchByMake() throws Exception {
        try {
            inventoryPage.open();
            String make = inventoryPage.chooseMake(1);
            inventoryPage.implementsSearch();
            List<String> carsId = inventoryPage.parseCarId();
            List<NameValuePair> urlParam = DataBaseUtil.createDbCarsRequest(carsId, driver.getCurrentUrl(), null, null);
            String response = DataBaseUtil.getDataFromDB(urlParam);
            Map result = DataBaseUtil.parseDataByInfo(carsId, response, "make");
            Checker.checkEquals(result, make.toLowerCase());
        }catch (Exception e){
            throw new Exception(e);
        }
    }

    @Test // DONE
    public void searchByModel() throws Exception {
        try {
            inventoryPage.open();
            inventoryPage.chooseMake(1);
            String model = inventoryPage.chooseModel(1);
            inventoryPage.implementsSearch();
            List<String> carsId = inventoryPage.parseCarId();
            List<NameValuePair> urlParam = DataBaseUtil.createDbCarsRequest(carsId, driver.getCurrentUrl(), null, null);
            String response = DataBaseUtil.getDataFromDB(urlParam);
            Map result = DataBaseUtil.parseDataByInfo(carsId, response, "model");
            Checker.checkEquals(result, model);
        }catch (Exception e){
            throw new Exception(e);
        }
    }

    @Test // DONE
    public void searchByFuelType() throws Exception {
        try {
            inventoryPage.open();
            String fuelType = inventoryPage.chooseFuel(1);
            inventoryPage.implementsSearch();
            List<String> carsId = inventoryPage.parseCarId();
            List<NameValuePair> urlParam = DataBaseUtil.createDbCarsRequest(carsId, driver.getCurrentUrl(), null, null);
            String response = DataBaseUtil.getDataFromDB(urlParam);
            Map result = DataBaseUtil.parseDataByInfo(carsId, response, "fuel");
            Checker.checkEquals(result, fuelType);
        }catch (Exception e){
            throw new Exception(e);
        }
    }

    @Test // DONE
    public void searchByTransmissionType() throws Exception {
        try {
            inventoryPage.open();
            String transmission = inventoryPage.chooseTransmision(1);
            inventoryPage.implementsSearch();
            List<String> carsId = inventoryPage.parseCarId();
            List<NameValuePair> urlParam = DataBaseUtil.createDbCarsRequest(carsId, driver.getCurrentUrl(), null, null);
            String response = DataBaseUtil.getDataFromDB(urlParam);
            Map result = DataBaseUtil.parseDataByInfo(carsId, response, "transmission");
            Checker.checkEquals(result, transmission);
        }catch (Exception e){
            throw new Exception(e);
        }
    }

    @Test(priority = 10) // DONE
    public void searchByExteriorColor() throws Exception {
        try {
            inventoryPage.open();
            String color = inventoryPage.chooseExteriorColor(1);
            inventoryPage.implementsSearch();
            List<String> carsId = inventoryPage.parseCarId();
            List<NameValuePair> urlParam = DataBaseUtil.createDbCarsRequest(carsId, driver.getCurrentUrl(), null, null);
            String response = DataBaseUtil.getDataFromDB(urlParam);
            Map result = DataBaseUtil.parseDataByInfo(carsId, response, "exteriorColor");
            Checker.checkEquals(result, color);
        }catch (Exception e){
            throw new Exception(e);
        }

    }

//    @Test(priority = 10)
//    public void cobinateSearch() throws Exception {
//        try{
//            inventoryPage.open();
//            HashMap<WebElement, String> fields = inventoryPage.parseSearchFormFields();
//            fields = inventoryPage.generate(fields);
//            Map<String, String> data = inventoryPage.fillForm(fields);
//            System.out.println(data);
//            inventoryPage.implementsSearch();
//            List<String> carsId = inventoryPage.parseCarId();
//            List<NameValuePair> urlParam = DataBaseUtil.createDbCarsRequest(carsId, driver.getCurrentUrl(), null);
//            String response = DataBaseUtil.getDataFromDB(urlParam);
//            Map<String, Map<String, String>> result = DataBaseUtil.parseDataByInfo(carsId, response, data);
//            Checker.checkAll(result, data);
//        }catch (Exception e){
//            throw new Exception(e);
//        }finally {
//            driver.close();
//        }
//    }

}
