package com.symfio.inDeveloping;


import com.symfio.BaseClasses.BasicTestCase;
import com.symfio.Pages.InventoryPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class rememberSearchResultFrontEndTest extends BasicTestCase{

    private InventoryPage inventoryPage;
    private Map<String, String> result;

    @BeforeClass
    private void setUp(){
        inventoryPage = PageFactory.initElements(getDriver(), InventoryPage.class);
    }

    @Test
    public void testUntitled() throws Exception {
        inventoryPage.open();
        result = new HashMap<>();
        result.put("Make", inventoryPage.chooseMake("ford"));
        result.put("page", inventoryPage.openLastPage());
        inventoryPage.openDetailsCar(null);
        inventoryPage.backToSearchResult(true);
        inventoryPage.checkSearchSave(result);
    }

    @Test
    public void testMakeModel() throws Exception {
        inventoryPage.open();
        result = new HashMap<>();
        result.put("Make", inventoryPage.chooseMake("ford"));
        result.put("Model",inventoryPage.chooseModel(1));
        result.put("page",inventoryPage.openLastPage());
        inventoryPage.openDetailsCar(null);
        inventoryPage.backToSearchResult(true);
        inventoryPage.checkSearchSave(result);
    }
}
