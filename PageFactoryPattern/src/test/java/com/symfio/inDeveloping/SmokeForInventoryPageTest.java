package com.symfio.inDeveloping;

import com.symfio.BaseClasses.BasicTestCase;
import com.symfio.Pages.InventoryPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class SmokeForInventoryPageTest extends BasicTestCase{

    private InventoryPage inventoryPage;

    @BeforeClass
    private void setUp(){
        inventoryPage = PageFactory.initElements(getDriver(), InventoryPage.class);
    }

    @Test // need more information for create this test
    public void checkPhotoIsVisible() throws Exception {
        inventoryPage.open();
        inventoryPage.checkAllPhotoVisible();
    }
}
