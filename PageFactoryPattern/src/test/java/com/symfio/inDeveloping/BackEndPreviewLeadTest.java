package com.symfio.inDeveloping;

import com.symfio.BaseClasses.BasicTestCase;
import com.symfio.Pages.LoginPage;
import com.symfio.Pages.MainBackEndPage;
import com.symfio.Utils.DataBaseUtil;
import com.symfio.Utils.JSONParser2;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;

public class BackEndPreviewLeadTest extends BasicTestCase{

    private LoginPage loginPage;
    private MainBackEndPage mainBackEndPage;

    @BeforeClass
    private void setUp(){
        loginPage = PageFactory.initElements(getDriver(), LoginPage.class);
        mainBackEndPage = PageFactory.initElements(getDriver(), MainBackEndPage.class);
    }

    @Test  // Need Ira's help for creating this test
    public void testUntitled(){
        try {
            loginPage.open();
            loginPage.loginAs(admin);
            mainBackEndPage.openLeadsPage();
            List<NameValuePair> params = mainBackEndPage.createPreviewLeadRequest();
            String response = DataBaseUtil.getDataFromDB(params);
            System.out.println(response);
            HashMap<String, List<String>> data = JSONParser2.parse(new JSONObject(response));
            mainBackEndPage.openLead(1);

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
