package com.symfio.inDeveloping;

import com.symfio.BaseClasses.BasicTestCase;
import com.symfio.Pages.BackEndPages.VehiclesPage;
import com.symfio.Pages.MainBackEndPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

public class MultiSearchForBackEndTest extends BasicTestCase{

    private MainBackEndPage mainBackEndPage;
    private VehiclesPage vehiclesPage;

    @BeforeClass
    private void setUp(){mainBackEndPage = PageFactory.initElements(getDriver(), MainBackEndPage.class);}

    @Test // need get params for more car with vin code on the current page
    public void testUntitled() throws Exception {
        mainBackEndPage.open();
        vehiclesPage = mainBackEndPage.openCarsListPage();
        Map<String, List<String>> map = vehiclesPage.checkSearchResult();
        vehiclesPage.checkMultiSearch(map);
    }
}
