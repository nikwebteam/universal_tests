package ASOURCE.TestingPages.Adminka;

import ASOURCE.BaseClasses.PreAfterTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;

public class PageMitarbeiter{

    public void openPageMiterbeiter(WebDriver driver, WebDriverWait wait, String baseUrl){
        driver.get(baseUrl + "/admin/de/users");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//a[contains(@id,'useritem')]")));
    }

    public void sortActiveDescending(WebDriver driver, WebDriverWait wait){
        String sortStatus = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//th[contains(@class,'sorting')" +
                "and contains(@aria-label,'Active')]"))).getAttribute("class");
        if (!sortStatus.contains("desc")){
            driver.findElement(By.xpath(".//th[contains(@class,'sorting') and contains(@aria-label,'Active')]")).click();
            wait.until(ExpectedConditions.stalenessOf(driver.findElement(By.xpath(".//a[contains(@id,'useritem')]"))));
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(".//a[contains(@id,'useritem')]"))));
        }
    }
    public void sortActiveAscending(WebDriver driver, WebDriverWait wait){
        String sortStatus = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//th[contains(@class,'sorting')" +
                "and contains(@aria-label,'Active')]"))).getAttribute("class");
        if (!sortStatus.contains("asc")){
            driver.findElement(By.xpath(".//th[contains(@class,'sorting') and contains(@aria-label,'Active')]")).click();
            wait.until(ExpectedConditions.stalenessOf(driver.findElement(By.xpath(".//a[contains(@id,'useritem')]"))));
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(".//a[contains(@id,'useritem')]"))));
        }
    }
    public void checkDescendingSort(int count, WebDriverWait wait){
        List<WebElement> statusUsers = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(".//a[contains(@id,'useritem')]")));
        for (int i = 0; i < statusUsers.size(); i++) {
            if (i < count){
                Assert.assertTrue(statusUsers.get(i).getAttribute("class").contains("statusOn"));
            }else{
                Assert.assertFalse(statusUsers.get(i).getAttribute("class").contains("statusOn"));
            }
        }
    }
    public void checkAscendingSort(int count, WebDriver driver, WebDriverWait wait) throws InterruptedException {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//a[contains(@id,'useritem')]")));
        List<WebElement> statusUsers = driver.findElements(By.xpath(".//a[contains(@id,'useritem')]"));
        for (int i = 0; i < statusUsers.size(); i++) {
            if (i > statusUsers.size() - count - 1){
                Assert.assertTrue(statusUsers.get(i).getAttribute("class").contains("statusOn"));
            }else{
                Assert.assertFalse(statusUsers.get(i).getAttribute("class").contains("statusOn"));
            }
        }
    }
    public boolean checkHasActive(WebDriverWait wait) throws InterruptedException {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//a[contains(@id,'useritem')]")));
        List<WebElement> list =  wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(".//a[contains(@id,'useritem')]")));
        boolean monitor = false;
        for (WebElement mon : list){
            if (mon.getAttribute("class").contains("statusOn")){
                monitor = true;
            }
        }
        return monitor;
    }
    public void doActiveUsers(int count, WebDriverWait wait){
        List<WebElement> statusUsers = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(".//a[contains(@id,'useritem')]")));
        int part = statusUsers.size() / count;
        for (int i = 0; i < statusUsers.size(); i += part) {
            statusUsers.get(i).click();
        }
    }
    public int getActiveUsers(WebDriverWait wait){
        List<WebElement> list =  wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(".//a[contains(@id,'useritem')]")));
        int count = 0;
        for (WebElement pair : list){
            if (pair.getAttribute("class").contains("statusOn")){
                count ++;
            }
        }
        return count;
    }
}
