package ASOURCE.TestingPages.Adminka;

import ASOURCE.BaseClasses.PreAfterTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class PageLeads extends PreAfterTest{

    public void hasLead(String name) throws Exception {

        boolean res = false;

        for (int i = 0; i < 10; i++) {
            List<WebElement> listTitle = getDriver().findElements(By.ByClassName.className("lead-author"));
            for (WebElement pair : listTitle){
                if (pair.getText().contains(name)){
                    res = true;
                    System.out.println("Has Lead : " + pair.getText());
                }
            }
            if (res = true){
                i = 10;
                res = true;
            }else {
                Thread.sleep(27000);
                getDriver().navigate().refresh();
            }
        }

        if (!res){
            throw new Exception("Has not such Lead...");
        }
    }

    public void deleteLead(String name){
        List<WebElement> LeadsList = getDriver().findElements(By.xpath(".//tr[contains(@class,'odd') or contains(@class,'even')]"));

        WebElement suchLead = null;

        for (WebElement pair : LeadsList){
            String leadName = pair.findElement(By.xpath(".//p[contains(@class,'lead-author')]/span[1]")).getText();
            if (leadName.contains(name)){
                suchLead = pair;
            }
        }

        if (suchLead != null){
            suchLead.findElement(By.xpath(".//a[contains(@onclick,'Deleted')]")).click();
            getWait().until(ExpectedConditions.stalenessOf(getDriver().findElement(By.ByClassName.className("lead-author"))));
            getWait().until(ExpectedConditions.visibilityOf(getDriver().findElement(By.ByClassName.className("lead-author"))));
            System.out.println("Delete is Successful!!!");
        }
    }

}
