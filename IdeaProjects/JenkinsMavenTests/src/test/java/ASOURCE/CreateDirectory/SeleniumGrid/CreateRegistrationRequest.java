package ASOURCE.CreateDirectory.SeleniumGrid;

import org.openqa.grid.common.GridRole;
import org.openqa.grid.common.RegistrationRequest;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.Map;

public class CreateRegistrationRequest {

    private static RegistrationRequest request;

    public static RegistrationRequest create(DesiredCapabilities capabilities, Map<String, Object> map){
        request = new RegistrationRequest();
        request.setRole(GridRole.NODE);
        request.addDesiredCapability(capabilities);
        request.setConfiguration(map);
        return request;
    }
}
