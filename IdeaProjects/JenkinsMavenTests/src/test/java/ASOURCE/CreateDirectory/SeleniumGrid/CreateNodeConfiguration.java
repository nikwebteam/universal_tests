package ASOURCE.CreateDirectory.SeleniumGrid;


import org.openqa.grid.common.RegistrationRequest;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class CreateNodeConfiguration {

    private static DesiredCapabilities capabilities;
    private static Map<String, Object> nodeConfiguration;

    public static DesiredCapabilities createBrowserConfiguration(String name, String type, int instance){
        switch (name.toLowerCase()){
            case "chrome":
                DesiredCapabilities chrome = DesiredCapabilities.chrome();
                chrome.setBrowserName(name.toLowerCase());
                chrome.setCapability("maxInstances",instance);
                chrome.setCapability("seleniumProtocol",type);
                capabilities = new DesiredCapabilities(chrome);
                break;
            case "firefox":
                DesiredCapabilities firefox = DesiredCapabilities.firefox();
                firefox.setBrowserName("*" + name.toLowerCase());
                FirefoxProfile profile = new ProfilesIni().getProfile("Selenium");
                firefox.setCapability(FirefoxDriver.PROFILE, profile);
                firefox.setCapability("seleniumProtocol",type);
                firefox.setCapability("maxInstances",instance);
                capabilities = new DesiredCapabilities(firefox);
                break;
            case "internetexplorer":
                DesiredCapabilities ie = DesiredCapabilities.internetExplorer();
                ie.setBrowserName(name);
                ie.setCapability("maxInstances",instance);
                ie.setCapability("seleniumProtocol",type);
                capabilities = new DesiredCapabilities(ie);
                break;
        }
        return capabilities;
    }

    public static Map<String, Object> createNodeConfiguration(int port, URL url, String hubHost, int hubPort){
        nodeConfiguration = new HashMap<>();
        nodeConfiguration.put(RegistrationRequest.AUTO_REGISTER, true);
        nodeConfiguration.put(RegistrationRequest.HUB_HOST, hubHost);
        nodeConfiguration.put(RegistrationRequest.HUB_PORT, hubPort);
        nodeConfiguration.put(RegistrationRequest.PORT, port);

        nodeConfiguration.put(RegistrationRequest.PROXY_CLASS, "org.openqa.grid.selenium.proxy.DefaultRemoteProxy");
        nodeConfiguration.put(RegistrationRequest.MAX_SESSION, 5);
        nodeConfiguration.put(RegistrationRequest.CLEAN_UP_CYCLE, 2000);
        nodeConfiguration.put(RegistrationRequest.REMOTE_HOST, url);
        nodeConfiguration.put(RegistrationRequest.MAX_INSTANCES, 5);
        return nodeConfiguration;
    }
}
