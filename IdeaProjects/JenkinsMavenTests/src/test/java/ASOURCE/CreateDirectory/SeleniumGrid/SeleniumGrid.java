package ASOURCE.CreateDirectory.SeleniumGrid;

import org.openqa.grid.common.GridRole;
import org.openqa.grid.common.RegistrationRequest;
import org.openqa.grid.internal.utils.SelfRegisteringRemote;
import org.openqa.grid.web.Hub;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class SeleniumGrid {

    private static Hub hub;
    private static SelfRegisteringRemote remote;

    public static void startHub(int port) throws Exception {
        hub = new Hub(CreateHubConfiguration.create(port));
        hub.start();
    }

    public static void startNode(String name, int instance, int port, String type) throws Exception {

        URL remoteURL = new URL("http://" + hub.getHost() + ":" + port);

        DesiredCapabilities browser = CreateNodeConfiguration.createBrowserConfiguration(name, type, instance);
        Map<String, Object> map = CreateNodeConfiguration.createNodeConfiguration(port, remoteURL, hub.getHost(), hub.getPort());

        remote = new SelfRegisteringRemote(CreateRegistrationRequest.create(browser, map));

        remote.startRemoteServer();
        remote.startRegistrationProcess();
    }

    public static void startNodeWebDriver(String browserName, int port) throws Exception {
        RegistrationRequest req = new RegistrationRequest();
        req.setRole(GridRole.NODE);

        req.addDesiredCapability(registerNodeByName(browserName));

// ===================== Create Node configuration ==================================== //
        Map<String, Object> nodeConfiguration = new HashMap<>();
        nodeConfiguration.put(RegistrationRequest.AUTO_REGISTER, true);
        nodeConfiguration.put(RegistrationRequest.HUB_HOST, hub.getHost());
        nodeConfiguration.put(RegistrationRequest.HUB_PORT, hub.getPort());
        nodeConfiguration.put(RegistrationRequest.PORT, port);

        URL remoteURL = new URL("http://" + hub.getHost() + ":" + port);

        nodeConfiguration.put(RegistrationRequest.PROXY_CLASS, "org.openqa.grid.selenium.proxy.DefaultRemoteProxy");
        nodeConfiguration.put(RegistrationRequest.MAX_SESSION, 5);
        nodeConfiguration.put(RegistrationRequest.CLEAN_UP_CYCLE, 2000);
        nodeConfiguration.put(RegistrationRequest.REMOTE_HOST, remoteURL);
        nodeConfiguration.put(RegistrationRequest.MAX_INSTANCES, 5);

        req.setConfiguration(nodeConfiguration);

        SelfRegisteringRemote remote = new SelfRegisteringRemote(req);

        remote.startRemoteServer();
        remote.startRegistrationProcess();
    }

    private static DesiredCapabilities registerNodeByName(String name){
        switch (name){
            case "firefox":
                DesiredCapabilities firefox = DesiredCapabilities.firefox();
                firefox.setBrowserName("firefox");
                FirefoxProfile profile = new ProfilesIni().getProfile("Selenium");
                firefox.setCapability(FirefoxDriver.PROFILE, profile);
                firefox.setCapability("seleniumProtocol","WebDriver");
                firefox.setCapability("maxInstances",5);
                return firefox;
            case "chrome":
                DesiredCapabilities capabilities = DesiredCapabilities.chrome();
                capabilities.setCapability("browserName","chrome");
                capabilities.setCapability("maxInstances",5);
                capabilities.setCapability("seleniumProtocol","WebDriver");
                return capabilities;
        }
        return null;
    }
}