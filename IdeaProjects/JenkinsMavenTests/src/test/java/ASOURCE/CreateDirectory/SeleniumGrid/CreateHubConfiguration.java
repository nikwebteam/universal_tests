package ASOURCE.CreateDirectory.SeleniumGrid;


import org.openqa.grid.internal.utils.GridHubConfiguration;

public class CreateHubConfiguration {

    private static GridHubConfiguration configuration;

    public static GridHubConfiguration create(int port){
        configuration = new GridHubConfiguration();
        configuration.setHost(null);
        configuration.setPort(port);
        configuration.setTimeout(30000000);
        return configuration;
    }
}
