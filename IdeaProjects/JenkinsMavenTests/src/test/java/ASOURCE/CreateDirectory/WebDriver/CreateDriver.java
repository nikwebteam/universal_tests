package ASOURCE.CreateDirectory.WebDriver;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class CreateDriver {

    private static RemoteWebDriver threadDriver = null;

    public static RemoteWebDriver create(String name) throws MalformedURLException {
        threadDriver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), eachOne(name));
        return threadDriver;
    }

    private static DesiredCapabilities eachOne(String name){

        switch (name.toLowerCase()){
            case "chrome":
                DesiredCapabilities chrome = DesiredCapabilities.chrome();
                chrome.setBrowserName("chrome");
                return chrome;
            case "firefox":
                DesiredCapabilities firefox = DesiredCapabilities.firefox();
                firefox.setBrowserName("firefox");
                return firefox;
            case "internetexplorer":
                DesiredCapabilities ie = DesiredCapabilities.internetExplorer();
                ie.setBrowserName("internetExplorer");
                return ie;
        }
        return null;
    }

}
