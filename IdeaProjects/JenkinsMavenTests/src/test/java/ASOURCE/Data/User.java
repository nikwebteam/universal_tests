package ASOURCE.Data;

import ASOURCE.BaseClasses.PreAfterTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.io.IOException;


public class User extends PreAfterTest{

    private String name;
    private String password;

    public User(){}
    public User(String name, String password){
        this.name = name;
        this.password = password;
    }

    public void create(String name, String password){
        this.name = name;
        this.password = password;
    }
    public void logIn(WebDriver driver) throws IOException {
        driver.findElement(By.id("username")).sendKeys(name);
        driver.findElement(By.id("password")).sendKeys(password);
        driver.findElement(By.id("loginBtn")).click();
    }
    public void logOut(){
        getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//a[contains(@class,'avatar')]"))).click();
        getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//a[contains(@href,'logout')]")));
        getDriver().findElement(By.xpath(".//a[contains(@href,'logout')]")).click();
    }
}
