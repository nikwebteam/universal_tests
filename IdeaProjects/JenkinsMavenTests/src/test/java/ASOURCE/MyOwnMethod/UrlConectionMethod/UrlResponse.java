package ASOURCE.MyOwnMethod.UrlConectionMethod;

import ASOURCE.MyOwnMethod.Selenium;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class UrlResponse {

    public static int getResponseServer(String requestUrl){
        int responseCode = 0;
        try {
            URL url = new URL(requestUrl);
            HttpURLConnection huc = (HttpURLConnection) url.openConnection();
            huc.setRequestMethod("GET");
            huc.connect();
            responseCode = huc.getResponseCode();
            huc.disconnect();
        }catch (Exception e){
            e.printStackTrace();
        }
        return responseCode;
    }

    private static void getResponseParalell(List<String> source, int form, int destination, HashMap<String, Integer> result){
        for (int i = form; i < destination; i++) {
            int response = Selenium.getResponseCode(source.get(i));
            result.put(source.get(i), response);
        }
    }
    public static HashMap<String, Integer> getParallelResponse(List<String> list, int threads, int countTasks) throws ExecutionException, InterruptedException {

        HashMap<String, Integer> response = new HashMap<>();

        ExecutorService executor = Executors.newFixedThreadPool(threads);

        List<Future<?>> futures = new ArrayList<>();

        int partSize = list.size() / countTasks;

        for (int i = 0; i < countTasks; i++) {
            final int finalI = i;
            futures.add(executor.submit(()->{
                int from = finalI * partSize;
                for (int j = from; j < from + partSize; j++) {
                    int res = Selenium.getResponseCode(list.get(j));
                    response.put(list.get(j), res);
                }
            }));
        }
        for (Future<?> f : futures){
            f.get();
        }
        executor.shutdown();
        return response;
    }

    public static List<Integer> getResponseServer(HashSet<String> url){
        Iterator<String> iterator = url.iterator();
        List<Integer> result = new ArrayList<>();
        while (iterator.hasNext()){
            result.add(getResponseServer(iterator.next()));
        }
        return result;
    }

    public static int getStatusCode(String url) throws IOException {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet httpGet = new HttpGet(url);
        HttpResponse resp = client.execute(httpGet);
        return resp.getStatusLine().getStatusCode();
    }

    public static String getResponseUrl(String requestUrl){
        String responseUrl = "";
        try {
            URL url = new URL(requestUrl);
            HttpURLConnection huc = (HttpURLConnection) url.openConnection();
            huc.setRequestMethod("GET");
            huc.connect();
            responseUrl = huc.getURL().toString();
            huc.disconnect();
        }catch (Exception e){
            e.printStackTrace();
        }
        return responseUrl;
    }
}
