package ASOURCE.MyOwnMethod;

import ASOURCE.BaseClasses.PreAfterTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.HashMap;

public class Check {

    public void checkLogIn(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//a[contains(@class,'avatar')]")));
        driver.findElement(By.xpath(".//a[contains(@class,'avatar')]")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//a[contains(@href,'logout')]")));
        driver.findElement(By.xpath(".//a[contains(@href,'/admin/logout')]")).isDisplayed();
        driver.findElement(By.xpath(".//a[contains(@class,'avatar')]")).click();
        System.out.println("LogIn is Checked !!!");
    }

    public void checkLogOut(String url, WebDriver driver){
        Assert.assertEquals(url + "/", driver.getCurrentUrl());
    }

    public void checkResponseCode(HashMap<String, Integer> map) throws Exception {
        StringBuffer buffer = new StringBuffer();

        for (HashMap.Entry<String, Integer> pair : map.entrySet()){
            if (pair.getValue() != 200){
                buffer.append("Server response error!!! - " + pair.getKey() + " : " + pair.getValue() + "\n");
            }
        }
        if (buffer.length() != 0){
            System.out.println(buffer.toString());
            throw new Exception("Test: ResponseServerTest-checkResponseForAllSiteLinks is Filed!!!");
        }
    }
}
