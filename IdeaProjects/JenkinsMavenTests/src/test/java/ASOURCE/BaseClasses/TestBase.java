package ASOURCE.BaseClasses;

import ASOURCE.CreateDirectory.WebDriver.CreateDriver;
import ASOURCE.CreateDirectory.SeleniumGrid.SeleniumGrid;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class TestBase {

    protected ThreadLocal<RemoteWebDriver> threadDriver = null;
    protected static WebDriverWait wait = null;
    public StringBuffer verificationErrors = new StringBuffer();
    public String baseUrl;
    public String userName = "nick@mail.com";
    public String password = "nick123";
    public String urlRequest = "http://autogalerie.disa.webteam.com.ua/rest-admin/test_info";

    @BeforeSuite
    public void setUpGrid() throws Exception {
        SeleniumGrid.startHub(4444);
        //SeleniumGrid.startNodeWebDriver();
    }

    @BeforeClass
    public void readHostForTest() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("HostForTest.txt"));
        baseUrl = sc.nextLine();
        sc.close();
    }

    @BeforeMethod
    public void setUp() throws MalformedURLException {
        threadDriver = new ThreadLocal<>();
        threadDriver.set(CreateDriver.create("Chrome"));
    }

    public WebDriver getDriver() {
        return threadDriver.get();
    }

    public WebDriverWait getWait(){
        wait = new WebDriverWait(getDriver(), 15, 200);
        return wait ;
    }

    @AfterMethod
    public void closeBrowser() throws IOException {
        getDriver().quit();
        File flt = new File("TestResult.txt");
        FileWriter wrt = new FileWriter(flt);
        String verificationErrorString = verificationErrors.toString();
        wrt.append("\n");
        wrt.append(verificationErrorString);
        wrt.flush();
    }

//======================================================================================================================//
//======================================================================================================================//
// ================= Private Method ====================//

    public Boolean isElementPresent(By by)
    {
        WebElement webElement = getDriver().findElement(by);
        if (webElement != null){
            return true;
        }else {
            return false;
        }
    }

    public Boolean isElementPresent2(By by)
    {
        try {
            getDriver().findElement(by);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public int countElement(By by){
        int count = 0;
        List<WebElement> list = getDriver().findElements(by);
        for (WebElement i : list) {count++;}
        return count;
    }

    public static int getResponseCode(String urlString) throws IOException{
        URL url = new URL(urlString);
        HttpURLConnection huc = (HttpURLConnection)url.openConnection();
        huc.setRequestMethod("GET");
        //huc.setReadTimeout(10);
        //huc.connect();
        int responseCode = huc.getResponseCode();
        huc.disconnect();
        return responseCode;
    }

    public static String getResponseUrl(WebElement webElement) throws IOException{
        String urlString = webElement.getAttribute("href").trim();
        URL url = new URL(urlString);
        HttpURLConnection huc = (HttpURLConnection)url.openConnection();
        huc.setRequestMethod("GET");
        huc.connect();
        return huc.getURL().toString();
    }

    public void photoIsDisplayed(By by){
        List<WebElement> listPhoto = getDriver().findElements(by);
        for (WebElement i : listPhoto){
            try{
                i.isDisplayed();
            }catch (Exception e){
                verificationErrors.append(e.getMessage());
            }
        }
    }

    public void checkRequest(List<WebElement> list, int countChecks) throws IOException {

        List<WebElement> array = list;
        Collections.shuffle(array);

        for (int i = 0; i < countChecks; i++) {
            int statusCode = getResponseCode(array.get(i).getAttribute("href").trim());
            if (statusCode == 200 || statusCode >= 300 && statusCode < 400) {

            }else {
                try {
                    throw new Exception("Request code is: ");
                } catch (Exception e) {
                    verificationErrors.append(e.getMessage() + " " + statusCode + " " + array.get(i).getAttribute("href") + "\n");
                }
            }
        }
    }

    public void checkRequest(List<WebElement> list) throws IOException {

        for (WebElement array : list) {
            int statusCode = getResponseCode(array.getAttribute("href").trim());
            if (statusCode == 200 || statusCode >= 300 && statusCode < 400) {

            }else {
                try {
                    throw new Exception("Request code is: ");
                } catch (Exception e) {
                    verificationErrors.append(e.getMessage() + " " + statusCode + " " + array.getAttribute("href") + "\n");
                }
            }
        }
    }

    public void checkRequestAcceptFor(List<WebElement> list, List<String> acept, int count) throws IOException {

        List<WebElement> array = list;

        for(int i = 0; i < array.size(); i++){
            for (String acp : acept){
                if (array.get(i).getText() == acp){
                    array.remove(i);
                }
            }
        }
        Collections.shuffle(array);
        for ( int i = 0; i < count; i++ ) {
            int statusCode = getResponseCode(array.get(i).getAttribute("href").trim());
            if (statusCode == 200 || statusCode >= 300 && statusCode < 400) {

            }else {
                try {
                    throw new Exception("Request code is: ");
                } catch (Exception e) {
                    verificationErrors.append(e.getMessage() + " " + statusCode + " " + array.get(i).getAttribute("href").trim() + "\n");
                }
            }
        }
    }

    public List<WebElement> isSame(List<WebElement> list){
        List<WebElement> copyList = list;

        for (int i = 0; i < list.size(); i++) {
            for (int j = list.size() - 1; j >= 0; j--) {
                if (list.get(i).getAttribute("href").trim().equals(list.get(j).getAttribute("href").trim())){
                    copyList.remove(j);
                }
            }
        }
        return copyList;
    }

    public static String removeChar(String s, Character c) {
        String tmp = s;
        String finalr = "";
        for (int i = 0; i < tmp.length(); i++) {
            if (tmp.charAt(i) == c){

            }else {
                finalr += tmp.charAt(i);
            }
        }
        return finalr;
    }

    public List<String> parseCarId(By by){

        List<WebElement> elementList = getDriver().findElements(by);

        List<String> idList = new ArrayList<>();

        for (WebElement e : elementList) {
            String s = e.getAttribute("href");
            for (int i = 1; i < s.length(); i++) {
                if (s.charAt(i - 1) == '-'){
                    if ( !(s.substring(i).contains("-")) ) {
                        int count = i;
                        String res = "";
                        while (s.charAt(count) != '.') {
                            res += s.charAt(count);
                            count++;
                        }
                        idList.add(res);
                    }
                }
            }
        }
        return idList;
    }

    public List<String> parseCarId(String string){

        List<String> idList = new ArrayList<>();

        String s = string;
        for (int i = 1; i < s.length(); i++) {
            if (s.charAt(i - 1) == '-'){
                if ( !(s.substring(i).contains("-")) ) {
                    int count = i;
                    String res = "";
                    while (s.charAt(count) != '.') {
                        res += s.charAt(count);
                        count++;
                    }
                    idList.add(res);
                }
            }
        }
        return idList;
    }

    public String getPageHost(){
        String s = baseUrl;
        s = s.substring(7);
        return removeChar(s,'/');
    }

    public List<NameValuePair> getUrlParam(List<String> list) throws UnsupportedEncodingException {
        List<NameValuePair> copy = new ArrayList<>();
        for (String e : list){
            copy.add(new BasicNameValuePair("id_simple[]", e));
        }
        return copy;
    }

    public String getDataFromBase(List<String> listIDCars, String info) throws IOException {
        // Create http Request for cars id by price
        List<NameValuePair> urlParameters = new ArrayList<>();

        urlParameters.add(new BasicNameValuePair("type", "vehicle"));
        urlParameters.add(new BasicNameValuePair("host", getPageHost()));
        urlParameters.addAll(getUrlParam(listIDCars));
        if (info != null) {
            urlParameters.add(new BasicNameValuePair("info[]", info));
            urlParameters.add(new BasicNameValuePair("info[]", ""));
        }

        HttpClient client = HttpClientBuilder.create().build();
        HttpPost httpPost = new HttpPost(urlRequest);
        byte[] encodedPassword = ( userName + ":" + password ).getBytes();
        BASE64Encoder encoder = new BASE64Encoder();
        httpPost.addHeader("Authorization","Basic " + encoder.encode( encodedPassword ));
        httpPost.setEntity(new UrlEncodedFormEntity(urlParameters));

        HttpResponse response = client.execute(httpPost);
        HttpEntity httpEntity = response.getEntity();

        BufferedReader reader = new BufferedReader(new InputStreamReader(httpEntity.getContent()));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line + "\n");
        }
        reader.close();
        return sb.toString();
    }

    public Map parseDataByInfo(List<String> list, String data, String info ) throws ParseException, JSONException {

        switch (info) {
            case "price":
                Map<String, Integer> priceByCar = new HashMap<>();
                for (String x : list) {
                    JSONObject jo = new JSONObject(data);
                    jo = jo.getJSONObject(x);
                    jo = jo.getJSONObject("price");
                    int value = jo.getInt("value");
                    priceByCar.put(x, value);
                }
                return priceByCar;
            case "odometer":
                Map<String, Integer> odometerByCar = new HashMap<>();
                for (String x : list) {
                    JSONObject jo = new JSONObject(data);
                    jo = jo.getJSONObject(x);
                    jo = jo.getJSONObject("odometer");
                    int value = jo.getInt("value");
                    odometerByCar.put(x, value);
                }
                return odometerByCar;
            case "last_added_inventory":
                Map<String, Date> dateAddedByCar = new HashMap<>();
                for (String x : list) {
                    JSONObject jo = new JSONObject(data);
                    jo = jo.getJSONObject(x);
                    String date = jo.getString("last_added_inventory");
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                    dateAddedByCar.put(x, simpleDateFormat.parse(date));
                }
                return dateAddedByCar;
            case "power":
                Map<String, Integer> listPowerKW = new HashMap<>();
                for (String pair : list){
                    JSONObject jo = new JSONObject(data);
                    jo = jo.getJSONObject(pair);
                    jo = jo.getJSONObject("specification");
                    jo = jo.getJSONObject("power");
                    listPowerKW.put(pair, jo.getInt("kw"));
                    listPowerKW.put(pair, jo.getInt("ps"));
                }
                return listPowerKW;
            case "MakeModel":
                Map<String, Map<String, Integer>> mapMakeModel = new HashMap<>();
                for (String pair : list){
                    Map<String, Integer> tmp = new HashMap<>();
                    JSONObject jo = new JSONObject(data);
                    jo = jo.getJSONObject(pair);
                    jo = jo.getJSONObject("specification");
                    String result = jo.getString("make") + jo.get("model");
                    jo = jo.getJSONObject("monthYear");
                    int year = jo.getInt("year");
                    tmp.put(result, year);
                    mapMakeModel.put(pair , tmp);
                }
                return mapMakeModel;
            default:
                return null;
        }
    }

    public <K, V> V getValueByKey(Map<K , V> map, K key){
        for (Map.Entry<K, V> m : map.entrySet()){
            if (m.getKey().equals(key)){
                return m.getValue();
            }
        }
        return null;
    }
    public <K, V> V getValueFromMap(Map<K , V> map){
        for (Map.Entry<K, V> m : map.entrySet()){
            return m.getValue();
        }
        return null;
    }
    public <K, V> K getKeyFromMap(Map<K , V> map){
        for (Map.Entry<K, V> m : map.entrySet()){
            return m.getKey();
        }
        return null;
    }
    public <K, V> K getKeyByValue(Map<K , V> map, K value){
        for (Map.Entry<K, V> m : map.entrySet()){
            if (m.getValue().equals(value)){
                return m.getKey();
            }
        }
        return null;
    }
    public <K, V> V getValueByKeyFromSecondMap(Map<K , Map<K, V>> map, K key){
        for (Map.Entry<K, Map<K, V>> m : map.entrySet()){
            if (m.getKey().equals(key)){
                return getValueFromMap(m.getValue());
            }
        }
        return null;
    }
    public <K, V> K getKeyByValueFromSecondMap(Map<K , Map<K, V>> map, K key){
        for (Map.Entry<K, Map<K, V>> m : map.entrySet()){
            if (m.getKey().equals(key)){
                return getKeyFromMap(m.getValue());
            }
        }
        return null;
    }

}