package PrepareTest.TestCaseParallel;

import ASOURCE.BaseClasses.PreAfterTest;
import ASOURCE.BaseClasses.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class buttonForOpenModalFormUniversal extends PreAfterTest {

    @Test
    public void simpleTest() throws InterruptedException {
        getDriver().get("http://amw.de/auto/audi/a1/gebrauchtwagen-UbCvmh.html");
        getDriver().manage().window().maximize();

        if (isElementPresent2(By.xpath(".//ul[contains(@class,'actions-bookmarks')]"))){
            List<WebElement> listButton = getDriver().findElements(By.xpath(".//ul[contains(@class,'actions-bookmarks')]//*/a"));
            for (WebElement x : listButton){
                String first =  x.getText();
                x.click();
                try {
                    getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//div[contains(@class,'modal') and contains(@class,'in')]//*/ul//*/a[contains(@class,'on')]")));
                    int countElement = countElement(By.xpath(".//div[contains(@class,'modal') and contains(@class,'in')]//*/ul//*/a[contains(@class,'on')]"));
                    Assert.assertEquals(1, countElement);
                }catch (Exception | Error e){
                    verificationErrors.append("Opened more Then one Window : " + e.getMessage() + "\n\n");
                }
                String second = "";
                try {
                    second = getDriver().findElement(By.xpath(".//div[contains(@class,'modal-body')]//*/ul[contains(@class,'form-togglers')]//*/a[contains(@class,'on')]")).getText();
                    Assert.assertEquals(first, second);
                }catch (Exception e){
                    verificationErrors.append("Open wrong window! " + "Expected window: " + first + " Actual window: " + second + "\n\n");
                }catch (Error error){
                    verificationErrors.append(error.getMessage() + "\n\n");
                }
                //Close current modal window
                try {
                    getDriver().findElement(By.xpath(".//div[contains(@class,'modal') and contains(@class,'in')]//*[contains(@class,'close')]")).click();
                    Thread.sleep(2500);
                }catch (Exception e){
                    verificationErrors.append("Cant close the window: " + second + e.getMessage() + "\n\n");
                }
            }
        }
    }
}
