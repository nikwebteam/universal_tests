package PrepareTest.TestCaseParallel;

import ASOURCE.BaseClasses.PreAfterTest;
import ASOURCE.BaseClasses.TestBase;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

public class blockContactDealerOnPageDetailsOfCars extends PreAfterTest {

    @Test
    public void testLink()throws Exception{
        getDriver().get("http://bavarianmotorcars.com/auto/acura/rdx/used-HMCBQg.html");
        getDriver().manage().window().maximize();

        //================================Check that block and title "Contact dealer" is Present===================================//
        try {
            Assert.assertTrue(isElementPresent(By.xpath(".//div[@class='widget-header' and contains(text(),'Contact Dealer')]")));
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }

        //================================Check photo in block "Contact dealer"=====================================================//
        try {
            Assert.assertTrue(isElementPresent(By.xpath(".//div[@class='person-data']/div[1]/div[1]/span/img")));
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }
        try {
            getDriver().findElement(By.xpath(".//div[@class='person-data']/div[1]/div[1]/span/img")).isDisplayed();
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }

        //============================Check personal info by photo in block "Contact dealer"==========================================//
        try {
            Assert.assertTrue(isElementPresent(By.xpath(".//div[@class='person-data']/div[2]/div[1]")));  //First and Last name
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }
        try {
            Assert.assertTrue(isElementPresent(By.xpath(".//div[@class='person-data']/div[2]/div[2]/span[2]/span"))); //phone number
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }
        try {
            Assert.assertTrue(isElementPresent(By.xpath(".//div[@class='person-data']/div[2]/div[3]/span[2]/span/a"))); //Email address
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }
        try {
            Assert.assertTrue(isElementPresent(By.xpath(".//div[@class='person-data']/div[1]/div[2]/div/a")));   //Button scan Contacts
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }
        try{
            getDriver().findElement(By.xpath(".//div[@class='person-data']/div[1]/div[2]/div/a")).click();  // click on button scan contacts
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }
        try {
            getDriver().findElement(By.xpath(".//div[@class='scancode-wrapper']/img")).isDisplayed();   //window for scan contacts info is displayed
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }

        //===============Check Block right with info locale contacts=================================================================//
        try {
            Assert.assertTrue(isElementPresent(By.xpath(".//*[@class='row-siteinfo type-name']")));     // name of City
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }
        try{
            Assert.assertTrue(isElementPresent(By.xpath(".//*[@class='row-siteinfo type-phone']")));    // phone number
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }
        try{
            Assert.assertTrue(isElementPresent(By.xpath(".//*[@class='row-siteinfo type-email']")));    //Email address
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }
        try{
            Assert.assertTrue(isElementPresent(By.xpath(".//span[@class='label' and contains(text(),'Address')]")));   //Post Address
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }
        try{
            Assert.assertTrue(isElementPresent(By.xpath(".//*[@class='row-siteinfo type-button']")));       //Button "Contact dealer"
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }
        try{
            getDriver().findElement(By.xpath(".//*[@class='row-siteinfo type-button']/a")).click();       //Button "Contact dealer"
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }
    }
}