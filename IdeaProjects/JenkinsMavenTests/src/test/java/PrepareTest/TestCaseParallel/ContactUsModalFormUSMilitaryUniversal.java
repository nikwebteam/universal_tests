package PrepareTest.TestCaseParallel;

import ASOURCE.BaseClasses.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ContactUsModalFormUSMilitaryUniversal extends TestBase {

    @Test
    public void firstTest() throws InterruptedException {
        //getDriver().get("http://amw.demo.symfio.de/auto/audi/a1/jahreswagen-tG7zph.html");
       // getDriver().get("http://bavarian.demo.symfio.de/auto/acura/rdx/used-EIX4Vj.html");
        getDriver().get("http://autoseredin.de/auto/audi/q7/neuwagen-TjeBxf.html");
        getDriver().manage().window().maximize();
        getDriver().findElement(By.xpath(".//a[@href='#modal_actions_bookmarks']")).click();


        String id  =  getDriver().findElement(By.xpath(".//form[contains(@action,'modal')]")).getAttribute("id");

        List<WebElement> SetButton = getDriver().findElements(By.xpath("//*[@id=\"modal_actions_bookmarks\"]//*/ul//*/a"));
        for (int i = 1; i < SetButton.size(); i++) {
            SetButton.get(i).click();
        }
        // Create Set with name of button for opened form fields
        List<String> SetFormsButton;
        String str = getDriver().findElement(By.xpath(".//div[contains(@id,'modal') and contains(@aria-hidden,'false')]")).getAttribute("class");

        SetFormsButton = cutStringByInfo(str,'-',SetButton.size());
        //System.out.println(SetFormsButton);

        // Store all form fields for checking
        Set<WebElement> SetText = new HashSet<>();
        Set<WebElement> SetNumeric = new HashSet<>();
        Set<WebElement> SetSelect = new HashSet<>();
        Set<WebElement> SetTextarea = new HashSet<>();
        Set<WebElement> SetCheckbox = new HashSet<>();
        WebElement email = getDriver().findElement(By.xpath(".//input[contains(@id,'" + id + "') and contains(@type,'email')]"));
        WebElement TestDrive = getDriver().findElement(By.xpath(".//input[contains(@id,'" + id + "') and contains(@id,'lbl')" +
                "and contains(@data-date-format,'dd/mm/yyyy hh:ii')]"));
        for (String name : SetFormsButton) {
            SetText.addAll( new HashSet<WebElement>(getDriver().findElements(By.xpath(".//input[contains(@id,'" + id + "')" +
                            "and contains(@id,'" + name + "') " +
                            "and not(contains(@data-plugin,'numeric'))" +
                            "and not(contains(@type,'email'))" +
                            "and not(contains(@type,'checkbox'))" +
                            "and not(contains(@data-date-format,'dd/mm/yyyy hh:ii')) " +
                            "and not(contains(@type,'hidden'))]"))));
            SetNumeric.addAll(new HashSet<WebElement>(getDriver().findElements(By.xpath(".//input[contains(@id,'" + id + "')" +
                    "and contains(@id,'" + name + "')" +
                    "and contains(@data-plugin,'numeric')]"))));
            SetSelect.addAll(new HashSet<WebElement>(getDriver().findElements(By.xpath(".//select[contains(@id,'" + id + "')" +
                    "and contains(@id," + name + ")]"))));
            SetTextarea.addAll(new HashSet<WebElement>(getDriver().findElements(By.xpath(".//textarea[contains(@id,'" + id + "')]"))));
            SetCheckbox.addAll(new HashSet<WebElement>(getDriver().findElements(By.xpath(".//input[contains(@id,'" + id + "')" +
                    "and contains(@type,'checkbox')" +
                    "and contains(@id,'" + id + "')]"))));
            //System.out.println(SetText.size()+" "+SetNumeric.size()+" "+SetSelect.size()+" "+SetTextarea.size()+" "+SetCheckbox.size()+" "+name);
        }

        // Check all fields only for text
        for (WebElement x : SetText){
            try {
                x.sendKeys("Test");
            }catch (Exception e){
                verificationErrors.append(" Field with simple text is filed! " + x.getAttribute("id") + " \n" + e.getMessage() + "\n\n");
            }
        }
        // Check all fields only for numeric
        for (WebElement x : SetNumeric){
            try {
                x.sendKeys("123");
            }catch (Exception e){
                verificationErrors.append(" Field with simple numeric is filed! " + x.getAttribute("id") + " \n\n");
            }
        }
        // Check all checkbox
        for (WebElement x : SetCheckbox){
            try {
                x.click();
            }catch (Exception e){
                verificationErrors.append(" Field with Checkbox is filed! " + x.getAttribute("id") + " \n\n");
            }
        }
        // Check all select
        for (WebElement x : SetSelect){
            try {
                new Select(x).selectByIndex(1);
                Thread.sleep(1000);
            }catch (Exception e){
                verificationErrors.append(" Field with Select is filed! " + x.getAttribute("id") + " \n\n");
            }
        }
        // Check field "email"
        try {
            email.sendKeys("test@test.com");
        }catch (Exception e){
            verificationErrors.append(" Field with Email test is filed! " + email.getAttribute("id") + " \n\n");
        }
        // Check field "Test Drive"
        try {
            TestDrive.sendKeys("15/12/2015 09:50");
        }catch (Exception e){
            verificationErrors.append(" Field with TestDrive test is filed! " + TestDrive.getAttribute("id") + " \n\n");
        }
        // Check all field "Textarea"
        for (WebElement x : SetTextarea) {
            try {
                x.sendKeys("some text for test field \"Comment\"");
            } catch (Exception e) {
                verificationErrors.append(" Field with Comment test is filed! " + x.getAttribute("id") + " \n\n");
            }
        }
        Thread.sleep(20000);
    }

    public String charInStringToUpperCaseByIndex(String str, Integer index){
        String result = "";
        for (int i = 0; i < str.length(); i++) {
            if (index == i){
                result += Character.toUpperCase(str.charAt(i));
            }else {
                result += str.charAt(i);
            }
        }
        return result;
    }

    public List<String> cutStringByInfo(String str, Character info, Integer index){

        List<String> list = new ArrayList<>();
        String t = str;
        for (int i = 0; i < index; i++) {
            t = t.substring(t.indexOf('-') + 1);
            if (i <= index - 2) {
                list.add(t.substring(0, t.indexOf(' ')));
            }else{
                list.add(t);
            }
        }
        return list;
    }
}
