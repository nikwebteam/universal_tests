package PrepareTest.TestCaseParallel;

import ASOURCE.BaseClasses.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import java.util.List;

public class ContactUsFullFormUSMilitaryUniversal extends TestBase {

    @Test
    public void firstTest() throws InterruptedException {
        getDriver().get(baseUrl + "/tradein");
        getDriver().manage().window().maximize();

        // Get id Form
        String id = getDriver().findElement(By.xpath(".//form[not(contains(@action,'modal'))]")).getAttribute("id");

        if (isElementPresent2(By.xpath(".//form[@id='" + id + "']//*/div[contains(@class,'section-tradein')]/div"))) {
            getDriver().findElement(By.xpath(".//form[@id='" + id + "']//*/div[contains(@class,'section-tradein')]/div")).click();
        }

        List<WebElement> listText = getDriver().findElements(By.xpath(".//form[@id='" + id + "']//*/input[contains(@id,'" + id + "')" +
                "and not(contains(@data-plugin,'numeric'))" +
                "and not(contains(@type,'email'))" +
                "and not(contains(@type,'checkbox'))" +
                "and not(contains(@placeholder,'Enter date'))" +
                "and not(contains(@type,'hidden'))]"));
        List<WebElement> listNumeric = getDriver().findElements(By.xpath(".//form[@id='" + id + "']//*/input[contains(@id,'" + id + "')" +
                "and contains(@data-plugin,'numeric')]"));
        List<WebElement> listSelect = getDriver().findElements(By.xpath(".//form[@id='" + id + "']//*/select[contains(@id,'" + id + "')]"));
        List<WebElement> listTextarea = getDriver().findElements(By.xpath(".//form[@id='" + id + "']//*/textarea"));
        List<WebElement> listCheckbox = getDriver().findElements(By.xpath(".//form[@id='" + id + "']//*/input[contains(@id,'" + id + "')" +
                "and contains(@type,'checkbox')]"));
        WebElement email = getDriver().findElement(By.xpath(".//form[@id='" + id + "']//*/input[contains(@id,'" + id + "')" +
                "and contains(@type,'email')]"));

        if (isElementPresent2(By.xpath(".//form[@id='" + id + "']//*/input[contains(@id,'" + id + "') and contains(@id,'lbl') and contains(@placeholder,'Enter date')]"))){

            WebElement TestDrive = getDriver().findElement(By.xpath(".//form[@id='" + id + "']//*/input[contains(@id,'" + id + "')" +
                    "and contains(@id,'lbl')" +
                    "and contains(@placeholder,'Enter date')]"));

            // Check field "Test Drive"
            try {
                TestDrive.sendKeys("15/12/2015 09:50");
            }catch (Exception e){
                verificationErrors.append(e.getMessage());
            }
        }

        // Check all fields only for text
        for (WebElement x : listText){
            try {
                x.sendKeys("Test");
            }catch (Exception e){
                verificationErrors.append(e.getMessage());
            }
        }
        // Check all fields only for numeric
        for (WebElement x : listNumeric){
            try {
                x.sendKeys("123");
            }catch (Exception e){
                verificationErrors.append(e.getMessage());
            }
        }
        // Check all checkbox
        for (WebElement x : listCheckbox){
            try {
                x.click();
            }catch (Exception e){
                verificationErrors.append(e.getMessage());
            }
        }
        // Check all select
        for (WebElement x : listSelect){
            try {
                new Select(x).selectByIndex(1);
                Thread.sleep(1000);
            }catch (Exception e){
                verificationErrors.append(e.getMessage() + "\n" + x.getAttribute("id"));
            }
        }
        // Check field "email"
        try {
            email.sendKeys("test@test.com");
        }catch (Exception e){
            verificationErrors.append(e.getMessage());
        }

        // Check all field "Textarea"
        for (WebElement x : listTextarea) {
            try {
                x.sendKeys("some text for test field \"Comment\"");
            } catch (Exception e) {
                verificationErrors.append(e.getMessage());
            }
        }
    }
}
