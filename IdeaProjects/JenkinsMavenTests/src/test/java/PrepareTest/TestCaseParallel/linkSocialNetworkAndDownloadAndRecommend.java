package PrepareTest.TestCaseParallel;

import ASOURCE.BaseClasses.PreAfterTest;
import ASOURCE.BaseClasses.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class linkSocialNetworkAndDownloadAndRecommend extends PreAfterTest {

    @Test
    public void testLink()throws Exception{
        getDriver().get("http://bavarian.demo.symfio.de/auto/acura/rdx/used-EIX4Vj.html");
        getDriver().manage().window().maximize();
        String winHandleBefore = getDriver().getWindowHandle();

//================================================= Check link Facebook ==================================================================================//

        try{
            Assert.assertTrue(isElementPresent(By.xpath(".//a[@class='bookmark-button button-facebook']")));  // Check link Facebook is present
        }catch (Exception e){
            verificationErrors.append("Element \"Link Facebook\" not present\n" + e.getMessage());
        }
        try{
            getDriver().findElement(By.xpath(".//a[@class='bookmark-button button-facebook']")).click();      // Click on link Facebook
        }catch (Exception e){
            verificationErrors.append("Can't click on element \"Facebook\"\n" + e.getMessage());
        }
        for (String winHandle:getDriver().getWindowHandles()) {                                               // Select opened window "Facebook"
            getDriver().switchTo().window(winHandle);
        }
        String facebookWindowTitle = getDriver().getTitle();                                                  // Store title page "Facebook"
        try{
            Assert.assertEquals(facebookWindowTitle, "Facebook");                                             // Check that title is right
            getDriver().close();                                                                              // Close window "Facebook"
        }catch (Error e){
            verificationErrors.append("Title for page \"Facebook\" is incorrect\n" + e.getMessage());
        }
        getDriver().switchTo().window(winHandleBefore);                                                       // Back to window Before

//============================== Check link Twitter =========================================================================================================//

        try{
            Assert.assertTrue(isElementPresent(By.xpath(".//a[@class='bookmark-button button-twitter']")));  //Check link Twitter is present
        }catch (Exception e){
            verificationErrors.append("Element \"Link Twitter\" not present\n" + e.getMessage());
        }
        try {
            getDriver().findElement(By.xpath(".//a[@class='bookmark-button button-twitter']")).click();      // Click on link Twitter
        }catch (Exception e){
            verificationErrors.append("Can't click on element \"Twitter\"\n" + e.getMessage());
        }
        for (String winHandle:getDriver().getWindowHandles()) {                                              // Select opened window "Twitter"
            getDriver().switchTo().window(winHandle);
        }
        String twitterWindowTitle = getDriver().getTitle();                                                  // Store title page "Twitter"
        try{
            Assert.assertEquals(twitterWindowTitle, "Поделиться ссылкой в Твиттере");                        // Check that title is right
            getDriver().close();                                                                             // Close window "Twitter"
        }catch (Error e){
            verificationErrors.append("Title for page \"Twitter\" is incorrect\n" + e.getMessage());
        }
        getDriver().switchTo().window(winHandleBefore);                                                      // Back to window Before

//================================================= Check link Google ======================================================================================//

        try{
            Assert.assertTrue(isElementPresent(By.xpath(".//a[@class='bookmark-button button-googleplus']")));  //Check link Google is present
        }catch (Exception e){
            verificationErrors.append("Element \"Link Google\" not present\n" + e.getMessage());
        }
        try {
            getDriver().findElement(By.xpath(".//a[@class='bookmark-button button-googleplus']")).click();      // Click on link Google
        }catch (Exception e){
            verificationErrors.append("Can't click on element \"Google\"\n" + e.getMessage());
        }
        for (String winHandle:getDriver().getWindowHandles()) {                                                 // Select opened window "Google"
            getDriver().switchTo().window(winHandle);
        }
        String googleWindowTitle = getDriver().getTitle();                                                      // Store title page "Google"
        try{
            Assert.assertEquals(googleWindowTitle, "Google+");                                                  // Check that title is right
            getDriver().close();                                                                                // Close window "Google"
        }catch (Error e){
            verificationErrors.append("Title for page \"Google\" is incorrect\n" + e.getMessage());
        }
        getDriver().switchTo().window(winHandleBefore);                                                         // Back to window Before

//=============================================== Check link Recommend =====================================================================================//

        try {
            Assert.assertTrue(isElementPresent(By.xpath(".//a[@href='#mailshare-modal']")));                    // Check that button "Recommend" is present
        }catch (Exception e){
            verificationErrors.append("Element \"Button Recommend\" not present\n" + e.getMessage());
        }
        try {
            getDriver().findElement(By.xpath(".//a[@href='#mailshare-modal']")).click();                        // Click on link "Recommend"
        }catch (Exception e){
            verificationErrors.append("Can't click on element \"Button Recommend\"\n" + e.getMessage());
        }
        Thread.sleep(1000);
        try {
            getDriver().findElement(By.xpath(".//div[@id='mailshare-modal']")).isDisplayed();                   // Check that window for recommend is present
        }catch (Exception e){
            verificationErrors.append("Window for Recommend is not present\n" + e.getMessage());
        }
        try {
            getDriver().findElement(By.xpath(".//div[contains(@class,'modal') and contains(@class,'in')]//*/button")).click();                      // Close window "Recommend"
        }catch (Exception e){
            verificationErrors.append("Can't close window \"Recommend\"\n" + e.getMessage());
        }
        Thread.sleep(1000);

//================================================ Check link download ===================================================================================//

        try {
            Assert.assertTrue(isElementPresent(By.xpath(".//a[@class='bookmark-button button-pdf']")));         // Check that button "Download" is present
        }catch (Exception e){
            verificationErrors.append("Element \"Button Download\" not present\n" + e.getMessage());
        }
        try {
            getDriver().findElement(By.xpath(".//a[@class='bookmark-button button-pdf']")).click();             // Click on button "Download"
        }catch (Exception e){
            verificationErrors.append("Can't click on element \"Button Download\"\n" + e.getMessage());
        }catch (Error error){
            verificationErrors.append("Can't click on element \"Button Download\"\n" + error.getMessage());
        }
        Thread.sleep(3000);                                                                                     // Wait when download window is present
        Actions action = new Actions(getDriver());
        action.sendKeys(Keys.CANCEL);                                                                           // Press Escape button for close the window for download file
    }
}