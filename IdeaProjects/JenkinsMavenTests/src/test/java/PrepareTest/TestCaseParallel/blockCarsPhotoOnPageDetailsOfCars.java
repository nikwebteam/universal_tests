package PrepareTest.TestCaseParallel;

import ASOURCE.BaseClasses.PreAfterTest;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

public class blockCarsPhotoOnPageDetailsOfCars extends PreAfterTest {

    @Test
    public void testLink()throws Exception{
        getDriver().get("http://bavarianmotorcars.com/auto/acura/rdx/used-HMCBQg.html");
        getDriver().manage().window().maximize();

        try{
            Assert.assertTrue(isElementPresent(By.xpath(".//div[@class='image-main big-image-1']/a")));  //check big photo for car
        }catch (Exception e){
            verificationErrors.append(e.getClass() +
                    " : " + "Element: .//div[@class='image-main big-image-1']/a\n");
        }
        try{
            getDriver().findElement(By.xpath(".//div[@class='image-main big-image-1']/a")).click();     //Click on the Big photo
            Thread.sleep(3000);
        }catch (Exception e){
            verificationErrors.append(e.getClass() +
            " : Element: .//div[@class='image-main big-image-1']/a\n");
        }
        try{
            getDriver().findElement(By.xpath(".//div[@class='modalbox-holder fade columns-both in']")).isDisplayed();
        }catch (Exception e){
            verificationErrors.append(e.getClass() +
                    " : Element: .//div[@class='modalbox-holder fade columns-both in']\n");
        }
        try{
            getDriver().findElement(By.xpath(".//a[@class='mb-outer-close']")).click();
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }
        try{
            Assert.assertTrue(isElementPresent(By.xpath(".//span[@class='image-count']")));               //check count photo for car
        }catch (Exception e){
            verificationErrors.append(e.getClass() +
                    " : " + "Element: .//span[@class='image-count']\n");
        }
        String countPhoto = " ";
        try {
            countPhoto = getDriver().findElement(By.xpath(".//span[@class='image-count']/span")).getText();   //get count photo of car
        }catch (Exception e){
            verificationErrors.append(e.getClass() +
            " : Element: .//span[@class='image-count']/span\n");
        }
        try {
            Assert.assertTrue(isElementPresent(By.xpath(".//span[@class='\"price-value\"']")));        // check price is present
        }catch (Exception e){
            verificationErrors.append(e.getClass() +
                    " : " + "Element: .//span[@class='\"price-value\"']\n");
        }
        try{
            Assert.assertTrue(isElementPresent(By.xpath(".//i[@class='icon icon-info-sign ']")));     // Check sign old price on the Big photo
        }catch (Exception e){
            verificationErrors.append(e.getClass() +
            " : " + "Element: .//i[@class='icon icon-info-sign \n");
        }
        int count = Integer.parseInt(countPhoto);
        for (int i = 1; i < count; i++) {
            try {
                getDriver().findElement(By.xpath(".//ul[@class='images-secondary']/li[" + Integer.toString(i) + "]/a/span/img"));  //check that all photo of car is present
            }catch (Exception e){
                verificationErrors.append(e.toString());
            }
        }
    }
}