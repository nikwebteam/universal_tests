package PrepareTest.TestCaseParallel;

import ASOURCE.BaseClasses.TestBase;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class ParserAllLinksFromSite extends TestBase {

    @Test
    public void checkSmartFilter() throws InterruptedException, IOException {

        HashSet<String> AllSiteLinks = new HashSet<>();

        List<String> urls = new ArrayList<>();

        // parse all links from base page
        try {
            Document document = Jsoup.connect(baseUrl).timeout(0).get();
            Element mBody = document.body();
            Elements NewLink = mBody.getElementsByTag("a");
            for (Element link : NewLink) {
                urls.add(link.attr("href").trim());
            }
        }catch (Exception e){
            verificationErrors.append(e.getMessage() + "\n" + baseUrl);
        }

        List<String> finalList = new ArrayList<>();

        for (String s : urls){
            if( (s.contains("xml")) || (s.contains("#0")) || (s.equals("/")) || (s.contains("pdf")) ) {

            }else {
                if (s.contains("http")) {
                    finalList.add(s);
                }else{
                    finalList.add(baseUrl + s);
                }
            }
        }

        // parse links from all pages site
        for (int i = 0; i < finalList.size(); i++) {
            AllSiteLinks.addAll(getLinks(finalList.get(i)));
        }

        // check request for all links from all pages site

        Iterator<String> iterator = AllSiteLinks.iterator();
        for (int i = 0; i < AllSiteLinks.size(); i++){
            int statusCode = getResponseCode(iterator.next());
            if (statusCode == 200 || statusCode >= 300 && statusCode < 400) {

            }else {
                try {
                    throw new Exception("Request code is: ");
                } catch (Exception e) {
                    verificationErrors.append(e.getMessage() + " " + statusCode + " " + iterator.next() + "\n");
                }
            }
        }
    }

    // Method who parse all links by URl page
    public List<String> getLinks(String url) throws IOException, InterruptedException {
        List<String> urls = new ArrayList<>();
        try {
            Document document = Jsoup.connect(url).timeout(0).get();
            Element Body = document.body();
            Elements AllLink = Body.getElementsByTag("a");
            for (Element link : AllLink)
            {
                urls.add(link.attr("href").trim());
            }
        } catch (Exception e){
            verificationErrors.append(e.getMessage() + "\n" + url + "\n");
        }

        List<String> finalList = new ArrayList<>();

        for (String s : urls){
            if( (s.contains("xml")) || (s.contains("#0")) || (s.equals("/")) || (s.contains("pdf"))) {

            }else {
                if (s.contains("http")) {
                    finalList.add(s);
                }else{
                    finalList.add(baseUrl + s);
                }
            }
        }
        return finalList;
    }
}
