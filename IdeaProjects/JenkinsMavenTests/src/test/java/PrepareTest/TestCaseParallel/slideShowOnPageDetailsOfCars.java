package PrepareTest.TestCaseParallel;

import ASOURCE.BaseClasses.PreAfterTest;
import ASOURCE.BaseClasses.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.util.List;

public class slideShowOnPageDetailsOfCars extends PreAfterTest {
    @Test
    public void first() throws InterruptedException {
        getDriver().get("http://bavarian.demo.symfio.de/auto/acura/rdx/used-EIX4Vj.html");
        getDriver().manage().window().maximize();
//================================== Start Tests =================================================================//

        if ( !(isElementPresent2(By.xpath(".//div[@class='image-main big-image-1']/*/img[@alt='No photos']"))) ){

            try {
                getDriver().findElement(By.xpath(".//div[@class='image-main big-image-1']/a")).click();         // Open window for watch photo of car
            }catch (Exception e){
                verificationErrors.append(e.getMessage());
            }
            getWait().until(ExpectedConditions.visibilityOf(getDriver().findElement(By.xpath(".//div[contains(@class,'modal')" +
                    "and contains(@class,'in')]//*/div[@class='mb-image-container']/span/img"))));
            
            if (isElementPresent(By.xpath(".//div[@class='mb-box']"))){
                List<WebElement> webElement = getDriver().findElements(By.xpath(".//img[@class='thumb-img']"));
                for (WebElement x : webElement) {
                    try {
                        x.click();
                        getWait().until(ExpectedConditions.visibilityOf(getDriver().findElement(By.xpath(".//div[contains(@class,'modal')" +
                                "and contains(@class,'in')]//*/div[@class='mb-image-container']/span/img"))));
                        Thread.sleep(500);
                        getDriver().findElement(By.xpath(".//div[@class='mb-image-container']/span/img")).isDisplayed();
                    }catch (Exception e){
                        verificationErrors.append(e.getMessage());
                    }
                }
                try {
                    getDriver().findElement(By.xpath(".//a[@class='mb-outer-close']")).click();
                }catch (Exception e){
                    verificationErrors.append(e.getMessage());
                }
            }
        }else{
            try {
                throw new NoSuchElementException("Test \"first\" is not executed! \nCause: Element big photo is not present");
            }catch (NoSuchElementException e){
                verificationErrors.append(e.getMessage());
            }
        }
//================================================ END Test ==========================================================================//
    }
}
