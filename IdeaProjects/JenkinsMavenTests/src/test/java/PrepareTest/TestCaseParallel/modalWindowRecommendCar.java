package PrepareTest.TestCaseParallel;

import ASOURCE.BaseClasses.PreAfterTest;
import ASOURCE.BaseClasses.TestBase;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

public class modalWindowRecommendCar extends PreAfterTest {

    @Test
    public void testLink()throws Exception{
        getDriver().get("http://bavarian.demo.symfio.de/auto/acura/rdx/used-EIX4Vj.html");
        getDriver().manage().window().maximize();

        try {
            getDriver().findElement(By.xpath(".//a[@href='#mailshare-modal']")).click();                        // Click on link "Recommend"
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }
        try {
            Assert.assertTrue(isElementPresent(By.xpath(".//div[@class='vphoto']/img")));                      // Block with photo is present
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }
        try {
            getDriver().findElement(By.xpath(".//div[@class='vphoto']/img")).isDisplayed();                    // Check that Image is displayed
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }
        try {
            Assert.assertTrue(isElementPresent(By.xpath(".//div[@class='summary-info']/p[1]")));               // Check that title of car is present
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }
        try {
            Assert.assertTrue(isElementPresent(By.xpath(".//div[@class='summary-info']/p[2]/a")));            // Check that link to the page with this car is present
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }
        try{
            Assert.assertTrue(isElementPresent(By.xpath(".//div[@class='block-form']/div[1]/div[2]/input")));  //Check that field first and last name is present
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }
        try {
            getDriver().findElement(By.xpath(".//div[@class='block-form']/div[1]/div[2]/input")).sendKeys("FirstTest and LastName");  // Check field first name and last name
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }
        try {
            getDriver().findElement(By.xpath(".//input[@id='modal_emails']")).sendKeys("test@test.com");         // Check field email
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }
        try {
            getDriver().findElement(By.xpath(".//textarea[@id='modal_comments']")).sendKeys("Just a Test and nothing more"); // Check field comments
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }
        try {
            Assert.assertTrue(isElementPresent(By.xpath(".//a[@class='button button-send']")));                 // Check that button "Send" is present
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }
        try{
            Assert.assertTrue(isElementPresent(By.xpath(".//div[@class='modal-header']/button")));             // Check that button close window is present
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }
        try {
            getDriver().findElement(By.xpath(".//div[contains(@class,'modal') and contains(@class,'in')]//*/button")).click();                 // Click on button close window
        }catch (Exception e){
            verificationErrors.append(e.toString());
        }
//===================================================== END ==================================================================================================//
    }
}