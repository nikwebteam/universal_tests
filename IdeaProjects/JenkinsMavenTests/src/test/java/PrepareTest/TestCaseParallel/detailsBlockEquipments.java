package PrepareTest.TestCaseParallel;

import ASOURCE.BaseClasses.PreAfterTest;
import ASOURCE.BaseClasses.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class detailsBlockEquipments extends PreAfterTest{

    @Test
    public void testLink()throws Exception {
        getDriver().get("http://bavarian.demo.symfio.de/auto/acura/rdx/used-EIX4Vj.html");
        getDriver().manage().window().maximize();

        if (isElementPresent2(By.xpath(".//span[contains(@class,'hint')]"))) {
            try {
                List<WebElement> listButton = getDriver().findElements(By.xpath(".//ul[contains(@class,'specs-tabs')]//*/a"));
                List<WebElement> listText = getDriver().findElements(By.xpath(".//ul[contains(@class,'specs-tabs')]//*/a/h6"));

                for (int i = 1; i < listButton.size(); i++) {
                    String first = "about " + listText.get(i).getText();
                    listButton.get(i).click();
                    String second = getDriver().findElement(By.xpath(".//div[contains(@class,'tab-pane')" +
                            "and contains(@class,'active')]/div[1]/span[2]")).getText();
                    Assert.assertEquals(first, second);
                }
            }catch (Error | Exception e){
                verificationErrors.append(e.getMessage());
            }
        }
    }

}
