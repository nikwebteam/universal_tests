package PrepareTest.TestCaseParallel;

import ASOURCE.BaseClasses.PreAfterTest;
import ASOURCE.BaseClasses.TestBase;
import org.json.JSONException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class checkDynamicSort extends PreAfterTest{
    @Test
    public void firstTest() throws InterruptedException, IOException, ParseException, JSONException {

        getDriver().get(baseUrl + "/fahrzeugsuche.html");
        getDriver().manage().window().maximize();
        WebDriverWait wait = new WebDriverWait(getDriver(), 15, 200);

        // Store name for method sorting
        List<WebElement> first = new ArrayList<>();
        try {
            first = getDriver().findElements(By.xpath(".//ul[@data-context='sort-list']/*/a"));
            first.add(0, getDriver().findElement(By.xpath(".//a[@data-toggle='dropdown']")));
        }catch (Error |Exception e){
            verificationErrors.append(e.getMessage());
        }
        // write name of method sorting in massive
        String[] listAttribute = new String[first.size()];
        for (int i = 0; i < first.size(); i++) {
            listAttribute[i] = first.get(i).getAttribute("data-name");
        }

        // start checking sorting by each method
            for (int i = 0; i < first.size(); i++) {
                String attribute = listAttribute[i];

                // First check by default sorted page and than we will teke each other method sorting
                if (i > 0) {
                    getDriver().findElement(By.xpath(".//a[@data-toggle='dropdown']")).click();
                    getDriver().findElement(By.xpath(".//ul[contains(@data-context,'sort-list')]//*/a[@data-name='" + listAttribute[i] + "']")).click();
                    wait.until(ExpectedConditions.stalenessOf(getDriver().findElement(By.xpath(".//div[contains(@class,'vehicle-img')]/a"))));
                    wait.until(ExpectedConditions.visibilityOf(getDriver().findElement(By.xpath(".//div[contains(@class,'vehicle-img')]/a"))));
                }

                // Count how much page by current sorting
                int countPage = Integer.parseInt(getDriver().findElement(By.xpath(".//span[@class='pages-number']")).getText().trim());

                // Check not more than four page for each method sorting
                if (countPage < 4) {
                    for (int j = 0; j < countPage; j++) {
                        List<String> list = parseCarId(By.xpath(".//div[contains(@class,'vehicle-img')]/a"));
                        checkResult(list, attribute);
                        getDriver().findElement(By.xpath(".//span[@class='pagination-buttons']/span[contains(@class,'next')]")).click();
                        wait.until(ExpectedConditions.stalenessOf(getDriver().findElement(By.xpath(".//div[contains(@class,'vehicle-img')]/a"))));
                        wait.until(ExpectedConditions.visibilityOf(getDriver().findElement(By.xpath(".//div[contains(@class,'vehicle-img')]/a"))));
                    }
                } else {
                    for (int j = 0; j < 4; j++) {
                        List<String> list = parseCarId(By.xpath(".//div[contains(@class,'vehicle-img')]/a"));
                        checkResult(list, attribute);
                        getDriver().findElement(By.xpath(".//a[contains(@class,'button button-next')]")).click();
                        wait.until(ExpectedConditions.stalenessOf(getDriver().findElement(By.xpath(".//div[contains(@class,'vehicle-img')]/a"))));
                        wait.until(ExpectedConditions.visibilityOf(getDriver().findElement(By.xpath(".//div[contains(@class,'vehicle-img')]/a"))));
                    }
                }
                // refresh window for new checking
                getDriver().navigate().refresh();
            }
    }

    public void checkResult(List<String> list,String attribute) throws IOException, ParseException, JSONException {
        switch (attribute){
            case "price_cheap":
                String result = getDataFromBase(list,"price");
                Map<String, Integer> mapLow = parseDataByInfo(list ,result, "price");
                for (int i = 0; i < list.size() - 1; i++) {
                    try {
                        Assert.assertTrue(getValueByKey(mapLow, list.get(i)) <= getValueByKey(mapLow,list.get(i + 1)));
                    }catch (Error e){
                        verificationErrors.append("Test by dynamic search BY price from Low to High is Filed!!!\n" + e.getMessage() + "\n");
                    }
                }
                break;
            case "price_high":
                String resultHigh = getDataFromBase(list,"price");
                Map<String, Integer> mapHigh = parseDataByInfo(list,resultHigh,"price");
                for (int i = 0; i < list.size() - 1; i++) {
                    try {
                        Assert.assertTrue(getValueByKey(mapHigh, list.get(i)) >= getValueByKey(mapHigh,list.get(i + 1)));
                    }catch (Error e){
                        verificationErrors.append("Test by dynamic search BY price from Low to High is Filed!!!\n" + e.getMessage() + "\n");
                    }
                }
                break;
            case "odometer_desc":
                String resultOdometerLow = getDataFromBase(list,"odometer");
                Map<String, Integer> mapOdometerLow = parseDataByInfo(list,resultOdometerLow,"odometer");
                for (int i = 0; i < list.size() - 1; i++) {
                    try {
                        Assert.assertTrue(getValueByKey(mapOdometerLow, list.get(i)) >= getValueByKey(mapOdometerLow,list.get(i + 1)));
                    }catch (Error e){
                        verificationErrors.append("Test by dynamic search BY odometer from Low to High is Filed!!!\n" + e.getMessage() + "\n");
                    }
                }
                break;
            case "odometer_asc":
                String resultOdometerHigh = getDataFromBase(list,"odometer");
                Map<String, Integer> mapOdometerHigh = parseDataByInfo(list,resultOdometerHigh,"odometer");
                for (int i = 0; i < list.size() - 1; i++) {
                    try {
                        Assert.assertTrue(getValueByKey(mapOdometerHigh, list.get(i)) <= getValueByKey(mapOdometerHigh,list.get(i + 1)));
                    }catch (Error e){
                        verificationErrors.append("Test by dynamic search BY odometer from Low to High is Filed!!!\n" + e.getMessage() + "\n");
                    }
                }
                break;
            case "last_added_inventory":
                String resultLastAdded = getDataFromBase(list, "last_added_inventory");
                Map<String, Date> mapDate = parseDataByInfo(list, resultLastAdded, "last_added_inventory");
                for (int i = 0; i < list.size() - 1; i++) {
                    try {
                        if (getValueByKey(mapDate, list.get(i)).equals(getValueByKey(mapDate, list.get(i + 1)))){
                            //System.out.println(list.get(i) + " : true : last_added_inventory " + getValueByKey(mapDate, list.get(i)) + " : " + getValueByKey(mapDate, list.get(i + 1)));
                        }else {
                            Assert.assertTrue(getValueByKey(mapDate, list.get(i)).after(getValueByKey(mapDate, list.get(i + 1))));
                        }
                    }catch (Error e){
                        verificationErrors.append(" Test by dynamic search BY last_added_inventory is Filed!!!\n" + e.getMessage());
                        }
                }
                break;
            case "power_asc":
                String resultPowerAsc = getDataFromBase(list,null);
                Map<String, Integer> mapPowerAsc = parseDataByInfo(list, resultPowerAsc, "power");
                for (int i = 0; i < list.size() - 1; i++) {
                    try{
                        Assert.assertTrue(getValueByKey(mapPowerAsc, list.get(i)) <= getValueByKey(mapPowerAsc, list.get(i + 1)));
                    }catch (Error e){
                        verificationErrors.append(" Test by dynamic sort BY power_asc is Filed!!!\n" + e.getMessage());
                    }
                }
                break;
            case "power_desc":
                String resultPowerDesc = getDataFromBase(list,null);
                Map<String, Integer> mapPowerDesc = parseDataByInfo(list, resultPowerDesc, "power");
                for (int i = 0; i < list.size() - 1; i++) {
                    try{
                        Assert.assertTrue(getValueByKey(mapPowerDesc, list.get(i)) >= getValueByKey(mapPowerDesc, list.get(i + 1)));
                    }catch (Error e){
                        verificationErrors.append(" Test by dynamic sort BY power_desc is Filed!!!\n" + e.getMessage());
                    }
                }
                break;
            case "make_model_desc":
                String data = getDataFromBase(list, null);
                Map<String, Map<String, Integer>> mapMakeModel = parseDataByInfo(list, data, "MakeModel");
                for (int i = 0; i < list.size() - 1; i++) {
                    String fName = getKeyByValueFromSecondMap(mapMakeModel, list.get(i));
                    int fYear =  getValueByKeyFromSecondMap(mapMakeModel, list.get(i));
                    String sName = getKeyByValueFromSecondMap(mapMakeModel, list.get(i + 1));
                    int sYear = getValueByKeyFromSecondMap(mapMakeModel, list.get(i + 1));

                    if (fName.compareTo(sName) > 0 || fName.compareTo(sName) >= 0 && fYear >= sYear){
                    }else{
                        verificationErrors.append("Test by dynamic sort BY make_model_desc is filed!  "
                        + list.get(i) + "=" + fName + " : " + fYear + "  VS  " + list.get(i + 1) + "=" + sName + " : " + sYear + "\n");
                    }
                }
                break;
            case "make_model_asc":
                String data2 = getDataFromBase(list, null);
                Map<String, Map<String, Integer>> mapMakeModel2 = parseDataByInfo(list, data2, "MakeModel");
                for (int i = 0; i < list.size() - 1; i++) {
                    String fName = getKeyByValueFromSecondMap(mapMakeModel2, list.get(i));
                    int fYear =  getValueByKeyFromSecondMap(mapMakeModel2, list.get(i));
                    String sName = getKeyByValueFromSecondMap(mapMakeModel2, list.get(i + 1));
                    int sYear = getValueByKeyFromSecondMap(mapMakeModel2, list.get(i + 1));
                    if (sName.compareTo(fName) > 0 || sName.compareTo(fName) >= 0 && fYear >= sYear){
                    }else{
                        verificationErrors.append("Test by dynamic sort BY make_model_asc is filed!  "
                                + list.get(i) + "=" + fName + " : " + fYear + "  VS  " + list.get(i + 1) + "=" + sName + " : " + sYear + "\n");
                    }
                }
                break;
            default:
                verificationErrors.append(" Type wrong info for check result!!! \n");
        }
    }
}
