package PrepareTest;

import ASOURCE.CreateDirectory.Leads.Lead;
import ASOURCE.Data.User;
import ASOURCE.Data.UserData;
import ASOURCE.BaseClasses.PreAfterTest;
import ASOURCE.MyOwnMethod.Check;
import ASOURCE.TestingPages.Adminka.PageLeads;
import org.testng.annotations.Test;

public class TradeInAdminka extends PreAfterTest{

    @Test
    public void linkForAutoTradeInLead() throws Exception {
        getDriver().manage().window().maximize();
        String baseUrl = "http://tradein.demo.symfio.de";
        getDriver().get(baseUrl);
        Lead lead = new Lead();
        lead.createAndSendTradeIn();
        getDriver().get(baseUrl + "/admin/login");
        User user = new User();
        user.create(UserData.getName(), UserData.getPassword());
        user.logIn(getDriver());
        Check check = new Check();
        check.checkLogIn(getDriver(), getWait());
        PageLeads pageLeads = new PageLeads();
        pageLeads.hasLead("Max Muster");
        pageLeads.deleteLead("Max Mus");   // нужно дописать чтобы ожидал удаления через wait
        user.logOut();
        check.checkLogOut(baseUrl, getDriver());
    }
}
