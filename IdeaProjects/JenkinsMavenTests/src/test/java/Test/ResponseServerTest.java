package Test;

import ASOURCE.BaseClasses.PreAfterTest;
import ASOURCE.BaseClasses.TestBase;
import ASOURCE.MyOwnMethod.Check;
import ASOURCE.MyOwnMethod.UrlConectionMethod.UrlResponse;
import ASOURCE.ParseDirectory.Links.ParserLink;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;

public class ResponseServerTest extends PreAfterTest{

    @Test
    public void checkResponseForAllSiteLinks() throws Exception {
        getDriver().get(baseUrl);
        getDriver().manage().window().maximize();
        System.out.println("Test Response is Started!!!");
        // Get all Links for Site
        ParserLink parserLink = new ParserLink();
        List<String> list = parserLink.parse(baseUrl);
        // Get response for all links
        HashMap<String, Integer> response = UrlResponse.getParallelResponse(list,8,8);
        // Check response
        Check check = new Check();
        check.checkResponseCode(response);
    }
}

// get test host
//        Long start = System.currentTimeMillis();
//
//        String baseURL = TestHost.readTestUrl();
//        // get links from all site
//        HashSet<String> list = new HashSet<>();
//        try {
//            list = ParserLink.parseLinksFromAllSite(baseURL);
//            //list = ParserLink.parseLinksByPage(baseURL);
//        } catch (InterruptedException | IOException e) {
//            e.printStackTrace();
//        }
//        Long tmp = System.currentTimeMillis();
//        System.out.println(tmp - start);
//        for (String p : list){
//            try {
//                System.out.println(p + " : " + UrlResponse.getStatusCode(p));
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        Long finish = System.currentTimeMillis();
//        System.out.println(list.size());
//        System.out.println(finish - start);
//        System.out.println(list);
//        // get response for all links
//        List<Integer> listResponse = UrlResponse.getResponseServer(list);
//        // check response
//        System.out.println(listResponse);
