package Test;

import ASOURCE.BaseClasses.PreAfterTest;
import ASOURCE.Data.User;
import ASOURCE.Data.UserData;
import ASOURCE.MyOwnMethod.Check;
import ASOURCE.TestingPages.Adminka.PageMitarbeiter;
import org.testng.annotations.Test;

import java.io.IOException;

public class MitarbeiterPageSortTest extends PreAfterTest{

    @Test
    public void sortActiveTest() throws IOException, InterruptedException {
        System.out.println("Test sort active test is Started!!!");
        getDriver().get(baseUrl + "/admin/login");
        getDriver().manage().window().maximize();
        User user = new User();
        user.create(UserData.getName(), UserData.getPassword());
        user.logIn(getDriver());
        Check check = new Check();
        check.checkLogIn(getDriver(), getWait());
        PageMitarbeiter pageMitarbeiter = new PageMitarbeiter();
        pageMitarbeiter.openPageMiterbeiter(getDriver(), getWait(), baseUrl);
        pageMitarbeiter.sortActiveDescending(getDriver(), getWait());
        if (!pageMitarbeiter.checkHasActive(getWait())){
            pageMitarbeiter.doActiveUsers(5, getWait());
        }
        int countActive = pageMitarbeiter.getActiveUsers(getWait());
        pageMitarbeiter.checkDescendingSort(countActive, getWait());
        pageMitarbeiter.sortActiveAscending(getDriver(), getWait());
        pageMitarbeiter.checkAscendingSort(countActive, getDriver(), getWait());
    }
}
